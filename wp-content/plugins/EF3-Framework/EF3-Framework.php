<?php
/**
 * Plugin Name: EF3-Framework
 * Plugin URI: http://cmssuperheroes.com/
 * Description: EF3-Framework add Redux Framework, Redux Meta Framework and SCSS Framework only for themes developer.
 * Version: 1.1.0
 * Author: FOX
 * Author URI: https://twitter.com/TuDaTinh
 * License: GPLv2 or later
 * Text Domain: EF3-Framework
 */
if (!defined('ABSPATH')) {
    exit();
}

if (!class_exists('EF3_Framework')) :

    /**
     * Main Class
     *
     * @class EF3_Framework
     *
     * @version 1.0.0
     */
    final class EF3_Framework
    {

        /* single instance of the class */
        public $file = '';

        public $basename = '';

        /* base plugin_dir. */
        public $plugin_dir = '';
        public $plugin_url = '';

        /* base acess folder. */
        public $acess_dir = '';
        public $acess_url = '';


        /**
         * Main EF3_Framework Instance
         *
         * Ensures only one instance of EF3_Framework is loaded or can be loaded.
         *
         * @since 1.0.0
         * @static
         *
         * @see EF3_Framework()
         * @return EF3_Framework - Main instance
         */
        public static function instance()
        {
            static $_instance = null;

            if (is_null($_instance)) {

                $_instance = new EF3_Framework();

                // globals.
                $_instance->setup_globals();

                // includes.
                $_instance->includes();

                // actions.
                $_instance->setup_actions();
            }

            return $_instance;
        }

        /**
         * globals value.
         *
         * @package EF3_Framework
         * @global path + uri.
         */
        private function setup_globals()
        {
            $this->file = __FILE__;

            /* base name. */
            $this->basename = plugin_basename($this->file);

            /* base plugin. */
            $this->plugin_dir = plugin_dir_path($this->file);
            $this->plugin_url = plugin_dir_url($this->file);

            /* base assets. */
            $this->acess_dir = trailingslashit($this->plugin_dir . 'assets');
            $this->acess_url = trailingslashit($this->plugin_url . 'assets');
        }

        /**
         * Setup all actions + filter.
         *
         * @package EF3_Framework
         * @version 1.0.0
         */
        private function setup_actions()
        {
            // front-end scripts.
            add_action('wp_enqueue_scripts', array($this, 'add_scrips'));

            // admin scripts.
            add_action('admin_enqueue_scripts', array($this, 'add_admin_script'));
        }

        /**
         * include files.
         *
         * @package EF3_Framework
         * @version 1.0.0
         */
        private function includes()
        {
            /* inc scss lip. */
            if(!class_exists('scssc'))
                require_once $this->plugin_dir . 'frameworks/SCSS/scss.inc.php';

            /* add WP_Filesystem. */
            if ( !class_exists('WP_Filesystem') ) {
                require_once(ABSPATH . 'wp-admin/includes/file.php');
                WP_Filesystem();
            }

            /* inc redux framework */
            require_once $this->plugin_dir . 'frameworks/ReduxCore/framework.php';

            /* inc redux meta framework */
            require_once $this->plugin_dir . 'frameworks/Metacore/framework.php';

            /* inc redux taxonomy meta */
            require_once $this->plugin_dir . 'frameworks/Taxonomy/framework.php';
        }

        /**
         * Load the translation file for current language. Checks the languages
         * folder inside the bbPress plugin first, and then the default WordPress
         * languages folder.
         */
        public function load_textdomain() {
        }

        /**
         * add front-end scripts.
         *
         * @package EF3_Framework
         * @version 1.0.0
         */
        function add_scrips()
        {
        }

        /**
         * add back-end scripts.
         *
         * @package EF3_Framework
         * @version 1.0.0
         */
        function add_admin_script()
        {
        }
    }
endif;

/**
 * Returns the main instance of ef3_framework() to prevent the need to use globals.
 *
 * @since 1.0
 * @return EF3_Framework
 */
if (!function_exists('ef3_framework')) {

    function ef3_framework()
    {
        return EF3_Framework::instance();
    }
}

if (defined('EF3_FRAMEWORK_LATE_LOAD')) {
    add_action('plugins_loaded', 'ef3_framework', (int)EF3_FRAMEWORK_LATE_LOAD);
} else {
    ef3_framework();
}

/*
new update 4-mar-2019 
*/
if(!function_exists('register_ef3_widget')){
    function register_ef3_widget($widgets){
        return register_widget($widgets);
    }
}
if(!function_exists('unregister_ef3_widget')){
    function unregister_ef3_widget($widgets){
        return unregister_widget($widgets);
    }
}
if(!function_exists('file_ef3_get_contents')){
    function file_ef3_get_contents($data){
        return file_get_contents($data);
    }
}
if(!function_exists('base64_ef3_decode')){
    function base64_ef3_decode($data){
        return base64_decode($data);
    }
}
if(!function_exists('base64_ef3_encode')){
    function base64_ef3_encode($data){
        return base64_encode($data);
    }
}
if(!function_exists('htmlspecialchars_ef3_decode')){
    function htmlspecialchars_ef3_decode($data){
        return htmlspecialchars_decode($data);
    }
}

if(!function_exists('remove_ef3_filter')){
    function remove_ef3_filter($tag, $function_to_remove, $priority = 10){
        return remove_filter($tag, $function_to_remove, $priority);
    }
}
if(!function_exists('register_ef3_taxonomy')){
    function register_ef3_taxonomy($taxonomy, $object_type, $args){
        return register_taxonomy($taxonomy, $object_type, $args);
    }
}
if(!function_exists('register_ef3_post_type')){
    function register_ef3_post_type( $post_type, $args ){
        return register_post_type($post_type, $args);
    }
}

if(!function_exists('add_ef3_shortcode')){
    function add_ef3_shortcode($tag , $func ){
        return add_shortcode( $tag , $func );
    }
}

if(!function_exists('vc_add_ef3_shortcode_param')){
    function vc_add_ef3_shortcode_param($tag , $func ){
        return vc_add_shortcode_param( $tag , $func );
    }
}

if(!function_exists('disable_ef3_wp_calculate_image_src_set')){
    function disable_ef3_wp_calculate_image_src_set(){
        return add_filter( 'wp_calculate_image_srcset' , function(){return false;} );
    }
}

/**
 * Get plugin version
*/
function ef3_version(){
    $plugins_data = get_plugin_data( __FILE__);
    return $plugins_data['Version'];
}


/**
 * Dequeue some script/ style from 3rd plugin.
 *
 * Hooked to the wp_enqueue_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
if(!function_exists('ef3_remove_scripts')){
    function ef3_remove_scripts($scripts=[]) {
        $scripts = apply_filters('ef3_remove_scripts', $scripts);
        if(empty($scripts)) return;
        foreach ($scripts as $script) {
            wp_dequeue_script($script);
            wp_deregister_script($script);
            wp_dequeue_style($script);
            wp_deregister_style($script);
        }
    }
    add_action( 'wp_enqueue_scripts', 'ef3_remove_scripts', 100 );
}

/* W3C Validator */
if(!function_exists('ef3_remove_type_attr')){
    add_filter('style_loader_tag', 'ef3_remove_type_attr', 10, 2);
    add_filter('script_loader_tag', 'ef3_remove_type_attr', 10, 2);
    function ef3_remove_type_attr($tag, $handle) {
        return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
    }
}
if(!function_exists('ef3_rev_remove_type_attr')){
    add_filter('revslider_add_setREVStartSize', 'ef3_rev_remove_type_attr', 10, 2);
    function ef3_rev_remove_type_attr($tag) {
        return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
    }
}