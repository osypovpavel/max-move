<?php
/**
 * The template for displaying Author Archive pages
 *
 * Used to display archive-type pages for posts by an author.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */

get_header(); ?>
    <div class="container">
    <div class="row blog-author">

	    <section id="primary" class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div id="content" role="main">

		<?php if ( have_posts() ) : ?>
 
			<?php
			// If a user has filled out their description, show a bio on their entries.
			if ( get_the_author_meta( 'description' ) ) : ?>
			<div class="author-info">
				<div class="author-avatar">
					<?php
					 
					$author_bio_avatar_size = apply_filters( 'twentytwelve_author_bio_avatar_size', 68 );
					echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
					?>
				</div><!-- .author-avatar -->
				<div class="author-description">
					<h2><?php printf( esc_html__( 'About %s', 'wp-maxclean' ), get_the_author() ); ?></h2>
					<p><?php the_author_meta( 'description' ); ?></p>
				</div><!-- .author-description	-->
			</div><!-- .author-info -->
			<?php endif; ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'single-templates/content/content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php wp_maxclean_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'single-templates/content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    <?php get_sidebar(); ?>
    </div>
</div>
</div>
<?php if(is_active_sidebar('sidebar-13') || is_active_sidebar('sidebar-14') || is_active_sidebar('sidebar-15')):?>
     <div class="contact-footer-page  triagl triagl-top triagl-secondary vc_row-fluid " >
        <div class="container">
    		<div class="row">        
    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
    				  <?php dynamic_sidebar('sidebar-13'); ?>
    			</div> 
    
    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
    				  <?php dynamic_sidebar('sidebar-14'); ?>
    			</div> 
    
    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
    				  <?php dynamic_sidebar('sidebar-15'); ?>
    			</div> 
    		</div>
    	</div>            
    </div>
<?php endif;?>
<?php get_footer(); ?>