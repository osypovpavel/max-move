<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>
 <?php 
global $post;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog">
    <h6 class="hidden"><?php the_title(); ?> </h6>
        <div class="blog-reviews">
          <blockquote><i class="icon fa fa-quote-left"></i>
            <p><?php echo apply_filters('get_the_excerpt', preg_match('/<blockquote>(.*)<\/blockquote>/', '', get_the_content())); ?></p>
          </blockquote>
          <span class="blog-reviews__autor">-- <?php if(get_post_meta($post->ID,'reviews_author',true)!='') echo esc_html(get_post_meta($post->ID,'reviews_author',true)); else the_author();?></span> 
		</div>
        <div class="border-color"></div>
        
	</div>
	<!-- .entry-blog -->
</article>
<!-- #post -->
