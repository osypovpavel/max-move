<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog">
		<div class="entry-inner">
		  <div class="entry-feature entry-video"><?php wp_maxclean_archive_audio(); ?></div>
		</div>
		<h2 class="entry-title"><?php if(is_sticky()) { echo "<i class='fa fa-thumb-tack'></i>";} ?><?php the_title(); ?></h2>
  
          <div class="entry-content ui-text"><?php echo wp_maxclean_limit_words(get_the_excerpt(),22); ?></div>
          <footer class="entry-footer">
		    <?php wp_maxclean_archive_readmore(); ?>
		  </footer>
          
         <div class="border-color"></div>
	</div>
	<!-- .entry-blog -->
</article>
 
