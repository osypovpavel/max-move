<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog entry-post">
        <div class="entry-inner">
            <?php if(get_the_post_thumbnail(get_the_ID(),'wp_maxclean_blog-large')):?>
                <ul class="info-post">
                  <li class="date"><?php echo get_the_date("d"); ?></li>
                  <li class="month"><?php echo get_the_date("M"); ?></li>
                </ul>
                <div class="entry-feature entry-feature-image"><?php the_post_thumbnail( 'full' ); ?></div>
            <?php  endif;?>
		</div>
        
         <h2 class="entry-title">
		 <?php if(is_sticky()) { echo "<i class='fa fa-thumb-tack'></i>";} ?>
		 <?php the_title(); ?>
		 </h2>
         <ul class="entry-meta">
             
           <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo esc_html__('By ','wp-maxclean');?><?php the_author(); ?></a></li>
            <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
        	<li class="comments-link"><i class="icon-commets fa fa-comments"></i><?php comments_popup_link( esc_html__( ' 0', 'wp-maxclean' ), esc_html__( ' 1', 'wp-maxclean' ), esc_html__( ' %', 'wp-maxclean' ) ); ?></li>
            <?php endif; ?>
            
          </ul>
          
          <div class="entry-content ui-text">
          <?php 
          the_content();
          wp_link_pages( array(
        		'before'      => '<div class="pagination loop-pagination"><span class="page-links-title">' . esc_html__( 'Pages:','wp-maxclean') . '</span>',
        		'after'       => '</div>',
        		'link_before' => '<span class="page-numbers">',
        		'link_after'  => '</span>',
    		) );
          ?>
          </div>
           
          <div class="footer-panel">
                <div class="social-links clearfix">
                <?php wp_maxclean_social_share(); ?>
                </div>
                <span class="footer-panel__title">TAGS :</span>
                 <?php if(has_tag()): ?>
                 <ul class="tags-group">
                    <li class="detail-tags"><?php the_tags('', ', ' ); ?></li>
                </ul>
                <?php endif; ?>
                
                <div class="border-color clearfix"></div>
          </div>
          
         
	</div>
	<!-- .entry-blog -->
</article>
<!-- #post -->
