<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog entry-post">
		<div class="entry-header">
			
		    <div class="entry-feature entry-audio"><?php $audio = wp_maxclean_archive_audio(); ?></div>
			 
		</div>
         <h2 class="entry-title">
		 <?php if(is_sticky()) { echo "<i class='fa fa-thumb-tack'></i>";} ?>
		 <?php the_title(); ?>
		 </h2>
         <ul class="entry-meta">
             
           <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo esc_html__('By ','wp-maxclean');?><?php the_author(); ?></a></li>
            <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
        	<li class="comments-link"><i class="icon-commets fa fa-comments"></i><?php comments_popup_link( esc_html__( ' 0', 'wp-maxclean' ), esc_html__( ' 1', 'wp-maxclean' ), esc_html__( ' %', 'wp-maxclean' ) ); ?></li>
            <?php endif; ?>
            <li>
            <div class="entry-date">
				<div class="arow-date"></div>
				<?php wp_maxclean_archive_post_icon(); ?>
				<span><?php echo get_the_date("F d,Y"); ?></span>
			</div>
            </li>
            
          </ul>
		<!-- .entry-header -->
         
		<div class="entry-content">
			<?php if($audio){ echo apply_filters('the_content', preg_replace(array('/\[audio(.*)\](.*)\[\/audio\]/', '/\[audio(.*)\]/', '/\[playlist(.*)\]/'), '', get_the_content(), 1));} else { the_content(); }
	    		wp_link_pages( array(
	        		'before'      => '<div class="pagination loop-pagination"><span class="page-links-title">' . esc_html__( 'Pages:','wp-maxclean') . '</span>',
	        		'after'       => '</div>',
	        		'link_before' => '<span class="page-numbers">',
	        		'link_after'  => '</span>',
	    		) );
			?>
		</div>
		<!-- .entry-content -->
	</div>
	<!-- .entry-blog -->
</article>
<!-- #post -->
