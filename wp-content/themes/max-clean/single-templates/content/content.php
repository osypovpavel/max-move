<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>
<?php 
 /*if ( has_post_thumbnail() ) {
     $attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'wp_maxclean_blog-large', false);
     $link_image =  esc_attr($attachment_image[0]);
 }*/
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog entry-post">
        <div class="entry-inner">
        <?php if(get_the_post_thumbnail(get_the_ID(),'wp_maxclean_blog-large')):?>
            <ul class="info-post">
              <li class="date"><?php echo get_the_date("d"); ?></li>
              <li class="month"><?php echo get_the_date("M"); ?></li>
            </ul>
            <div class="entry-feature entry-feature-image"><?php the_post_thumbnail( 'wp_maxclean_blog-large' ); ?></div>
            <?php  endif;?>
		</div>
         <h2 class="entry-title">
         <a href="<?php the_permalink(); ?>">
	    		<?php
		    		if(is_sticky()){
		                echo "<i class='fa fa-thumb-tack'></i>";
		            }
		    	?>
	    		<?php the_title(); ?>
	       </a>
		 
		 </h2>
          
          <ul class="entry-meta">
             
           <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo esc_html__('By ','wp-maxclean');?><?php the_author(); ?></a></li>
            <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
        	<li class="comments-link"><i class="icon-commets fa fa-comments"></i><?php comments_popup_link( esc_html__( ' 0', 'wp-maxclean' ), esc_html__( ' 1', 'wp-maxclean' ), esc_html__( ' %', 'wp-maxclean' ) ); ?></li>
            <?php endif; ?>
            
          </ul>
          <div class="entry-content ui-text"><?php echo wp_maxclean_limit_words(get_the_excerpt(),220); ?></div>
          <footer class="entry-footer">
		    <?php wp_maxclean_archive_readmore(); ?>
		  </footer>
          
         <div class="border-color"></div>
	</div>
	<!-- .entry-blog -->
</article>
<!-- #post -->
