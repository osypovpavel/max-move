<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog">
		<div class="entry-inner">
		  <div class="entry-feature entry-video"><?php wp_maxclean_archive_audio(); ?></div>
		</div>
		<h2 class="entry-title">
		 <a href="<?php the_permalink(); ?>">
	    		<?php
		    		if(is_sticky()){
		                echo "<i class='fa fa-thumb-tack'></i>";
		            }
		    	?>
	    		<?php the_title(); ?>
	       </a>
		 </h2>
 
		 <ul class="entry-meta">
            <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo esc_html__('By ','wp-maxclean');?><?php the_author(); ?></a></li>
            <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
        	<li class="comments-link"><i class="icon-commets fa fa-comments"></i><?php comments_popup_link( esc_html__( ' 0', 'wp-maxclean' ), esc_html__( ' 1', 'wp-maxclean' ), esc_html__( ' %', 'wp-maxclean' ) ); ?></li>
            <?php endif; ?>
            
          </ul>
          <div class="entry-content ui-text"><?php the_excerpt();?></div>
          <footer class="entry-footer">
		    <?php wp_maxclean_archive_readmore(); ?>
		  </footer>
          
         <div class="border-color"></div>
	</div>
	<!-- .entry-blog -->
</article>
 
