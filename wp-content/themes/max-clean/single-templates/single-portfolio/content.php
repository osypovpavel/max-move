<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
 $portfolio_meta=wp_maxclean_post_meta_data();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog entry-post">
        <?php if(isset($portfolio_meta->_cms_show_title) && $portfolio_meta->_cms_show_title=='1'):?>
	     <h2 class="entry-title">
		  <?php the_title(); ?>
		 </h2>
         
         <?php endif;?>
		<div class="entry-content">
			<?php the_content();?>
		</div>
        <h6 class="hidden"><?php the_title(); ?> </h6>
		<!-- .entry-content -->
		<!-- .entry-footer -->
	</div>
	<!-- .entry-blog -->
</article>
<!-- #post -->
