<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-blog entry-post">
	  
		<div class="entry-content">
			<?php
    			if($gallery) {
    			    echo apply_filters('the_content', preg_replace('/\[gallery(.*)]/', '', get_the_content(), 1));
    			} else {
    			    the_content();
    			}
	    	 
			?>
		</div>
		<!-- .entry-content -->
	</div>
	<!-- .entry-blog -->
</article>
<!-- #post -->
