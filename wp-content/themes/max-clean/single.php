<?php
/**
 * The Template for displaying all single posts
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */

get_header(); ?>
<div class="container">
    <div class="row single-row">
        <div id="primary" class="col-xs-12">
            <div id="content" role="main">
                <style>
                    .entry-feature.entry-feature-image{
                        max-height: 500px;
                        overflow: hidden;
                    }
                    .post img{
                        margin: 0 auto;
                        object-fit: cover;
                        height: 100%;
                        width: 100%;
                    }
                </style>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php
                        $roles = get_the_author_meta('roles');
                    ?>
                    <?php get_template_part( 'single-templates/single/content', get_post_format() ); ?>
<!--                    <div class="about-autor clearfix">-->
<!--                         <div class="about-autor__foto">-->
<!--                         --><?php //echo get_avatar(get_the_author_meta('ID'), 121); ?>
<!--                          </div>-->
<!--                            <div class="about-autor__inner">-->
<!--                            <span class="about-autor__name">--><?php //echo get_the_author(); ?><!--</span><span class="about-autor__category">  --><?php //echo ''.$roles[0]; ?><!-- </span>-->
<!---->
<!--                              <div class="border-color"></div>-->
<!--                              <div class="about-autor__text ui-text">-->
<!--                              --><?php //echo get_the_author_meta('description'); ?>
<!--                              </div>-->
<!--                          </div>-->
<!--                    </div>-->
                    <?php comments_template( '', true ); ?>

                <?php endwhile; // end of the loop. ?>

            </div><!-- #content -->
        </div><!-- #primary -->
<!--        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">-->
<!--            --><?php //get_sidebar(); ?>
<!--        </div>-->
    </div>
</div>
<?php if(0 > 1)://if(is_active_sidebar('sidebar-13') || is_active_sidebar('sidebar-14') || is_active_sidebar('sidebar-15')):?>
<div class="contact-footer-page triagl triagl-top triagl-secondary vc_row-fluid " >
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
                  <?php dynamic_sidebar('sidebar-13'); ?>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
                  <?php dynamic_sidebar('sidebar-14'); ?>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
                  <?php dynamic_sidebar('sidebar-15'); ?>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<?php get_footer(); ?>