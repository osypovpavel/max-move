<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     20.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product, $woocommerce_loop, $smof_data;
$woo_columns_layout = $smof_data['woo_columns_layout'];
$woo_number_page = $smof_data['woo_number_page'];

 
$woo_columns = (isset($_GET['woo-columns'])) ? intval($_GET['woo-columns']) : $smof_data['woo_columns_layout'];
// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;
// Store column count for displaying the grid
 
// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;
// Increase loop count
$woocommerce_loop['loop']++;
// Extra post classes
$classes = array();
 
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woo_columns || 1 == $woo_columns )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woo_columns )
	$classes[] = 'last';
    

switch ($woo_columns) {
	case 2:
		$classes[] = 'col-md-6 col-lg-6 woo-product';
		break;
	case 4:
		$classes[] = 'col-md-3 col-lg-3 woo-product';
		break;
	default:
		$classes[] = 'col-md-4 col-lg-4 woo-product';
		break;
}
?>
<div <?php post_class( $classes ); ?>>
	<div class="cshero-carousel-item-wrap">
        <div class="cshero-carousel-item">
        	<div class="cshero-woo-image">
			<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
				<a href="<?php the_permalink(); ?>">
					<?php
						/**
						 * woocommerce_before_shop_loop_item_title hook
						 *
						 * @hooked woocommerce_show_product_loop_sale_flash - 10
						 * @hooked woocommerce_template_loop_product_thumbnail - 10
						 */
						do_action( 'woocommerce_before_shop_loop_item_title' );
					?>
				</a>
			</div>
		</div>
		<div class="cshero-woo-meta">
			<div class="cshero-product-title">
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			</div>
			<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
			?>
			<?php
				/**
				 * woocommerce_after_shop_loop_item hook
				 *
				 * @hooked woocommerce_template_loop_add_to_cart - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item' ); 
			?>
			 
		</div>
	</div>
</div>
