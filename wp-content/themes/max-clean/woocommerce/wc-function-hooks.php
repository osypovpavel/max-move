<?php

//add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
/**
 * Custom Product Filter Price - Default Widget
 */
add_action( 'widgets_init', 'wp_maxclean_override_woocommerce_widgets', 15 );
function wp_maxclean_override_woocommerce_widgets() { 
    if ( class_exists( 'WC_Widget_Price_Filter' ) ) {
        unregister_widget( 'WC_Widget_Price_Filter' );
        include_once( 'widgets/cms-price-filter.php' );
        register_widget( 'Custom_WC_Widget_Price_Filter' );
    } 
}

add_filter('loop_shop_per_page', 'wp_maxclean_products_per_page');
function wp_maxclean_products_per_page()
{
    global $smof_data;
     
    return !empty($smof_data['woo_number_page']) ? $smof_data['woo_number_page'] : 9;
}
