<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     10.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
    return;
}
if ( !wp_script_is('', 'wp-maxclean-owl-carousel')) {
            wp_enqueue_style('wp-maxclean-owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), '1.0.1');
            wp_enqueue_script('wp-maxclean-owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '1.3.2', true);
        }
        if ( !wp_script_is('', 'owl-carousel-cms')) {
           // wp_enqueue_script('owl-carousel-cms', get_template_directory_uri() . '/assets/js/owl.carousel.cms.js', array( 'jquery' ), '1.3.2', true);
        }
$posts_per_page = 6;
if ( ! $related = wc_get_related_products( $product->get_ID(),$posts_per_page ) ) {
	return;
}
  
if ( sizeof( $related ) == 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->get_ID() )
) );

$wp_maxclean_products = new WP_Query( $args );

$woocommerce_loop['columns'] = 4;

ob_start();
$date = time() . '_' . uniqid(true);

if ( $wp_maxclean_products->have_posts() ) : ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div id="cs_carousel_product<?php echo esc_attr($date); ?>" class="cms-carousel related-product-carousel">
		<?php while ( $wp_maxclean_products->have_posts() ) : $wp_maxclean_products->the_post(); ?>
			<?php wc_get_template_part( 'content', 'related-product' ); ?>
		<?php endwhile; // end of the loop. ?>
	</div>
    </div>
<?php endif;
wp_reset_postdata();
