<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package cshero
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
 
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
	<?php // You can start editing here -- including this comment! ?>
	<?php if ( have_comments() ) : ?>
        <div class="st-comments-wrap">
            <h3 class="comments-title">
                <span><?php comments_number(esc_html__('Comments','wp-maxclean'),esc_html__('Comments (1)','wp-maxclean'),esc_html__('Comments (%)','wp-maxclean')); ?></span>
            </h3>
            
            <ul class="comments-list clearfix">
				<?php wp_list_comments( 'type=comment&callback=wp_maxclean_comment' ); ?>
			</ul>
			<?php wp_maxclean_comment_nav(); ?>
        </div>
	<?php endif; // have_comments() ?>

	<?php
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name__mail' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
    if ( is_user_logged_in() ){
        $comment_class='col-xs-12 col-sm-12 col-md-12 col-lg-12';
        $enddiv='<div class="row">';
    }else{
        $comment_class='col-xs-12 col-sm-12 col-md-7 col-lg-7';
        $enddiv='';  
    }  
	$args = array(
			'id_form'           => 'commentform',
			'id_submit'         => 'submit',
            'class_submit'      => 'btn',
            'name_submit'       => 'submit',
			'title_reply'       => esc_html__( 'Leave A Comment',"wp-maxclean"),
			'title_reply_to'    => esc_html__( 'Leave A Comment To %s',"wp-maxclean"),
			'cancel_reply_link' => esc_html__( 'Cancel Comment',"wp-maxclean"),
			'label_submit'      => esc_html__( 'submit comment',"wp-maxclean"),
			'comment_notes_before' => '',
            'format'            => 'xhtml',
            'comment_field' =>  $enddiv.'
                    <div class="'.$comment_class.'">
                        <div class="input-group">
                            <textarea id="comment-text" name="comment" placeholder="'.esc_html__( "COMMENT", "wp-maxclean" ).'" required rows="8" tabindex="4" class="form-control"></textarea>
                        </div>
                    </div>
                </div>',
             'submit_button'=>'
                    <div class="row">
                        <div class="col-md-5">
                        <button class="btn" tabindex="5">submit comment<i class="icon fa fa-angle-double-right"></i> </button>
                        </div>
                      </div> ',
            
	       'comment_notes_after' => '',
	);
	comment_form($args);
     
	?>

</div><!-- #comments -->
