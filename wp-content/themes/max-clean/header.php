<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
?>
<?php global $smof_data; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="initial-scale=1, width=device-width" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ): ?>
<link rel="icon" type="image/png" href="<?php echo esc_url($smof_data['favicon_icon']['url']); ?>"/>
<?php endif; ?>
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<?php
wp_enqueue_script( 'wp-maxclean-html5-ie-js', get_template_directory_uri().'/assets/js/html5.js', array() ,'3.7.0');
?>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php 
	global $smof_data;
 ?>
<body <?php body_class(); ?>>
<style>
    .entry-content p,.entry-content li{
        font-size: 16px!important;
        line-height: 1.2!important;
        font-weight: 400;
        color: #3f3f3f;
    }
    .entry-content li{
        margin-bottom: 5px;
    }
    .footer-panel__title{
        display: none;
    }
</style>
<?php wp_maxclean_get_page_loading(); ?>
<div id="page" class="<?php wp_maxlean_page_class(); ?>">
	<header id="masthead" class="header">
		<?php wp_maxclean_header(); ?>
	</header><!-- #masthead -->
    <?php wp_maxclean_page_title(); ?>
	<div id="main">
    
    
    