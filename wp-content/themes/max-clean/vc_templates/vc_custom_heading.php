<?php
/**
 * @var $this WPBakeryShortCode_VC_Custom_heading
 */
if($atts['cms_template'] && $atts['cms_template']!='vc_custom_heading.php') {
    require( get_template_directory() . '/vc_templates/'.$atts['cms_template'] );
}else{
    $output = $output_text = $text = $google_fonts = $font_container = $el_class = $css = $google_fonts_data = $font_container_data = '';
    extract( $this->getAttributes( $atts ) );
    $link = vc_gitem_create_link( $atts );
    extract( $this->getStyles( $el_class, $css, $google_fonts_data, $font_container_data, $atts ) );
    $settings = get_option( 'wpb_js_google_fonts_subsets' );
    $subsets = '';
    if ( is_array( $settings ) && ! empty( $settings ) ) {
    	$subsets = '&subset=' . implode( ',', $settings );
    }
    if ( ! empty( $link ) ) {
    	$text = '<' . $link . '>' . $text . '</a>';
    }
    if ( ! empty( $google_fonts_data ) && isset( $google_fonts_data['values']['font_family'] ) ) {
    	wp_enqueue_style( 'vc_google_fonts_' . vc_build_safe_css_class( $google_fonts_data['values']['font_family'] ), '//fonts.googleapis.com/css?family=' . $google_fonts_data['values']['font_family'] . $subsets );
    }
    
    $style_sub='';
    if ( ! empty( $styles ) ) {
        $style_sub = 'style="' . esc_attr( implode( ';', $styles ) ) . '"';
    }
    $styles[]=isset($atts['letter_spacing_title'])? 'letter-spacing:'.$atts['letter_spacing_title']:'';
    
    $style = '';
    if ( ! empty( $styles ) ) {
        $style = 'style="' . esc_attr( implode( ';', $styles ) ) . '"';
    }

    $border_style='style="'; 
    $border_width = isset($atts['border_color_width'])? ' width:'.$atts['border_color_width'].';':'';
    $border_thick = isset($atts['border_color_thick'])? ' border-width:'.$atts['border_color_thick'].';':'';
    $border_margin_top = isset($atts['border_color_margin_top'])? ' margin-top:'.$atts['border_color_margin_top'].';':'';
    $border_margin_bottom = isset($atts['border_color_margin_bottom'])? ' margin-bottom:'.$atts['border_color_margin_bottom'].';':'';
    $border_style.=$border_width.$border_thick.$border_margin_top.$border_margin_bottom;
    $border_style.='"';
    
    if(isset($atts['heading_style'])) $css_class.=' '.$atts['heading_style'];

    $output .= '<div class="wraper-heading">';
    
    if(!empty($text)){
    if(apply_filters('vc_custom_heading_template_use_wrapper', false)) {
        $output .= '<div class="' . esc_attr( $css_class ) . '" >';
        $output .= '<' . $font_container_data['values']['tag'] . ' ' . $style . ' >';
        $output .= $text;
        $output .= '</' . $font_container_data['values']['tag'] . '>';
        $output .= '</div>';
    } else {
        $output .= '<' . $font_container_data['values']['tag'] . '  class="ui-title-block ' . esc_attr( $css_class ) . '" '.$style.'>';
        $output .= $text;
        $output .= '</' . $font_container_data['values']['tag'] . '>';
    }
    } 
    if(isset($atts['subtitle']) && $atts['subtitle']!=''){    
        if(isset($atts['subtitle_on_top'])) $output='<div class="' . esc_attr( $css_class ) . ' sub-title-on-top ui-subtitle-block ">'.$atts['subtitle'].'</div>'.$output;
        else $output.='<div class="' . esc_attr( $css_class ) . ' sub-title-on-bottom ui-subtitle-block ">'.$atts['subtitle'].'</div>';
    }

    if(isset($atts['show_border_color']))
        $output.='<div class="' . esc_attr( $css_class ) . ' border-color" '.$border_style.'></div>';

    $output .= '</div>';  
    echo ''.$output;
}