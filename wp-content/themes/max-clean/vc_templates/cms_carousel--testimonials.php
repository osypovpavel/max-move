<div class="layout-testimonials home-testimonials-carousel cms-carousel <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php
    $posts = $atts['posts'];
    while($posts->have_posts()){
        $posts->the_post();
        $testimonial_meta=wp_maxclean_post_meta_data();
        ?>
        <div class="cms-carousel-item">
            <div class="container">
                
                <div class="media-image  ">
                    <?php
                        if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'related-img', false)):
                            $class = ' has-thumbnail';
                            $thumbnail = get_the_post_thumbnail(get_the_ID(),'thumbnail');
                        else:
                            $class = ' no-image';
                            $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                        endif;
                        echo '<div class="cms-grid-media round-figure round-figure_mod-a '.esc_attr($class).'">'.$thumbnail.'</div>';
                    ?>
                </div>
                    
                <div class="cms-carousel-content">
                    <blockquote>
                    <?php the_content();?>
                    </blockquote>
                    <span class="slider-reviews-autor"><strong>-- <?php the_title();?>,</strong> <?php echo esc_attr($testimonial_meta->_cms_testimonial_position) ?></span>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>