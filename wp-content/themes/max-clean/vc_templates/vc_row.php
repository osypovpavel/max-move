<?php
global $smof_data;
/** @var $this WPBakeryShortCode_VC_Row */

$output = $after_output = $row_style = $row_data = $video_style = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$uqid = uniqid();

$el_class = $this->getExtraClass( $el_class );
$css_classes = array(
    'wpb_row', //deprecated
    'vc_row-fluid',
    'cshero_'. $uqid,
    $el_class,
    vc_shortcode_custom_css_class( $css ),
);

$wrapper_attributes = array();
// build attributes for wrapper
if ( ! empty( $row_id ) ) {
    $wrapper_attributes[] = 'id="' . esc_attr( $row_id ) . '"';
}

/* Row Full Width */
$class_full_width = '';
if($full_width) {
    $css_classes[] = 'cms-row-full-width';
}

if ( ! empty( $full_height ) ) {
    $css_classes[] = ' vc_row-o-full-height';
    if ( ! empty( $content_placement ) ) {
        $css_classes[] = ' vc_row-o-content-' . $content_placement;
    }
}

/* Class Animation For Row */
if ($row_animation) {
    wp_enqueue_script( 'waypoints');
    $css_classes[] = $row_animation;
}
 
$html_overlay_row = '';
if($overlay_row == 'yes') {
    $html_overlay_row .= '<div class="cms-overlay-color" style="background-color: '.$overlay_color.'; opacity: '.$overlay_opacity.';"></div>';
}

if($overlay_row == 'yes') {
    $css_classes[]= 'row-overlay-color';
}

$triagl_class='';
if($enable_triagl_style && $triagl_style){
    $triagl_style == "topgray"? $triagl_class='triagl triagl-top triagl-gray' :'';
	$triagl_style == "topprimary"? $triagl_class='triagl triagl-top triagl-primary' :'';
    $triagl_style == "topsecondary"? $triagl_class='triagl triagl-top triagl-secondary' :'';
    $triagl_style == "topwhite"? $triagl_class='triagl triagl-top triagl-white' :'';
    $triagl_style == "btmtransparent"? $triagl_class='triagl-btm-tran' :'';
    $triagl_style == "btmgray"? $triagl_class='triagl triagl-btm triagl-gray' :'';
	$triagl_style == "btmprimary"? $triagl_class='triagl triagl-btm triagl-primary' :'';
    $triagl_style == "btmsecondary"? $triagl_class='triagl triagl-btm triagl-secondary' :'';
    $triagl_style == "btmwhite"? $triagl_class='triagl triagl-btm triagl-white' :'';
}
if(isset($is_call_to_action) && $is_call_to_action=='1') $css_classes[]= 'row-calltoaction';
$css_classes[]= $triagl_class;
if($hide_mobile_bg) $css_classes[]= 'hide-bg-mobile';

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

$bg_row_position='';
if($bg_position!='') $bg_row_position=' background-position: '.esc_attr($bg_position).' !important;';
$border_top_style='';
if($border_top_color!='') $border_top_style=' border-top: 6px solid '.esc_attr($border_top_color).';';
$zindex_style='';
if($rowz_index!='') $zindex_style=' z-index:'.esc_attr($rowz_index).';';
$overflow_style='';
if($overflow_hidden) $overflow_style=' overflow: hidden;';
$style = ' style ="'.$row_style. $bg_row_position. $border_top_style. $zindex_style. $overflow_style.'"';   
 
/* Waves */
$wave_margin_bottom=$wave_margin_bottom_of_wtop;
if($wave_margin_bottom_of_wtop !='' && strpos($wave_margin_bottom_of_wtop,'px')===false) $wave_margin_bottom = esc_attr($wave_margin_bottom).'px';
$wave_margin_top = $wave_margin_top_of_wbottom;
if($wave_margin_top_of_wbottom !='' && strpos($wave_margin_top_of_wbottom,'px')===false) $wave_margin_top = esc_attr($wave_margin_top).'px';
$wave_top_style = $wave_margin_bottom_of_wtop!=''?' style="margin-bottom:'.esc_attr($wave_margin_bottom).'"':'';
$wave_buttom_style = $wave_margin_top_of_wbottom!=''?' style="margin-top:'.esc_attr($wave_margin_top).'"':'';

$dwdu = (isset($row_data_wow_duration) and $row_data_wow_duration!='')?'data-wow-duration="'.esc_attr($row_data_wow_duration).'"':''; 
$dwd = (isset($data_row_delay) and $data_row_delay!='')?' data-wow-delay="'.esc_attr($data_row_delay).'"':'';   

?>
    
<div <?php echo ''.$dwdu.$dwd; ?> <?php echo implode( ' ', $wrapper_attributes ); ?> <?php echo esc_attr($row_data); ?><?php echo ''.$style; ?>>
    <?php if($enable_waves_top):?><?php echo '<div class="border-wave border-wave_'.$wave_top_color_type.' " '.$wave_top_style.' ></div>';?><?php endif ; ?>
    <?php echo ''.$html_overlay_row; ?>
    <?php if(!$full_width): ?><div class="container"><div class="row"><?php endif ; ?>
    <?php if($full_width): ?><div class="no-container"><div class="row"><?php endif ; ?>
    <?php echo wpb_js_remove_wpautop( $content ); ?>
    <?php if(!$full_width): ?></div></div><?php endif ; ?>
    <?php if($full_width): ?></div></div><?php endif ; ?>
     
    <?php if($enable_waves_bottom):?><?php echo '<div class="border-wave border-wave_'.$wave_bottom_color_type.' " '.$wave_buttom_style.' ></div>';?><?php endif ; ?>
</div>
