<?php
	$content_align = !empty($atts['content_align'])?$atts['content_align']:'';
?>
<div class="cms-social-wraper <?php echo esc_attr($atts['template']);echo esc_attr($content_align);?>" id="<?php echo esc_attr($atts['html_id']);?>">
	<ul class="icon-list social-links">
	<?php
		$items = ((int)$atts['cms_item'])?(int)$atts['cms_item']:1;
	    for($i=1;$i<=$items;$i++){ ?>
            <?php
            $icon = isset($atts['icon'.$i])?$atts['icon'.$i]:'';
            $link = !empty($atts['link'.$i]) ? $atts['link'.$i] : '#';
        ?>
            <li class="social-links-item fancybox-item clearfix">
            	<?php if($icon !=''):?>
	               
	                    <a class="social-links-link" href="<?php echo esc_url($link);?>"><i class="icon <?php echo esc_attr($icon);?>"></i></a>
	                
                <?php endif;?>
            </li>
        <?php }
	?>
	</ul>
</div>