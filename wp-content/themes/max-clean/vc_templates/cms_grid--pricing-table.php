<?php 
    /* get categories */
        $taxo = 'category';
        $_category = array();
        if(!isset($atts['cat']) || $atts['cat']==''){
            $terms = get_terms($taxo);
            foreach ($terms as $cat){
                $_category[] = $cat->term_id;
            }
        } else {
            $_category  = explode(',', $atts['cat']);
        }
        $atts['categories'] = $_category;
         
?>
<div class="cms-grid-wraper cms-grid-wraper-pricing-table <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php if($atts['filter']=="true" and $atts['layout']=='masonry'):?>
        <div class="cms-grid-filter">
            <ul class="cms-filter-category list-unstyled list-inline">
                <li><a class="active" href="#" data-group="all">All</a></li>
                <?php 
                if(is_array($atts['categories']))
                foreach($atts['categories'] as $category):?>
                    <?php $term = get_term( $category, $taxo );?>
                    <li><a href="#" data-group="<?php echo esc_attr('category-'.$term->slug);?>">
                            <?php echo esc_attr($term->name);?>
                        </a>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    <?php endif;?>
    
    <ul class="list-price row cms-grid <?php echo esc_attr($atts['grid_class']);?>">
        <?php
        $posts = $atts['posts'];
        $size = ($atts['layout']=='basic')?'thumbnail':'medium';
        while($posts->have_posts()){
            $posts->the_post();
            $pricing_meta=wp_maxclean_post_meta_data();
            $groups = array();
            $groups[] = '"all"';
            foreach(cmsGetCategoriesByPostID(get_the_ID(),$taxo) as $category){
                $groups[] = '"category-'.$category->slug.'"';
            }
            ?>
            <li class="list-price-item cms-grid-item <?php echo esc_attr($atts['item_class']);?> <?php echo esc_attr((isset($pricing_meta->_cms_best_value) && $pricing_meta->_cms_best_value==true)?'best-value':'');?>">
                  <h3 class="list-price-title"> <?php the_title();?></h3>
                  <span class="list-price-total"><strong><?php echo esc_attr($pricing_meta->_cms_unit); ?><?php echo esc_attr($pricing_meta->_cms_price); ?></strong><?php echo esc_attr($pricing_meta->_cms_time); ?></span>
                  <ul class="description">
                        <?php if(!empty($pricing_meta->_cms_option1)): ?>
                            <li><?php echo esc_attr($pricing_meta->_cms_option1);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option2)): ?>
                            <li><?php echo esc_attr($pricing_meta->_cms_option2);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option3)): ?>
                           <li><?php echo esc_attr($pricing_meta->_cms_option3);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option4)): ?>
                           <li><?php echo esc_attr($pricing_meta->_cms_option4);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option5)): ?>
                            <li><?php echo esc_attr($pricing_meta->_cms_option5);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option6)): ?>
                            <li><?php echo esc_attr($pricing_meta->_cms_option6);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option7)): ?>
                            <li><?php echo esc_attr($pricing_meta->_cms_option7);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option8)): ?>
                           <li><?php echo esc_attr($pricing_meta->_cms_option8);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option9)): ?>
                           <li><?php echo esc_attr($pricing_meta->_cms_option9);?></li>
                        <?php endif; ?>
                        <?php if(!empty($pricing_meta->_cms_option10)): ?>
                            <li><?php echo esc_attr($pricing_meta->_cms_option10);?></li>
                        <?php endif; ?>
                  </ul>
                  <div class="border-color"></div>
                  <?php 
                  $buttontext='';
                  $urllink='';
                  $buttontext = !empty($pricing_meta->_cms_button_text)? $pricing_meta->_cms_button_text: 'buy now';
                  $urllink = !empty($pricing_meta->_cms_button_link)? $pricing_meta->_cms_button_link: get_post_permalink();
                  ?>
                  <a href="<?php echo esc_url($urllink); ?>" class="btn btn-default"><?php echo esc_attr($buttontext); ?></a> 
			</li>
            
            <?php
        }
        ?>
    </ul>
    
</div>