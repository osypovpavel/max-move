<div class="cms-carousel-home-clients">
<?php if(isset($atts['border_top'])):?>
<div class="border-color"></div>
<?php endif;?>
<div class="cms-carousel list-clients <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>"> 
    <?php
    $posts = $atts['posts'];
    while($posts->have_posts()){
        $posts->the_post();
        $team_meta=wp_maxclean_post_meta_data();
        ?>
        <div class="cms-team-item list-clients-item">
            <?php 
                if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false)):
                    $class = ' has-thumbnail';
                    $thumbnail = get_the_post_thumbnail(get_the_ID(),'original');
                else:
                    $class = ' no-image';
                    $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                endif;
                //echo '<div class="cms-grid-media '.esc_attr($class).'">'.$thumbnail.'</div>';
                echo ''.$thumbnail;
            ?>
           
        </div>
        <?php
    }
    ?>
</div>
</div>