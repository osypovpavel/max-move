<div class="layout2-testimonials cms-carousel <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php
    $posts = $atts['posts'];
    while($posts->have_posts()){
        $posts->the_post();
        $testimonial_meta=wp_maxclean_post_meta_data();
        ?>
        <div class="cms-carousel-item">
           
                
                <div class="media-image  ">
                   
                </div>
                    
                <div class="cms-carousel-content slider-reviews-quote">
                    <i class="icon fa fa-quote-left"></i>
                    <blockquote>
                    <?php the_content();?>
                    </blockquote>
                    <div class="round-figure"> 
                     <?php
                        if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'related-img', false)):
                            $class = ' has-thumbnail';
                            $thumbnail = get_the_post_thumbnail(get_the_ID(),'thumbnail');
                        else:
                            $class = ' no-image';
                            $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                        endif;
                        echo '<div class="cms-grid-media round-figure_mod-a '.esc_attr($class).'">'.$thumbnail.'</div>';
                    ?>
                    </div>
                    <span class="slider-reviews-autor">-- <?php the_title();?></span><span class="slider-reviews-address"><?php echo esc_attr($testimonial_meta->_cms_testimonial_position) ?></span> 
                </div>
             
             
        </div>
        <?php
    }
    ?>
</div>