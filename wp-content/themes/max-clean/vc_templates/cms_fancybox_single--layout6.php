<?php 
    $icon_name = "icon_" . $atts['icon_type'];
    $iconClass = isset($atts[$icon_name])?$atts[$icon_name]:'';
    $align_class= (isset($atts['align']) && $atts['align']!='')?'advantages-list_right':'advantages-list_left';
?>
<div class="home-fancyboxes-wraper cms-fancyboxes-wraper layout6 <?php echo esc_attr($align_class);?> <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>">

    <div class="cms-fancyboxes-body " onclick="">
        <div class="cms-fancybox-item">
            <?php if(isset($atts['title']) && $atts['title']!=''):?>
                <h3><?php echo apply_filters('the_title',$atts['title']);?></h3>
            <?php endif;?>
            <?php if(isset($atts['description_item'])): ?>
                <div class="fancy-box-content">
                    <?php echo apply_filters('the_content',$atts['description']);?>
                </div>
            <?php endif; ?>
            <?php 
            $image_url = '';
            if (!empty($atts['image'])) {
                $attachment_image = wp_get_attachment_image_src($atts['image'], 'full');
                $image_url = $attachment_image[0];
            }
            ?>
            <?php if($image_url):?>
            <div class="fancy-box-image">
                <img src="<?php echo esc_url($image_url);?>" />
            </div>
            <?php else:?>
            <div class="fancy-box-icon">
                <i class="<?php echo esc_attr($iconClass);?>"></i>
            </div>
            <?php endif;?>
            <div class="cms-fancyboxes-head">
                <?php if(isset($atts['title_item']) && $atts['title_item']!=''):?>
                    <div class="cms-fancyboxes-title">
                        <?php echo apply_filters('the_title',$atts['title_item']);?>
                    </div>
                <?php endif;?>
                <?php if(isset($atts['description_item']) && $atts['description_item']!=''):?>
                    <div class="cms-fancyboxes-description">
                        <?php echo apply_filters('the_content',$atts['description_item']);?>
                    </div>
                <?php endif;?>
            </div>
            
            <?php if(isset($atts['button_text']) && !empty($atts['button_text'])):?>
                <div class="cms-fancyboxes-foot">
                    <?php
                    $class_btn = ($atts['button_type']=='button')?'btn btn-default btn-large':'';
                    ?>
                    <a href="<?php echo esc_url($atts['button_link']);?>" class="<?php echo esc_attr($class_btn);?>"><?php echo esc_attr($atts['button_text']);?></a>
                </div>
            <?php endif;?>
        </div>
    </div>
     
</div>