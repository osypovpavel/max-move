<?php 
$radcls= 'bg'.rand();
?>
<div class="cms-fancyboxes-wraper list-contacts <?php echo esc_attr($atts['template']);?> <?php echo ' '.esc_attr($radcls);?>"  id="<?php echo esc_attr($atts['html_id']);?>" data-icon-color="<?php echo !empty($atts['icon_color'])? $atts['icon_color']:'';?>" >
     <?php if(isset($atts['title']) && $atts['title']!=''):?>
        <div class="cms-fancyboxes-head">
            <div class="cms-fancyboxes-title">
                <?php echo apply_filters('the_title',$atts['title']);?>
            </div>
            <div class="cms-fancyboxes-description">
                <?php echo apply_filters('the_content',$atts['description']);?>
            </div>
        </div>
    <?php endif;?>
    
    <?php if(isset($atts['title_item']) && $atts['title_item']!=''):?>
        <i class="icon icon_mod-b"></i>
        <span class="list-contacts__title"><?php echo apply_filters('the_title',$atts['title_item']);?></span>
    <?php endif;?>
    <div class="border-color"></div>
    <?php
    $output = '
    	<div class="contact-info">
    		<div class="wpb_wrapper">
    			' . wpb_js_remove_wpautop( $content, true ) . '
    		</div>
    	</div>
    ';
    ?>
    <span class="list-contacts__text">
        <?php echo ''.$output;?>
    </span> 
    <?php if(isset($atts['button_text']) && $atts['button_text']!=''):?>
        <div class="cms-fancyboxes-foot">
            <?php
            $class_btn = ($atts['button_type']=='button')?'btn btn-default btn-large':'';
            ?>
            <a href="<?php echo esc_url($atts['button_link']);?>" class="<?php echo esc_attr($class_btn);?>"><?php echo esc_attr($atts['button_text']);?></a>
        </div>
    <?php endif;?>     
</div>
 