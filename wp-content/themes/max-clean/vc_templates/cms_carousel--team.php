<div class="cms-carousel home-team-carousel <?php echo esc_attr($atts['template']);?><?php echo (isset($atts['nav']) && $atts['nav']=='true')?' show-nav':'';?><?php echo (isset($atts['dots']) && $atts['dots']=='true')?' show-dots':'';?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php
    $posts = $atts['posts'];
    while($posts->have_posts()){
        $posts->the_post();
        $team_meta=wp_maxclean_post_meta_data();
        ?>
        <div class="cms-team-item slide">
            <?php 
                if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false)):
                    $class = ' has-thumbnail';
                    $thumbnail = get_the_post_thumbnail(get_the_ID(),'wp_maxclean_square-thumb');
                else:
                    $class = ' no-image';
                    $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                endif;
                echo '<div class="cms-grid-media '.esc_attr($class).'">'.$thumbnail.'</div>';
            ?>
           
          <span class="cms-carousel-title"><?php the_title();?></span>
          <div class="border-color"></div>
          <span class="cms-carousel-position"><?php echo esc_attr($team_meta->_cms_team_position) ?></span>
          <ul class="social-links">
                <?php if(!empty($team_meta->_cms_social_1)):?>
                    <li class="social-links-item"><a class="social-links-link" href="<?php echo esc_attr($team_meta->_cms_social_1); ?>"><i class="<?php echo esc_attr($team_meta->_cms_social_1_icon); ?>"></i></a></li>
                <?php endif; ?>
                <?php if(!empty($team_meta->_cms_social_2)):?>
                    <li class="social-links-item"><a class="social-links-link" href="<?php echo esc_attr($team_meta->_cms_social_2); ?>"><i class="<?php echo esc_attr($team_meta->_cms_social_2_icon); ?>"></i></a></li>
                <?php endif; ?>
                <?php if(!empty($team_meta->_cms_social_3)):?>
                    <li class="social-links-item"><a class="social-links-link" href="<?php echo esc_attr($team_meta->_cms_social_3); ?>"><i class="<?php echo esc_attr($team_meta->_cms_social_3_icon); ?>"></i></a></li>
                <?php endif; ?>
                <?php if(!empty($team_meta->_cms_social_4)):?>
                    <li class="social-links-item"><a class="social-links-link" href="<?php echo esc_attr($team_meta->_cms_social_4); ?>"><i class="<?php echo esc_attr($team_meta->_cms_social_4_icon); ?>"></i></a></li>
                <?php endif; ?>
                <?php if(!empty($team_meta->_cms_social_5)):?>
                    <li class="social-links-item"><a class="social-links-link" href="<?php echo esc_attr($team_meta->_cms_social_5); ?>"><i class="<?php echo esc_attr($team_meta->_cms_social_5_icon); ?>"></i></a></li>
                <?php endif; ?>
                <?php if(!empty($team_meta->_cms_social_6)):?>
                    <li class="social-links-item"><a class="social-links-link" href="<?php echo esc_attr($team_meta->_cms_social_6); ?>"><i class="<?php echo esc_attr($team_meta->_cms_social_6_icon); ?>"></i></a></li>
                <?php endif; ?>
          </ul>
        </div>
        <?php
    }
    ?>
</div>