<?php 
    $icon_name = "icon_" . $atts['icon_type'];
    $iconClass = isset($atts[$icon_name])?$atts[$icon_name]:'';
?>
<div class="cms-fancyboxes-wraper cms-fancyboxes-wraper-scheme clearfix <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php if(isset($atts['title']) && $atts['title']!=''):?>
        <div class="cms-fancyboxes-head">
            <div class="cms-fancyboxes-title">
                <?php echo apply_filters('the_title',$atts['title']);?>
            </div>
            <div class="cms-fancyboxes-description">
                <?php echo apply_filters('the_content',$atts['description']);?>
            </div>
        </div>
    <?php endif;?>
    <div class="cms-fancyboxes-body clearfix">
        <div class="cms-fancybox-item list-scheme-item">
            <?php 
            $image_url = '';
            if (!empty($atts['image'])) {
                $attachment_image = wp_get_attachment_image_src($atts['image'], 'full');
                $image_url = $attachment_image[0];
            }
            ?>
            <a href="<?php echo esc_url($atts['button_link']);?>" class="list-scheme-link">
                    <?php if($image_url):?>
                        <div class="fancy-box-image">
                            <img src="<?php echo esc_url($image_url);?>" />
                        </div>
                    <?php else:?>
                        <div class="fancy-box-icon">
                            <i class="<?php echo esc_attr($iconClass);?> helper"></i>
                        </div>
                    <?php endif;?>
    				<span class="list-scheme-number"><?php echo esc_attr($atts['scheme_number']) ? esc_attr($atts['scheme_number']):'0';?></span>
                    <?php if(isset($atts['title_item'])):?>
                        <div class="list-scheme-title"><?php echo apply_filters('the_title',$atts['title_item']);?></div>
                    <?php endif;?>
			</a> 
            <?php if(isset($atts['show_arrow'])):?>
			<span class="arrow"><i class="arrow-inner fa fa-angle-right"></i> </span> 
            <?php endif;?>
            <?php if(isset($atts['button_text']) && $atts['button_text']!=''):?>
                <div class="cms-fancyboxes-foot">
                    <?php
                    $class_btn = ($atts['button_type']=='button')?'btn btn-default btn-large':'';
                    ?>
                    <a href="<?php echo esc_url($atts['button_link']);?>" class="<?php echo esc_attr($class_btn);?>"><?php echo esc_attr($atts['button_text']);?></a>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
