<?php 
    /* get categories */
        $taxo = 'category';
        $_category = array();
        if(!isset($atts['cat']) || $atts['cat']==''){
            $terms = get_terms($taxo);
            foreach ($terms as $cat){
                $_category[] = $cat->term_id;
            }
        } else {
            $_category  = explode(',', $atts['cat']);
        }
        
        $atts['categories'] = $_category;
       
        $posts = $atts['posts'];
        $porst_per_page = $posts->query_vars['posts_per_page'];
        $total_page = $posts->found_posts;
        
        wp_enqueue_style( 'wp-mediaelement' );
        wp_enqueue_script( 'wp-mediaelement' );
        
         
        wp_register_script( 'cms-loadmore-js', get_template_directory_uri().'/assets/js/cms_loadmore.js', array('jquery') ,'1.0',true);
        // What page are we on? And what is the pages limit?
        global $wp_query;
        $max = $wp_query->max_num_pages;
        $limit = $atts['limit'];
        $paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
        // Add some parameters for the JS.
        $current_id =  str_replace('-','_',$atts['html_id']);
        wp_localize_script(
            'cms-loadmore-js',
            'cms_more_obj'.$current_id,
            array(
                'startPage' => $paged,
                'maxPages' => $max,
                'total' => $wp_query->found_posts,
                'perpage' => $limit,
                'nextLink' => next_posts($max, false),
                'masonry' => $atts['layout']
            )
        );
        wp_enqueue_script( 'cms-loadmore-js' );
        
        $grid_animate = isset($atts['grid_home_animation']) ? $atts['grid_home_animation'] : '';
        $dwdu = (isset($grid_data_wow_duration) and $grid_data_wow_duration!='')?'data-wow-duration="'.esc_attr($grid_data_wow_duration).'"':''; 
?>
<div class="cms-grid-wraper cms-grid-wraper-smoving <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>" data-btntext="<?php echo esc_html__('Read More', 'wp-maxclean');?>">
    <?php if($atts['filter']=="true" and $atts['layout']=='masonry'):?>
        <div class="cms-grid-filter">
            <ul class="cms-filter-category list-unstyled list-inline">
                <li><a class="active" href="#" data-group="all">All</a></li>
                <?php 
                if(is_array($atts['categories']))
                foreach($atts['categories'] as $category):?>
                    <?php $term = get_term( $category, $taxo );?>
                    <li><a href="#" data-group="<?php echo esc_attr('category-'.$term->slug);?>">
                            <?php echo esc_attr($term->name);?>
                        </a>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    <?php endif;?>
    <div class="row cms-grid cms-grid-item-smoving <?php echo esc_attr($atts['grid_class']);?>">
        <?php
        $posts = $atts['posts'];
        $size = ($atts['layout']=='basic')?'wp_maxclean_medium-thumb':'medium';
        $count = 0;
        $delay = 0;
        while($posts->have_posts()){
            $posts->the_post();
            $customfields = get_post_custom(get_the_ID());
            $groups = array();
            $groups[] = '"all"';
            $count ++;
            $delay += 0.5;
            foreach(cmsGetCategoriesByPostID(get_the_ID(),$taxo) as $category){
                
                $groups[] = '"category-'.$category->slug.'"';
            }
            ?>
            <div data-wow-delay="<?php echo esc_attr($delay).'s'; ?>" <?php echo ''.$dwdu;?> class="cms-grid-item <?php echo esc_attr($atts['item_class']);?> <?php echo ''.$grid_animate;?>" data-groups='[<?php echo implode(',', $groups);?>]'>
                <div class="grid-item-inner">
                <?php 
                    if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $size, false)):
                        $class = ' has-thumbnail';
                        $thumbnail = get_the_post_thumbnail(get_the_ID(),$size);
                    else:
                        $class = ' no-image';
                        $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                    endif;
                    echo '<div class="cms-grid-media '.esc_attr($class).'">'.$thumbnail.'</div>';
                ?>
                <div class="grid-services-info"> 
                    <div class="cms-grid-title">
                        <?php the_title();?>
                    </div>
                    <div class="cms-grid-subtitle">
                        <?php echo ''.$customfields['subtitle'][0]; ?>
                    </div> 
                </div>
                </div>
            </div>
            
            <?php
        }
        ?>
    </div>
    <?php if(isset($atts['show_readmore']) && $atts['show_readmore']):?> 
    <div class="loadMore text-center">
        <div class="cms_pagination"></div>
    </div>
    <?php endif;?>
</div>
 