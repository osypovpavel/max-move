<div class="cms-carousel about-service-carousel <?php echo esc_attr($atts['template']);?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php
    $posts = $atts['posts'];
    while($posts->have_posts()){
        $posts->the_post();
        $customfields = get_post_custom(get_the_ID());
        ?>
         <div class="cms-grid-item">
                <?php 
                    if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'wp_maxclean_medium-thumb', false)):
                        $class = ' has-thumbnail';
                        $thumbnail = get_the_post_thumbnail(get_the_ID(),'wp_maxclean_medium-thumb');
                        $image_src = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'original') );
                    else:
                        $class = ' no-image';
                        $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                        $image_src = get_template_directory_uri().'/assets/images/no-image.jpg';
                    endif;
                    echo '<div class="cms-grid-media '.esc_attr($class).'">'.'<a href="'.esc_url($image_src).'" data-rel="prettyPhoto[gallery_home_clean]">'.'<span class="slide-bg"><i class="icon fa fa-search"></i></span>'.$thumbnail.'</a></div>';
                ?>
                
            </div>
        <?php
    }
    ?>
</div>