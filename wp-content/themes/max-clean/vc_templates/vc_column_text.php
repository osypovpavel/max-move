<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $css_animation
 * @var $css
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Column_text
 */
 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$class_to_filter = 'wpb_text_column wpb_content_element ' . $this->getCSSAnimation( $css_animation );
$class_to_filter .= vc_shortcode_custom_css_class( $css, ' ' ) . $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts );
 
$style='style="';
$column_text_style_waper=$atts['wrap_text_width']!=''?'max-width:'.$atts['wrap_text_width'].'; margin: auto;':'';
$fontzise = $atts['fontsize']!=''?' font-size: '.$atts['fontsize'].';':'';
$fontweight = $atts['font_weight']!=''?' font-weight: '.$atts['font_weight'].';':'';
$style.=$column_text_style_waper.$fontzise.$fontweight;
$style.='"';

$output = '
	<div class="' . esc_attr( $css_class ) . '">
		<div class="wpb_wrapper" '.$style.'>
			' . wpb_js_remove_wpautop( $content, true ) . '
		</div>
	</div>
';

echo ''.$output;
?>
