<div class="cms-carousel home-service-carousel <?php echo esc_attr($atts['template']);?><?php echo (isset($atts['nav']) && $atts['nav']=='true')?' show-nav':'';?><?php echo (isset($atts['dots']) && $atts['dots']=='true')?' show-dots':'';?>" id="<?php echo esc_attr($atts['html_id']);?>">
    <?php
    $posts = $atts['posts'];
    while($posts->have_posts()){
        $posts->the_post();
        $customfields = get_post_custom(get_the_ID());
        ?>
        <div class="cms-carousel-item">
            <?php 
                if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false)):
                    $class = ' has-thumbnail';
                    $thumbnail = get_the_post_thumbnail(get_the_ID(),'wp_maxclean_medium-thumb');
                else:
                    $class = ' no-image';
                    $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                endif;
                echo '<div class="cms-grid-media '.esc_attr($class).'">'.$thumbnail.'</div>';
            ?>
            <div class="slider-services-info">
                <div class="cms-carousel-title">
                    <?php the_title();?>
                </div>
                <?php if(isset($customfields['subtitle'][0]) && $customfields['subtitle'][0]!=''):?>
                <div class="cms-carousel-subtitle">
                    <?php echo esc_attr($customfields['subtitle'][0]); ?>
                </div>
                <?php endif;?>
            </div>
        </div>
        <?php
    }
    ?>
</div>