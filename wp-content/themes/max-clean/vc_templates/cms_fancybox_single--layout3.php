<?php 
    $icon_name = "icon_" . $atts['icon_type'];
    $iconClass = isset($atts[$icon_name])?$atts[$icon_name]:'';
    $style='style="';
    $bgcolor=isset($atts['background_color'])?' background-color:'.esc_attr($atts['background_color']).';':'';
    $font_style = isset($atts['font_color'])? ' color:'.esc_attr($atts['font_color']).';':'';
    $style.=$bgcolor.$font_style;
    $style.='"';
    
?>
<div class="cms-fancyboxes-wraper cms-fancyboxes-wraper-bgcolor <?php echo esc_attr($atts['template']);?>" data-border-after-color="<?php echo !empty($atts['border_after_color'])? $atts['border_after_color']:'';?>" id="<?php echo esc_attr($atts['html_id']);?>" <?php echo ''.$style;?>>
    <?php if(isset($atts['title']) && $atts['title']!=''):?>
        <div class="cms-fancyboxes-head">
            <div class="cms-fancyboxes-title">
                <?php echo apply_filters('the_title',$atts['title']);?>
            </div>
            <div class="cms-fancyboxes-description">
                <?php echo apply_filters('the_content',$atts['description']);?>
            </div>
        </div>
    <?php endif;?>
    <div class="cms-fancyboxes-body">
        <div class="cms-fancybox-item">
            <?php 
            $image_url = '';
            if (!empty($atts['image'])) {
                $attachment_image = wp_get_attachment_image_src($atts['image'], 'full');
                $image_url = $attachment_image[0];
            }
            ?>
            <?php if($image_url):?>
            <div class="fancy-box-image">
                <img src="<?php echo esc_attr($image_url);?>" />
            </div>
            <?php else:?>
            <div class="fancy-box-icon">
                <i class="<?php echo esc_attr($iconClass);?>"></i>
            </div>
            <?php endif;?>
            <?php if(isset($atts['title_item']) && $atts['title_item']!=''):?>
                <div class="list-pennant-title"><?php echo apply_filters('the_title',$atts['title_item']);?></div>
            <?php endif;?>
            <?php if(isset($atts['description_item']) && $atts['description_item']!=''):?>
            <div class="fancy-box-content">
                <?php echo apply_filters('the_content',$atts['description_item']);?>
            </div>
            <?php endif; ?>
            <?php if(isset($atts['button_text']) && $atts['button_text']!=''):?>
                <div class="cms-fancyboxes-foot">
                    <?php
                    $class_btn = ($atts['button_type']=='button')?'btn btn-large':'';
                    ?>
                    <a href="<?php echo esc_url($atts['button_link']);?>" class="<?php echo esc_attr($class_btn);?>"><?php echo esc_attr($atts['button_text']);?></a>
                </div>
            <?php endif;?>
        </div>
    </div>
    
</div>
 