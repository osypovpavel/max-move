<?php
/**
 * @var $this WPBakeryShortCode_VC_Column
 */

$output = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$el_class = $this->getExtraClass( $el_class );
$width = wpb_translateColumnWidthToSpan( $width );
$width = vc_column_offset_class_merge( $offset, $width );

$col_animate = isset($col_animation) ? $col_animation : '';
$css_classes = array(
	$this->getExtraClass( $el_class ),
	'wpb_column',
	'vc_column_container',
    $col_align,
	$width,
	$col_animate,
	vc_shortcode_custom_css_class( $css ),
);

$dwdu = (isset($col_data_wow_duration) and $col_data_wow_duration!='')?'data-wow-duration="'.$col_data_wow_duration.'"':''; 
$dwd = (isset($data_col_delay) and $data_col_delay!='')?' data-wow-delay="'.$data_col_delay.'" ':''; 
  
$wrapper_attributes = array();
$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';

$output .= '<div '.$dwdu.$dwd . implode( ' ', $wrapper_attributes ) . '>';
$output .= '<div class="vc_column-inner ' . esc_attr( trim( vc_shortcode_custom_css_class( $css ) ) ) . '">';
$output .= '<div class="wpb_wrapper">';
$output .= wpb_js_remove_wpautop( $content );
$output .= '</div>' . $this->endBlockComment( '.wpb_wrapper' );
$output .= '</div>';
$output .= '</div>' . $this->endBlockComment( $this->getShortcode() );

echo ''.$output;
