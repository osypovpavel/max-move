<?php
/**
 * The Template for displaying all single portfolio
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
global $maxclean_base;
$portfolio_meta=wp_maxclean_post_meta_data();
get_header(); 
$taxo = 'portfolio-category';
if(is_active_sidebar('sidebar-4')) $class='col-sm-8 col-md-8 col-lg-8';
else $class='col-sm-12 col-md-12 col-lg-12';
$vd_class='hide-video-download';
?>
<?php
if((isset($portfolio_meta->_cms_show_video_button) && $portfolio_meta->_cms_show_video_button=='1') || (isset($portfolio_meta->_cms_show_download_file_button) && $portfolio_meta->_cms_show_download_file_button=='1')):
$vd_class='';
?>
<div class="block-btns">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
            <?php if(isset($portfolio_meta->_cms_show_video_button) && $portfolio_meta->_cms_show_video_button=='1'):?>
            <a href="<?php echo esc_url($portfolio_meta->_cms_video_url);?>" rel="prettyPhoto" class="btn btn-default popup-video"><i class="icon icon-play fa fa-play"></i> <?php esc_html_e( 'WATCH THE AD', 'wp-maxclean' );?></a>
            <?php endif;?>
            <?php if(isset($portfolio_meta->_cms_show_download_file_button) && $portfolio_meta->_cms_show_download_file_button=='1'):?>
            <a href="<?php echo esc_url( $portfolio_meta->_cms_file_url); ?>" target="_blank" class="btn btn-default"><?php esc_html_e( 'Download Info files', 'wp-maxclean' );?><i class="icon icon-arrow fa fa-angle-down"></i></a>
            <?php endif;?> 
        </div>
      </div>
    </div>
</div>
<?php endif;?>

    <div class="row-portfolio <?php echo esc_attr($vd_class); ?>">
        <div class="container">
    		<div class="row">     
                <?php //if(is_active_sidebar('sidebar-4')):?>
                <?php //<div id="secondary-left" class="col-xs-12 col-sm-4 col-md-4 col-lg-4">?>
                     <?php //dynamic_sidebar('sidebar-4'); ?>
                <?php //</div>?>
                <?php //endif;?>
                <?php// echo esc_attr($class); ?>
                <div id="primary" class="col-xs-12">
                    <style>
                        p{
                            font-size: 16px;
                            margin-bottom: 5px!important;
                            line-height: 1.2!important;
                        }
                    </style>
                    <div id="content" class="portfolio-page" role="main">
        
                        <?php while ( have_posts() ) : the_post(); ?>
        
                            <?php get_template_part( 'single-templates/single-portfolio/content', get_post_format() ); ?>
         
                        <?php endwhile; // end of the loop. ?>
        
                    </div><!-- #content -->
                </div><!-- #primary -->
            </div>
        </div>
    </div>

<?php if(is_active_sidebar('sidebar-13') || is_active_sidebar('sidebar-14') || is_active_sidebar('sidebar-15')):?>
<?php //<div class="contact-footer-page triagl triagl-top triagl-secondary vc_row-fluid portfolio-footer-page" >?>
    <div class="contact-footer-page triagl-top triagl-secondary vc_row-fluid portfolio-footer-page" style="padding: 0;">
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">-->
<!--                  --><?php //dynamic_sidebar('sidebar-13'); ?>
<!--            </div>-->
<!---->
<!--            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">-->
<!--                  --><?php //dynamic_sidebar('sidebar-14'); ?>
<!--            </div>-->
<!---->
<!--            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">-->
<!--                  --><?php //dynamic_sidebar('sidebar-15'); ?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>
<?php endif;?>
<?php get_footer(); ?>