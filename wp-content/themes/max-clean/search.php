<?php
/**
 * The template for displaying Search Results pages
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */

get_header(); ?>
<div class="container">
    <div class="row blog-search">

        <section id="primary" class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <div id="content" role="main">

            <?php if ( have_posts() ) : ?>

                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'single-templates/content/content' ); ?>
                <?php endwhile; ?>

                <?php wp_maxclean_paging_nav(); ?>

            <?php else : ?>

                <article id="post-0" class="post no-results not-found section_searce-page">
                    <header class="entry-header">
                        <h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'wp-maxclean' ); ?></h1>
                    </header>

                    <div class="entry-content">
                        <p><?php esc_html_e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'wp-maxclean' ); ?></p>
                        <?php get_search_form(); ?>
                    </div><!-- .entry-content -->
                </article><!-- #post-0 -->

            <?php endif; ?>

            </div><!-- #content -->
        </section><!-- #primary -->
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?php get_sidebar(); ?>
        </div>

    </div>

</div>
<?php if(is_active_sidebar('sidebar-13') || is_active_sidebar('sidebar-14') || is_active_sidebar('sidebar-15')):?>
<div class="contact-footer-page triagl triagl-top triagl-secondary vc_row-fluid " >
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
                <?php dynamic_sidebar('sidebar-13'); ?>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
                <?php dynamic_sidebar('sidebar-14'); ?>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column vc_column_container    ">
                <?php dynamic_sidebar('sidebar-15'); ?>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<?php get_footer(); ?>