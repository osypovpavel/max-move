<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
global $smof_data, $maxclean_meta;
 
?>
    </div><!-- #main --> 
    <?php if(!empty($smof_data['enable_footer_top']) =='1' || !empty($smof_data['enable_footer_bottom']) =='1'): ?>
		<footer class="footer">
            <?php if (!empty($smof_data['enable_footer_top']) =='1'): ?>
                <div id="cshero-footer-top">
                    <div class="container">
                    <?php if(!empty($maxclean_meta->_cms_enable_footer_top_head) == '1'): ?>
                        <?php if(!empty($smof_data['footer_botton_back_to_top'])==1):?>
                          <div class="row">
                            <div class="col-xs-12">
                            <div id="back_to_top" class="back_to_top"><span class="go_up"><a href="javascript:void(0);"><i class="flaticon-cleaning11"></i><strong>BACK TO TOP</strong></a></span></div>
                            </div>
                          </div>
                        <?php endif;?>
                        <?php if(!empty($smof_data['enable_footer_top_menu_row'])==1):?>
                          <div class="row footer-1">
                            <div class="col-md-5">
                            <nav id="footer-navigation" class="footer-navigation">
                                <?php
                                $attr = array(
                                    'menu' =>wp_maxclean_menu_location(),
                                    'menu_class' => 'nav-menu menu-footer-menu',
                                );
                                
                                $menu_locations = get_nav_menu_locations();
                                
                                if(!empty($menu_locations['second'])){
                                    $attr['theme_location'] = 'second';
                                }
                                wp_nav_menu( $attr ); ?>
                            </nav>
                             
                            </div>
                            <div class="col-md-2"><?php dynamic_sidebar('sidebar-5'); ?></div>
                            <div class="col-md-5"><?php dynamic_sidebar('sidebar-6'); ?></div>
                          </div>
                          <?php endif;?>
                        <?php endif;?>
                          <div class="row footer-2">
                            <div class="col-md-4">
                                <?php dynamic_sidebar('sidebar-7'); ?>
                            </div>
                            <div class="col-md-4">
                                <?php dynamic_sidebar('sidebar-8'); ?>
                               
                            </div>
                            <div class="col-md-4">
                                <?php dynamic_sidebar('sidebar-9'); ?>
                            </div>
                          </div>
                    </div>
                </div>
            <?php endif;?>
            <?php if (!empty($smof_data['enable_footer_bottom']) =='1'): ?>
                <div id="cshero-footer-bottom">
                    <div class="container" style="padding: 5px 15px; margin-top: 15px">
                        <div class="row">
                            <div class="copyright">
                                <?php echo ''.$smof_data['footer_bottom_content'];?> 
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
             
		</footer><!-- #site-footer -->
        <?php endif;?>
	</div><!-- #page -->
	<?php wp_footer(); ?>
</body>
</html>