<?php
/**
 * Page title template
 * @since 1.0.0
 * @author Fox
 */
function wp_maxclean_page_title(){
    global $smof_data, $maxclean_meta, $maxclean_base;
    
    /* page options */
    if(is_page() && isset($maxclean_meta->_cms_page_title) && $maxclean_meta->_cms_page_title){
        if(isset($maxclean_meta->_cms_page_title_type)){
            $smof_data['page_title_layout'] = $maxclean_meta->_cms_page_title_type;
        }
    }
    
    if(!empty($smof_data['page_title_layout'])){
        ?>
        <div id="page-title" class="page-title">
            <div class="container">
            <div class="row">
			<?php 
				/*
				* Get content of breadcrumb and check to echo data if it not null
				*/
				ob_start();
				$maxclean_base->wp_maxclean_getBreadCrumb();
				$br = ob_get_clean();
			?>
            <?php switch ($smof_data['page_title_layout']){
                case '1':
                    ?>
                    <div id="page-title-text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h1><?php $maxclean_base->wp_maxclean_getPageTitle(); ?></h1></div>
                    <div id="breadcrumb-text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 <?php echo (trim(strip_tags($br)) == '')?' empty-breadcrumb':'' ?>"><?php $maxclean_base->wp_maxclean_getBreadCrumb(); ?></div>
                    <?php
                    break;
                case '2':
                    ?>
                    <div id="page-title-text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h1><?php $maxclean_base->wp_maxclean_getPageTitle(); ?></h1></div>
                    <div id="breadcrumb-text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 <?php echo (trim(strip_tags($br)) == '')?' empty-breadcrumb':'' ?>"><?php $maxclean_base->wp_maxclean_getBreadCrumb(); ?></div>
                    <?php          
                    break;
                 
            } ?>
            </div>
            </div>
            <?php if($smof_data['page_title_layout']==1):?>
                <div class="border-title-page">
                    <div class="border-wave border-wave_primary "></div>
                </div>
            <?php endif;?>
            <?php if($smof_data['page_title_layout']==2):?>
                <div class="triagl triagl-btm triagl-primary"></div>
            <?php endif;?>
        </div><!-- #page-title -->
        <?php
    }
}
/**
 * Page Information foot template
 * @since 1.0.0
 * @author Knight
 */
function wp_maxclean_page_info_foot(){
    global $smof_data, $maxclean_meta, $maxclean_base;
    
    /* page options */
    
    $wow_duration ='';
    $animate_class='';
    if(is_page() && isset($maxclean_meta->_cms_page_info_foot_type) && $maxclean_meta->_cms_page_info_foot_type){
        if(isset($maxclean_meta->_cms_page_info_foot_type)){
            $smof_data['page_info_foot_layout'] = $maxclean_meta->_cms_page_info_foot_type;
        }
        if(isset($maxclean_meta->_cms_is_animation) && $maxclean_meta->_cms_is_animation){
            $wow_duration ='data-wow-duration="2s"';
            $animate_class= ' max_animate wow bounceInUp';
        }
    }
    
    if($smof_data['page_info_foot_layout']){
        ?>
            <?php 
           // var_dump($smof_data['page_info_foot_layout']); die;
            switch ($smof_data['page_info_foot_layout']){
                case '2':
                    ?>
                    <div <?php echo ''.$wow_duration;?> class="contact-footer-page border-wave_secondary vc_row-fluid <?php echo ''.$animate_class;?>" >
                        <div class="container">
                    		<div class="row">        
                    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column">
                    				  <?php dynamic_sidebar('sidebar-13'); ?>
                    			</div> 
                    
                    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column">
                    				  <?php dynamic_sidebar('sidebar-14'); ?>
                    			</div> 
                    
                    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column">
                    				  <?php dynamic_sidebar('sidebar-15'); ?>
                    			</div> 
                    		</div>
                    	</div>            
                    </div>
                    <?php
                    break;
                case '3':
                    ?>
                   <div <?php echo ''.$wow_duration;?> class="contact-footer-page triagl triagl-top triagl-secondary vc_row-fluid <?php echo ''.$animate_class;?>" >
                        <div class="container">
                    		<div class="row">        
                    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column">
                    				  <?php dynamic_sidebar('sidebar-13'); ?>
                    			</div> 
                    
                    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column">
                    				  <?php dynamic_sidebar('sidebar-14'); ?>
                    			</div> 
                    
                    			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  wpb_column">
                    				  <?php dynamic_sidebar('sidebar-15'); ?>
                    			</div> 
                    		</div>
                    	</div>            
                    </div>
                    <?php          
                    break;
                 
            } ?>
            
        <?php
    }
}
 

/**
 * Get Header Layout.
 * 
 * @author Fox
 */
function wp_maxclean_header(){
    global $smof_data, $maxclean_meta;
    /* header for page */
    if(isset($maxclean_meta->_cms_header) && $maxclean_meta->_cms_header){
        if(isset($maxclean_meta->_cms_header_layout)){
            $smof_data['header_layout'] = $maxclean_meta->_cms_header_layout;
        }
    }
    /* load template. */
    get_template_part('inc/header/header', isset($smof_data['header_layout'])?$smof_data['header_layout']:'');
}

function wp_maxclean_main_menu(){
      
    $attr = array(
        'menu' =>wp_maxclean_menu_location(),
        'menu_class' => 'nav-menu menu-main-menu',
    );
    
    $menu_locations = get_nav_menu_locations();
    
    if(!empty($menu_locations['primary'])){
        $attr['theme_location'] = 'primary';
    }
    
    /* enable mega menu. */
    if(class_exists('HeroMenuWalker')){ $attr['walker'] = new HeroMenuWalker(); }
    
    /* main nav. */
    wp_nav_menu( $attr );  
}
/**
 * Get menu location ID.
 * 
 * @param string $option
 * @return NULL
 */
function wp_maxclean_menu_location($option = '_cms_primary'){
    global $maxclean_meta;
    /* get menu id from page setting */
    return (isset($maxclean_meta->$option) && $maxclean_meta->$option) ? $maxclean_meta->$option : null ;
}

function wp_maxclean_get_page_loading() {
    global $smof_data, $maxclean_meta;
    
    
    if((isset($smof_data['enable_page_loadding']) && $smof_data['enable_page_loadding']==true ) && (isset($maxclean_meta->_cms_enable_page_loading) && $maxclean_meta->_cms_enable_page_loading)){ ?>   
      <div id="ip-container" class="ip-container"> 
       
        <header class="ip-header">
          <div class="ip-loader">
            <div class="text-center">
              <div class="ip-logo"> <img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/logo.png');?>" height="35" width="114" alt="logo"> </div>
            </div>
            <svg width="60px" height="60px" viewBox="0 0 80 80" class="ip-inner">
            <path d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z" class="ip-loader-circlebg"></path>
            <path id="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" class="ip-loader-circle"></path>
            </svg> </div>
        </header>
      </div>
  <?php
       /* echo '<div id="cms-loadding">';
        switch ($smof_data['page_loadding_style']){
            case '2':
                echo '<div class="ball"></div>';
                break;
            default:
                echo '<div class="loader"></div>';
                break;
        }
        echo '</div>';*/
    }
}

/**
 * Add page class
 * 
 * @author Fox
 * @since 1.0.0
 */
function wp_maxlean_page_class(){
    global $smof_data,$maxclean_meta;
     
    $page_class = '';
    /* check boxed layout */
    if($smof_data['body_layout']){
        $page_class = 'cs-boxed';
    } else {
        $page_class = 'cs-wide';
    }
    if(is_page() && isset($maxclean_meta->_cms_page_custom_class) && $maxclean_meta->_cms_page_custom_class){
        $page_class.= ' '.$maxclean_meta->_cms_page_custom_class;
    }
    
    echo apply_filters('wp_maxlean_page_class', $page_class);
}

/**
 * Add main class
 * 
 * @author Fox
 * @since 1.0.0
 */
function wp_maxclean_main_class(){
    global $maxclean_meta;
    
    $main_class = '';
    /* chect content full width */
    if(is_page() && isset($maxclean_meta->_cms_full_width) && $maxclean_meta->_cms_full_width){
        /* full width */
        $main_class = "no-container";
    } else {
        /* boxed */
        $main_class = "container";
    }
    
    echo apply_filters('wp_maxclean_main_class', $main_class);
}
 

/**
 * Archive detail
 * 
 * @author Fox
 * @since 1.0.0
 */
function wp_maxclean_archive_detail(){
    ?>
    <ul>
        <li class="detail-author"><?php esc_html_e('By', 'wp-maxclean'); ?> <?php the_author_posts_link(); ?></li>
        <?php if(has_category()): ?>
        <li class="detail-terms"><?php the_terms( get_the_ID(), 'category', '<i class="fa fa-sitemap"></i>', ' / ' ); ?></li>
        <?php endif; ?>
        <li class="detail-comment"><i class="fa fa-comments-o"></i><a href="<?php the_permalink(); ?>"><?php echo comments_number('0','1','%'); ?> <?php esc_html_e('Comments', 'wp-maxclean'); ?></a></li>
        <?php if(has_tag()): ?>
        <li class="detail-tags"><?php the_tags('<i class="fa fa-tags"></i>', ', ' ); ?></li>
        <?php endif; ?>
    </ul>
    <?php
}

/**
 * Archive readmore
 * 
 * @author Fox
 * @since 1.0.0
 */
function wp_maxclean_archive_readmore(){
    echo '<a class="btn btn-default" href="'.get_the_permalink().'" title="'.get_the_title().'" >'.esc_html__('Більше...', 'wp-maxclean').'</a>';
}

/**
 * Media Audio.
 * 
 * @param string $before
 * @param string $after
 */
function wp_maxclean_archive_audio() {
	global $maxclean_base;
    /* get shortcode audio. */
    $shortcode = $maxclean_base->wp_maxclean_getShortcodeFromContent('audio', get_the_content());
    
    if($shortcode != ''){
        echo do_shortcode($shortcode);
        
        return true;
        
    } else {
        if(has_post_thumbnail()){
            the_post_thumbnail();
        }
        
        return false;
    }
    
}

/**
 * Media Video.
 *
 * @param string $before
 * @param string $after
 */
function wp_maxclean_archive_video() {
    
    global $wp_embed, $maxclean_base;
    /* Get Local Video */
    $local_video = $maxclean_base->wp_maxclean_getShortcodeFromContent('video', get_the_content());
    
    /* Get Youtobe or Vimeo */
    $remote_video = $maxclean_base->wp_maxclean_getShortcodeFromContent('embed', get_the_content());
    
    if($local_video){
        /* view local. */
        echo do_shortcode($local_video);
        
        return true;
        
    } elseif ($remote_video) {
        /* view youtobe or vimeo. */
        echo do_shortcode($wp_embed->run_shortcode($remote_video));
        
        return true;
        
    } elseif (has_post_thumbnail()) {
        /* view thumbnail. */
        the_post_thumbnail();
    } else {
        return false;
    }
    
}

/**
 * Gallerry Images
 * 
 * @author Fox
 * @since 1.0.0
 */
function wp_maxclean_archive_gallery(){
	global $maxclean_base;
    /* get shortcode gallery. */
    $shortcode = $maxclean_base->wp_maxclean_getShortcodeFromContent('gallery', get_the_content());
    
    if($shortcode != ''){
        preg_match('/\[gallery.*ids=.(.*).\]/', $shortcode, $ids);
        
        if(!empty($ids)){
        
            $array_id = explode(",", $ids[1]);
            ?>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                <?php $i = 0; ?>
                <?php foreach ($array_id as $image_id): ?>
        			<?php
                    $attachment_image = wp_get_attachment_image_src($image_id, 'full', false);
                    if($attachment_image[0] != ''):?>
        				<div class="item <?php if( $i == 0 ){ echo 'active'; } ?>">
                    		<img style="width:100%;" data-src="holder.js" src="<?php echo esc_url($attachment_image[0]);?>" />
                    	</div>
                    <?php $i++; endif; ?>
                <?php endforeach; ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        		    <span class="fa fa-angle-left"></span>
        		</a>
        		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        		    <span class="fa fa-angle-right"></span>
        		</a>
        	</div>
            <?php
            
            return true;
        
        } else {
            if(has_post_thumbnail()){
                the_post_thumbnail();
            }
        }
    } else {
        if(has_post_thumbnail()){
            the_post_thumbnail();
        }
    }
}

/**
 * Quote Text.
 * 
 * @author Fox
 * @since 1.0.0
 */
function wp_maxclean_archive_quote() {
    /* get text. */
    preg_match('/\<blockquote\>(.*)\<\/blockquote\>/', get_the_content(), $blockquote);
    
    if(!empty($blockquote[0])){
        echo ''.$blockquote[0].'';
        return true;
    } else {
        if(has_post_thumbnail()){
            the_post_thumbnail();
        }
        return false;
    }
}

/**
 * Get icon from post format.
 * 
 * @return multitype:string Ambigous <string, mixed>
 * @author Fox
 * @since 1.0.0
 */
function wp_maxclean_archive_post_icon() {
    $post_icon = array('icon'=>'fa fa-file-text-o','text'=>__('STANDARD', 'wp-maxclean'));
    switch (get_post_format()) {
        case 'gallery':
            $post_icon['icon'] = 'fa fa-camera-retro';
            $post_icon['text'] = esc_html__('GALLERY', 'wp-maxclean');
            break;
        case 'link':
            $post_icon['icon'] = 'fa fa-external-link';
            $post_icon['text'] = esc_html__('LINK', 'wp-maxclean');
            break;
        case 'quote':
            $post_icon['icon'] = 'fa fa-quote-left';
            $post_icon['text'] = esc_html__('QUOTE', 'wp-maxclean');
            break;
        case 'video':
            $post_icon['icon'] = 'fa  fa-youtube-play';
            $post_icon['text'] = esc_html__('VIDEO', 'wp-maxclean');
            break;
        case 'audio':
            $post_icon['icon'] = 'fa fa-volume-up';
            $post_icon['text'] = esc_html__('AUDIO', 'wp-maxclean');
            break;
        default:
            $post_icon['icon'] = 'fa fa-image';
            $post_icon['text'] = esc_html__('STANDARD', 'wp-maxclean');
            break;
    }
    echo '<i class="'.$post_icon['icon'].'"></i>';
}
 
/**
 * Max Clean Animation lib
 */
function wp_maxclean_animate_lib() {
 $animate = array(
    esc_html__( 'None', 'wp-maxclean' ) => '',
    esc_html__( 'None', 'wp-maxclean' ) => '',
    esc_html__( 'FadeIn', 'wp-maxclean' ) => 'max_animate wow fadeIn',
    esc_html__( 'FadeInDown', 'wp-maxclean' ) => 'max_animate wow fadeInDown',
    esc_html__( 'FadeInLeft', 'wp-maxclean' ) => 'max_animate wow fadeInLeft',
    esc_html__( 'FadeInRight', 'wp-maxclean' ) => 'max_animate wow fadeInRight',
    esc_html__( 'FadeInUp', 'wp-maxclean' ) => 'max_animate wow fadeInUp',
    esc_html__( 'BounceInUp', 'wp-maxclean' ) => 'max_animate wow bounceInUp',
    esc_html__( 'BounceInDown', 'wp-maxclean' ) => 'max_animate wow bounceInDown',
    esc_html__( 'BounceInLeft', 'wp-maxclean' ) => 'max_animate wow bounceInLeft',
    esc_html__( 'BounceInRight', 'wp-maxclean' ) => 'max_animate wow bounceInRight',
    esc_html__( 'ZoomIn', 'wp-maxclean' ) => 'max_animate wow zoomIn',
    esc_html__( 'ZoomInDown', 'wp-maxclean' ) => 'max_animate wow zoomInDown',
    esc_html__( 'ZoomInLeft', 'wp-maxclean' ) => 'max_animate wow zoomInLeft',
    esc_html__( 'ZoomInRight', 'wp-maxclean' ) => 'max_animate wow zoomInRight',
    esc_html__( 'ZoomInUp', 'wp-maxclean' ) => 'max_animate wow zoomInUp',
    esc_html__( 'SlideInDown', 'wp-maxclean' ) => 'max_animate wow slideInDown',
    esc_html__( 'SlideInLeft', 'wp-maxclean' ) => 'max_animate wow slideInLeft',
    esc_html__( 'SlideInRight', 'wp-maxclean' ) => 'max_animate wow slideInRight',
    esc_html__( 'SlideInUp', 'wp-maxclean' ) => 'max_animate wow slideInUp',
 );
 return $animate;
}

/**
 * Social Share
 *
 * @author Knight
 * @since 1.0.0
 */
function wp_maxclean_social_share() {
    $url = get_the_permalink();
    $title = get_the_title();
    ?>  
        <ul class="cms-social default text-center">
            <li><a target="_blank" href="https://twitter.com/home?status=<?php echo esc_url(esc_html__('Check out this article','wp-maxclean')) ;?>:%20<?php echo esc_url($title);?>%20-%20<?php echo esc_url($url);?>"><i class="icon fa fa-twitter"></i></a></li>
            <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url);?>"><i aria-hidden="true" class="icon fa fa-facebook"></i></a></li>
            <li><a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode($url);?>"><i aria-hidden="true" class="icon fa fa-google-plus"></i></a></li>
            <li><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode($url);?>&amp;title=<?php echo urlencode($title);?>"><i aria-hidden="true" class="icon fa fa-linkedin"></i></a></li>
            <li><a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode($url);?>"><i aria-hidden="true" class="icon fa fa-pinterest"></i></a></li>
        </ul>
         
    <?php
}

/* convert dates to readable format */
if (!function_exists('wp_maxclean_relative_time')) {
    function wp_maxclean_relative_time($a) {
        //get current timestampt
        $b = strtotime("now");
        //get timestamp when tweet created
        $c = strtotime($a);
        //get difference
        $d = $b - $c;
        //calculate different time values
        $minute = 60;
        $hour = $minute * 60;
        $day = $hour * 24;
        $week = $day * 7;

        if (is_numeric($d) && $d > 0) {
            //if less then 3 seconds
            if ($d < 3)
                return esc_html__('right now','wp-maxclean');
            //if less then minute
            if ($d < $minute)
                return floor($d) . esc_html__(' seconds ago','wp-maxclean');
            //if less then 2 minutes
            if ($d < $minute * 2)
                return esc_html__('1 minute ago','wp-maxclean');
            //if less then hour
            if ($d < $hour)
                return floor($d / $minute) . esc_html__(' minutes ago','wp-maxclean');
            //if less then 2 hours
            if ($d < $hour * 2)
                return esc_html__('1 hour ago','wp-maxclean');
            //if less then day
            if ($d < $day)
                return floor($d / $hour) . esc_html__(' hours ago','wp-maxclean');
            //if more then day, but less then 2 days
            if ($d > $day && $d < $day * 2)
                return esc_html__('yesterday','wp-maxclean');
            //if less then year
            if ($d < $day * 365)
                return floor($d / $day) . esc_html__(' days ago','wp-maxclean');
            //else return more than a year
            return esc_html__('over a year ago','wp-maxclean');
        }
    }
}
  