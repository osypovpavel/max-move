<?php
/**
 * Meta options
 * 
 * @author Fox
 * @since 1.0.0
 */
class MaxCleanMetaOptions
{
    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'wp_maxclean_add_meta_boxes'));
        add_action('admin_enqueue_scripts', array($this, 'wp_maxclean_admin_script_loader'));
    }
    /* add script */
    function wp_maxclean_admin_script_loader()
    {
        global $pagenow;
        if (is_admin() && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
            wp_enqueue_style('metabox', get_template_directory_uri() . '/inc/options/css/metabox.css');
            
            wp_enqueue_script('easytabs', get_template_directory_uri() . '/inc/options/js/jquery.easytabs.min.js');
            wp_enqueue_script('metabox', get_template_directory_uri() . '/inc/options/js/metabox.js');
        }
    }
    /* add meta boxs */
    public function wp_maxclean_add_meta_boxes()
    {
        $this->maxclean_add_meta_box('wp_maxclean_template_page_options', esc_html__('Setting', 'wp-maxclean'), 'page');
        $this->maxclean_add_meta_box('wp_maxclean_template_post_options', esc_html__('Setting', 'wp-maxclean'), 'post');
        $this->maxclean_add_meta_box('wp_maxclean_team_position', esc_html__('Position', 'wp-maxclean'), 'team');
        $this->maxclean_add_meta_box('wp_maxclean_team_social', esc_html__('Social', 'wp-maxclean'), 'team');
        $this->maxclean_add_meta_box('wp_maxclean_testimonial_position', esc_html__('Position', 'wp-maxclean'), 'testimonial');
        $this->maxclean_add_meta_box('wp_maxclean_portfolio_video_downloadfile', esc_html__('Setting', 'wp-maxclean'), 'portfolio'); 
        $this->maxclean_add_meta_box('wp_maxclean_pricing_page_option', esc_html__('Pricing options', 'wp-maxclean'), 'pricings');
    }
    
    public function maxclean_add_meta_box($id, $label, $post_type, $context = 'advanced', $priority = 'default')
    {
        add_meta_box('_cms_' . $id, $label, array($this, $id), $post_type, $context, $priority);
    }
    /* --------------------- PAGE ---------------------- */
    function wp_maxclean_template_page_options() {
        ?>
        <div class="tab-container clearfix">
	        <ul class='etabs clearfix'>
	           <li class="tab"><a href="#tabs-general"><i class="fa fa-server"></i><?php esc_html_e('General', 'wp-maxclean'); ?></a></li>
	           <li class="tab"><a href="#tabs-page-title"><i class="fa fa-connectdevelop"></i><?php esc_html_e('Page Title', 'wp-maxclean'); ?></a></li>
               <li class="tab"><a href="#tabs-page-info-foot"><i class="fa fa-connectdevelop"></i><?php esc_html_e('Page Info Foot', 'wp-maxclean'); ?></a></li>
               <li class="tab"><a href="#tabs-footer"><i class="fa fa-diamond"></i><?php esc_html_e('Footer', 'wp-maxclean'); ?></a></li>
	        </ul>
	        <div class='panel-container'>
                <div id="tabs-general">
                <?php
                wp_maxclean_options(array(
                    'id' => 'full_width',
                    'label' => esc_html__('Full Width','wp-maxclean'),
                    'type' => 'switch',
                    'options' => array('on'=>'1','off'=>''),
                ));
                wp_maxclean_options(array(
                    'id' => 'enable_page_loading',
                    'label' => esc_html__('Enable page loading','wp-maxclean'),
                    'type' => 'switch',
                    'options' => array('on'=>'1','off'=>''),
                ));
                 wp_maxclean_options(array(
                    'id' => 'page_custom_class',
                    'label' => esc_html__('Page custom class','wp-maxclean'),
                    'type' => 'text'
                ));
                wp_maxclean_options(array(
                    'id' => 'logo_select',
                    'label' => esc_html__('Logo','wp-maxclean'),
                    'type' => 'imegesselect',
                    'options' => array(
                        '' => get_template_directory_uri().'/inc/options/images/logo/logo.jpg',
                        '1' => get_template_directory_uri().'/inc/options/images/logo/max-move.jpg',
                    )
                ));
                ?>
                </div>
                 
                <div id="tabs-page-title">
                <?php
                /* page title. */
                wp_maxclean_options(array(
                    'id' => 'page_title',
                    'label' => esc_html__('Custom','wp-maxclean'),
                    'type' => 'switch',
                    'options' => array('on'=>'1','off'=>''),
                    'follow' => array('1'=>array('#page_title_enable'))
                ));
                ?>  <div id="page_title_enable"><?php
                wp_maxclean_options(array(
                    'id' => 'page_title_text',
                    'label' => esc_html__('Title','wp-maxclean'),
                    'type' => 'text',
                ));
                wp_maxclean_options(array(
                    'id' => 'page_title_type',
                    'label' => esc_html__('Layout','wp-maxclean'),
                    'type' => 'imegesselect',
                    'options' => array(
                        '' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-0.png',
                        '1' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-1.png',
                        '2' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-2.png',
                    )
                ));
                ?>
                </div>
                </div>
                
                <div id="tabs-page-info-foot">
                    <?php
                        wp_maxclean_options(array(
                            'id' => 'page_info_foot_type',
                            'label' => esc_html__('Layout','wp-maxclean'),
                            'type' => 'imegesselect',
                            'options' => array(
                                '1' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-0.png',
                                '2' => get_template_directory_uri().'/inc/options/images/pageinfofoot/pif-s-1.png',
                                '3' => get_template_directory_uri().'/inc/options/images/pageinfofoot/pif-s-2.png',
                            )
                        ));
                         wp_maxclean_options(array(
                            'id' => 'is_animation',
                            'label' => esc_html__('is animation?','wp-maxclean'),
                            'type' => 'switch',
                            'options' => array('on'=>'1','off'=>''),
                        ));
                    ?>
                </div>
                
                <div id="tabs-footer">
                    <?php
                    wp_maxclean_options(array(
                        'id' => 'enable_footer_top_head',
                        'label' => esc_html__('Enable footer top head','wp-maxclean'),
                        'type' => 'switch',
                        'options' => array('on'=>'1','off'=>''),
                        'default' => 'on',
                       
                    ));
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    function wp_maxclean_template_post_options(){
        ?>
        <div class="tab-container clearfix">
	        <ul class='etabs clearfix'>
	           <li class="tab"><a href="#tabs-page-title"><i class="fa fa-connectdevelop"></i><?php esc_html_e('Page Title', 'wp-maxclean'); ?></a></li>
	        </ul>
	        <div class='panel-container'>
                <div id="tabs-page-title">
                <?php
               
                wp_maxclean_options(array(
                    'id' => 'page_title_text',
                    'label' => esc_html__('Title','wp-maxclean'),
                    'type' => 'text',
                ));
               
                ?>
               
                </div>
                 
            </div>
        </div>
        <?php
    }
    function wp_maxclean_team_position(){
        wp_maxclean_options(array(
            'id' => 'team_position',
            'label' => esc_html__('Position','wp-maxclean'),
            'type' => 'text'
        ));
    }
    function wp_maxclean_team_social(){ ?>
        <div class="social-item">
            <?php
            wp_maxclean_options(array(
                'id' => 'social_1',
                'label' => esc_html__('Item 1','wp-maxclean'),
                'type' => 'text'
            ));
            wp_maxclean_options(array(
                'id' => 'social_1_icon',
                'label' => esc_html__('Icon','wp-maxclean'),
                'type' => 'text'
            ));
            ?>
        </div>
                <hr>
        <div class="social-item">
            <?php
            wp_maxclean_options(array(
                'id' => 'social_2',
                'label' => esc_html__('Item 2','wp-maxclean'),
                'type' => 'text'
            ));
            wp_maxclean_options(array(
                'id' => 'social_2_icon',
                'label' => esc_html__('Icon','wp-maxclean'),
                'type' => 'text'
            ));
            ?>
        </div>
                <hr>
        <div class="social-item">
            <?php
            wp_maxclean_options(array(
                'id' => 'social_3',
                'label' => esc_html__('Item 3','wp-maxclean'),
                'type' => 'text'
            ));
            wp_maxclean_options(array(
                'id' => 'social_3_icon',
                'label' => esc_html__('Icon','wp-maxclean'),
                'type' => 'text'
            ));
            ?>
        </div>
                <hr>
        <div class="social-item">
            <?php
            wp_maxclean_options(array(
                'id' => 'social_4',
                'label' => esc_html__('Item 4','wp-maxclean'),
                'type' => 'text'
            ));
            wp_maxclean_options(array(
                'id' => 'social_4_icon',
                'label' => esc_html__('Icon','wp-maxclean'),
                'type' => 'text'
            ));
            ?>
        </div>
                <hr>
        <div class="social-item">
            <?php
            wp_maxclean_options(array(
                'id' => 'social_5',
                'label' => esc_html__('Item 5','wp-maxclean'),
                'type' => 'text'
            ));
            wp_maxclean_options(array(
                'id' => 'social_5_icon',
                'label' => esc_html__('Icon','wp-maxclean'),
                'type' => 'text'
            ));
            ?>
        </div>
                <hr>
        <div class="social-item">
            <?php
            wp_maxclean_options(array(
                'id' => 'social_6',
                'label' => esc_html__('Item 6','wp-maxclean'),
                'type' => 'text'
            ));
            wp_maxclean_options(array(
                'id' => 'social_6_icon',
                'label' => esc_html__('Icon','wp-maxclean'),
                'type' => 'text'
            ));
            ?>
        </div>
          <?php  
    }
    function wp_maxclean_testimonial_position(){
        wp_maxclean_options(array(
            'id' => 'testimonial_position',
            'label' => esc_html__('Position','wp-maxclean'),
            'type' => 'text'
        ));
    }
    function wp_maxclean_portfolio_video_downloadfile(){
        ?>
        <div class="tab-container clearfix">
	        <ul class='etabs clearfix'>
	           <li class="tab"><a href="#tabs-video"><i class="fa fa-server"></i><?php esc_html_e('Video', 'wp-maxclean'); ?></a></li>
               <li class="tab"><a href="#tabs-downloadfile"><i class="fa fa-diamond"></i><?php esc_html_e('Download file', 'wp-maxclean'); ?></a></li>
               <li class="tab"><a href="#tabs-title"><i class="fa fa-diamond"></i><?php esc_html_e('Title', 'wp-maxclean'); ?></a></li>
	        </ul>
	        <div class='panel-container'>
                <div id="tabs-video">
                <?php
                wp_maxclean_options(array(
                    'id' => 'show_video_button',
                    'label' => esc_html__('Show video button','wp-maxclean'),
                    'type' => 'switch',
                    'options' => array('on'=>'1','off'=>''),
                    'follow' => array('1'=>array('#video_url_enable'))
                )); ?>
                <div id="video_url_enable" >
                <?php
                wp_maxclean_options(array(
                    'id' => 'video_url',
                    'label' => esc_html__('Video url (ex: http://www.youtube.com/watch?v=qqXi8WmQ_WM)','wp-maxclean'),
                    'type' => 'textarea',
                ));
                ?>
                </div> 
                </div>
                <div id="tabs-downloadfile">
                <?php
                wp_maxclean_options(array(
                    'id' => 'show_download_file_button',
                    'label' => esc_html__('Show download file button','wp-maxclean'),
                    'type' => 'switch',
                    'options' => array('on'=>'1','off'=>''),
                    'follow' => array('1'=>array('#download_file_url_enable'))
                ));
                ?>
                <div id="download_file_url_enable" >
                <?php
                wp_maxclean_options(array(
                    'id' => 'file_url',
                    'label' => esc_html__('File','wp-maxclean'),
                    'type' => 'file',
                    'placeholder' => esc_html__('Select file download','wp-maxclean'),
                ));
                ?>
                </div>
                </div>
                <div id="tabs-title">
                <?php
               wp_maxclean_options(array(
                    'id' => 'show_title',
                    'label' => esc_html__('Show title','wp-maxclean'),
                    'type' => 'switch',
                    'options' => array('on'=>'1','off'=>''),
                )); ?>
                  
                </div>
                
            </div>
        </div>
        <?php
    }
    function wp_maxclean_pricing_page_option(){
        echo '<div class="pricing-option-wrap">';
        wp_maxclean_options(array(
            'id' => 'price',
            'label' => esc_html__('Price','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'unit',
            'label' => esc_html__('Unit','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'time',
            'label' => esc_html__('Time','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option1',
            'label' => esc_html__('Option 1','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option2',
            'label' => esc_html__('Option 2','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option3',
            'label' => esc_html__('Option 3','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option4',
            'label' => esc_html__('Option 4','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option5',
            'label' => esc_html__('Option 5','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option6',
            'label' => esc_html__('Option 6','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option7',
            'label' => esc_html__('Option 7','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option8',
            'label' => esc_html__('Option 8','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option9',
            'label' => esc_html__('Option 9','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'option10',
            'label' => esc_html__('Option 10','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'button_text',
            'label' => esc_html__('Button Text','wp-maxclean'),
            'type' => 'text',
        ));
        wp_maxclean_options(array(
            'id' => 'button_link',
            'label' => esc_html__('Button Link','wp-maxclean'),
            'type' => 'text',
        ));
         wp_maxclean_options(array(
            'id' => 'best_value',
            'label' => esc_html__('Is bets value?','wp-maxclean'),
            'type' => 'switch',
            'options' => array('on'=>'1','off'=>''),
        ));
        echo '</div>';
    }
}

new MaxCleanMetaOptions();