<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
if (! class_exists('Redux')) {
    return;
}

// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters('opt_name', 'smof_data');

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    // Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => $theme->get('Name'),
    'page_title' => $theme->get('Name'),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => true,
    // Use a asynchronous font on the front end or font string
    // 'disable_google_fonts_link' => true, // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-smiley',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    // 'open_expanded' => true, // Allow you to start the panel in an expanded way initially.
    'disable_save_warn' => true, // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => 'dashicons-dashboard',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'dashicons-smiley',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit' => '', // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'red',
            'shadow' => true,
            'rounded' => false,
            'style' => ''
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right'
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover'
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave'
            )
        )
    )
);

Redux::setArgs($opt_name, $args);

/**
 * General Options.
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Favicon Icon', 'wp-maxclean'),
    'icon' => 'el-icon-star',
    'fields' => array(
        array(
            'title' => esc_html__('Icon', 'wp-maxclean'),
            'subtitle' => esc_html__('Select a favicon icon (.png, .jpg).', 'wp-maxclean'),
            'id' => 'favicon_icon',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>get_template_directory_uri().'/assets/images/favicon.png'
            )
        ),
    )
));

/**
 * Header Options
 * 
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Header', 'wp-maxclean'),
    'icon' => 'el-icon-credit-card',
    'fields' => array(
        array(
            'id' => 'header_layout',
            'title' => esc_html__('Layouts', 'wp-maxclean'),
            'subtitle' => esc_html__('select a layout for header', 'wp-maxclean'),
            'default' => '',
            'type' => 'image_select',
            'options' => array(
                '' => get_template_directory_uri().'/inc/options/images/header/h-default.png'
            )
        ),
        array(
            'subtitle' => esc_html__('enable sticky mode for menu.', 'wp-maxclean'),
            'id' => 'menu_sticky',
            'type' => 'switch',
            'title' => esc_html__('Sticky Header', 'wp-maxclean'),
            'default' => false,
        ),
        array(
            'subtitle' => esc_html__('enable sticky mode for menu Tablets.', 'wp-maxclean'),
            'id' => 'menu_sticky_tablets',
            'type' => 'switch',
            'title' => esc_html__('Sticky Tablets', 'wp-maxclean'),
            'default' => false,
            'required' => array( 0 => 'menu_sticky', 1 => '=', 2 => 1 )
        ),
        array(
            'subtitle' => esc_html__('enable sticky mode for menu Mobile.', 'wp-maxclean'),
            'id' => 'menu_sticky_mobile',
            'type' => 'switch',
            'title' => esc_html__('Sticky Mobile', 'wp-maxclean'),
            'default' => false,
            'required' => array( 0 => 'menu_sticky', 1 => '=', 2 => 1 )
        )
    )
));

/* Header top */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Header Top', 'wp-maxclean'),
    'icon' => 'el-icon-minus',
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => esc_html__('Enable header top.', 'wp-maxclean'),
            'id' => 'enable_header_top',
            'type' => 'switch',
            'title' => esc_html__('Enable Header Top', 'wp-maxclean'),
            'default' => false,
        ) 
    )
));

/* Header logo */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Logo', 'wp-maxclean'),
    'icon' => 'el-icon-picture',
    'subsection' => true,
    'fields' => array(
        array(
            'title' => esc_html__('Select Logo', 'wp-maxclean'),
            'subtitle' => esc_html__('Select an image file for your logo.', 'wp-maxclean'),
            'id' => 'main_logo',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>get_template_directory_uri().'/assets/images/logo.png'
            )
        ),
        array(
            'subtitle' => esc_html__('in pixels.', 'wp-maxclean'),
            'id' => 'main_logo_height',
            'title' => 'Logo Height',
            'type' => 'dimensions',
            'width' => false,
            'output' => array('#cshero-header-logo a img'),
            'default' => array(
                'height' => '35px',
            )
        ),
        array(
            'subtitle' => esc_html__('in pixels.', 'wp-maxclean'),
            'id' => 'sticky_logo_height',
            'title' => 'Sticky Logo Height',
            'type' => 'dimensions',
            'output' => array('#cshero-header.header-fixed #cshero-header-logo a img'),
            'width' => false,
            'default' => array(
                'height' => '28px',
            )
        )
         
    )
));
/* Header transparent */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Page Title & BC', 'wp-maxclean'),
    'icon' => 'el-icon-map-marker',
    'fields' => array(
        array(
            'id' => 'page_title_layout',
            'title' => esc_html__('Layouts', 'wp-maxclean'),
            'subtitle' => esc_html__('select a layout for page title', 'wp-maxclean'),
            'default' => '1',
            'type' => 'image_select',
            'options' => array(
                '' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-0.png',
                '1' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-1.png',
                '2' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-2.png',
                 
            )
        ),
        array(
            'id'       => 'page_title_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', 'wp-maxclean' ),
            'subtitle' => esc_html__( 'page title background with image, color, etc.', 'wp-maxclean' ),
            'output'   => array('.page-title')
        ),
    )
));

/* Main Menu */
Redux::setSection($opt_name, array(
    'icon' => 'el-icon-podcast',
    'title' => esc_html__('Page Title', 'wp-maxclean'),
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'page_title_typography',
            'type' => 'typography',
            'title' => esc_html__('Typography', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('.page-title #page-title-text h1'),
            'units' => 'px',
            'subtitle' => esc_html__('Typography option with title text.', 'wp-maxclean')
        ),
    )
));

/* Menu Sticky */
Redux::setSection($opt_name, array(
    'icon' => 'el-icon-random',
    'title' => esc_html__('Page Info Foot', 'wp-maxclean'),
    'subsection' => true,
    'fields' => array(
       array(
            'id' => 'page_info_foot_layout',
            'title' => esc_html__('Layouts', 'wp-maxclean'),
            'subtitle' => esc_html__('select a layout for page information foot', 'wp-maxclean'),
            'default' => '1',
            'type' => 'image_select',
            'options' => array(
                '' => get_template_directory_uri().'/inc/options/images/pagetitle/pt-s-0.png',
                '1' => get_template_directory_uri().'/inc/options/images/pageinfofoot/pif-s-1.png',
                '2' => get_template_directory_uri().'/inc/options/images/pageinfofoot/pif-s-2.png',
                 
            )
        ),
    )
));

/**
 * Page Title
 *
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'icon' => 'el-icon-random',
    'title' => esc_html__('Breadcrumb', 'wp-maxclean'),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => esc_html__('The text before the breadcrumb home.', 'wp-maxclean'),
            'id' => 'breacrumb_home_prefix',
            'type' => 'text',
            'title' => esc_html__('Breadcrumb Home Prefix', 'wp-maxclean'),
            'default' => 'Home'
        ),
        array(
            'id' => 'breacrumb_typography',
            'type' => 'typography',
            'title' => esc_html__('Typography', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('.page-title #breadcrumb-text','.page-title #breadcrumb-text ul li a'),
            'units' => 'px',
            'subtitle' => esc_html__('Typography option with title text.', 'wp-maxclean'),
            'default' => array(
                'color' => '',
            )
        ),
    )
));

/* Breadcrumb */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Body', 'wp-maxclean'),
    'icon' => 'el-icon-website',
    'fields' => array(
        array(
            'subtitle' => esc_html__('Set layout boxed default(Wide).', 'wp-maxclean'),
            'id' => 'body_layout',
            'type' => 'switch',
            'title' => esc_html__('Boxed Layout', 'wp-maxclean'),
            'default' => false,
        ),
        array(
            'id'       => 'body_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', 'wp-maxclean' ),
            'subtitle' => esc_html__( 'body background with image, color, etc.', 'wp-maxclean' ),
            'output'   => array('body'),
            'default'  => array(
                'background-color' => '#ffffff',
            )
        )
    )
));

/**
 * Content
 *
 * css color.
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Page Loadding', 'wp-maxclean'),
    'icon' => 'el-icon-compass',
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => esc_html__('Enable page loadding.', 'wp-maxclean'),
            'id' => 'enable_page_loadding',
            'type' => 'switch',
            'title' => esc_html__('Enable Page Loadding', 'wp-maxclean'),
            'default' => false,
        ),
      
    )
));
/* page */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Footer', 'wp-maxclean'),
    'icon' => 'el-icon-credit-card',
    'fields' => array(
        array(
            'subtitle' => esc_html__('Enable footer top.', 'wp-maxclean'),
            'id' => 'enable_footer_top',
            'type' => 'switch',
            'title' => esc_html__('Enable Footer Top', 'wp-maxclean'),
            'default' => false,
        ),
        array(
            'subtitle' => esc_html__('enable button back to top.', 'wp-maxclean'),
            'id' => 'footer_botton_back_to_top',
            'type' => 'switch',
            'title' => esc_html__('Back To Top', 'wp-maxclean'),
            'default' => true,
        ),
        array(
            'subtitle' => esc_html__('Enable footer top menu row.', 'wp-maxclean'),
            'id' => 'enable_footer_top_menu_row',
            'type' => 'switch',
            'title' => esc_html__('Enable Footer Top Menu Row', 'wp-maxclean'),
            'default' => true,
        )
    )
));
/* archive */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Footer Bottom', 'wp-maxclean'),
    'icon' => 'el-icon-bookmark',
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => esc_html__('Enable footer bottom.', 'wp-maxclean'),
            'id' => 'enable_footer_bottom',
            'type' => 'switch',
            'title' => esc_html__('Enable Footer Botton', 'wp-maxclean'),
            'default' => true,
        ),
        array(
            'id'       => 'footer_bottom_content',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Content', 'wp-maxclean' ),
            'subtitle' => esc_html__( 'Input the html content', 'wp-maxclean' ),
            'default'  => '<span> &copy; Copyrights 2015<strong> MAXCLEAN.</strong></span><span> All Rights Reserved.</span><span> Designed with<i class="icon fa fa-heart-o"></i> by<a href="#"><strong>Templines</strong></a></span> ',
            'output'   => array('#cshero-footer-bottom .container'), 
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', 'wp-maxclean'),
            'id' => 'footer_botton_margin',
            'title' => 'Margin',
            'type' => 'spacing',
            'mode' => 'margin',
            'units' => array('px'),
            'output' => array('body .footer #cshero-footer-bottom .container'),
            'default' => array(
                'margin-top' => '55px',
                'margin-right' => 'auto',
                'margin-bottom' => '0px',
                'margin-left' => 'auto',
                'units' => 'px'
            ),
            'required' => array( 0 => 'enable_footer_bottom', 1 => '=', 2 => 1 )
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', 'wp-maxclean'),
            'id' => 'footer_botton_padding',
            'title' => 'Padding',
            'type' => 'spacing',
            'mode' => 'padding',
            'units' => array('px'),
            'output' => array('body .footer #cshero-footer-bottom .container'),
            'default' => array(
                'padding-top' => '38px',
                'padding-right' => '0px',
                'padding-bottom' => '40px',
                'padding-left' => '0px',
                'units' => 'px'
            ),
            'required' => array( 0 => 'enable_footer_bottom', 1 => '=', 2 => 1 )
        )
        
    )
));

/* Single */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Woocommerces', 'wp-maxclean'),
    'icon' => 'el el-shopping-cart',
    'fields' => array(
        array(
            'id'        => 'woo_columns_layout',
            'type'      => 'slider',
            'title'     => esc_html__('Product List Columns', 'wp-maxclean'),
            'subtitle'  => esc_html__('Choose number of columns in product list.', 'wp-maxclean'),
            'desc'      => esc_html__('Slider description. Min: 2, max: 4, step: 1, default value: 3', 'wp-maxclean'),
            "default"   => 3,
            "min"       => 2,
            "step"      => 1,
            "max"       => 4,
            'display_value' => 'label'
        ),
        array(
            'id'        => 'woo_number_page',
            'type'      => 'slider',
            'title'     => esc_html__('Number Product Per Page', 'wp-maxclean'),
            'subtitle'  => esc_html__('The number product to show on each page. Max is 30', 'wp-maxclean'),
            'desc'      => esc_html__('Slider description. Min: 2, max: 30, step: 1, default value: 12', 'wp-maxclean'),
            "default"   => 12,
            "min"       => 2,
            "step"      => 1,
            "max"       => 30,
            'display_value' => 'label'
        )   
    )
));

/**
 * coming soon option
 * 
 * extra css for customer.
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Styling', 'wp-maxclean'),
    'icon' => 'el-icon-adjust',
    'fields' => array(
        array(
            'subtitle' => esc_html__('set color main color.', 'wp-maxclean'),
            'id' => 'primary_color',
            'type' => 'color',
            'title' => esc_html__('Primary Color', 'wp-maxclean'),
            'default' => '#26c9ff'
        ),
        array(
            'id' => 'secondary_color',
            'type' => 'color',
            'title' => esc_html__('Secondary Color', 'wp-maxclean'),
            'default' => '#ffd526'
        ),
        array(
            'subtitle' => esc_html__('set color for tags <a></a>.', 'wp-maxclean'),
            'id' => 'link_color',
            'type' => 'color',
            'title' => esc_html__('Link Color', 'wp-maxclean'),
            'output'  => array('a'),
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('set color for tags <a></a>.', 'wp-maxclean'),
            'id' => 'link_color_hover',
            'type' => 'color',
            'title' => esc_html__('Link Color Hover', 'wp-maxclean'),
            'output'  => array('a:hover'),
            'default' => ''
        )
    )
));

/**
 * Styling
 * 
 * css color.
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Typography', 'wp-maxclean'),
    'icon' => 'el-icon-text-width',
    'fields' => array(
        array(
            'id' => 'font_body',
            'type' => 'typography',
            'title' => esc_html__('Body Font', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body'),
            'units' => 'px',
            'subtitle' => esc_html__('Typography option with each property can be called individually.', 'wp-maxclean'),
            'default' => array(
                'color' => '#333',
                'font-style' => '',
                'font-weight' => '300',
                'font-family' => 'Roboto',
                'google' => true,
                'font-size' => '13px',
                'line-height' => '1.4',
                'text-align' => ''
            )
        ),
        array(
            'id' => 'font_h1',
            'type' => 'typography',
            'title' => esc_html__('H1', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body h1'),
            'units' => 'px',
        ),
        array(
            'id' => 'font_h2',
            'type' => 'typography',
            'title' => esc_html__('H2', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body h2'),
            'units' => 'px',
        ),
        array(
            'id' => 'font_h3',
            'type' => 'typography',
            'title' => esc_html__('H3', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body h3'),
            'units' => 'px',
        ),
        array(
            'id' => 'font_h4',
            'type' => 'typography',
            'title' => esc_html__('H4', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body h4'),
            'units' => 'px',
        ),
        array(
            'id' => 'font_h5',
            'type' => 'typography',
            'title' => esc_html__('H5', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body h5'),
            'units' => 'px',
        ),
        array(
            'id' => 'font_h6',
            'type' => 'typography',
            'title' => esc_html__('H6', 'wp-maxclean'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('body h6'),
            'units' => 'px',
        )
    )
));

/**
 * Button
 * 
 * css color.
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Extra Fonts', 'wp-maxclean'),
    'icon' => 'el el-fontsize',
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'google-font-1',
            'type' => 'typography',
            'title' => esc_html__('Font 1', 'wp-maxclean'),
            'google' => true,
            'font-backup' => false,
            'font-style' => false,
            'color' => false,
            'text-align'=> false,
            'line-height'=>false,
            'font-size'=> false,
            'subsets'=> false,
        ),
        array(
            'id' => 'google-font-selector-1',
            'type' => 'textarea',
            'title' => esc_html__('Selector 1', 'wp-maxclean'),
            'subtitle' => esc_html__('add html tags ID or class (body,a,.class,#id)', 'wp-maxclean'),
            'validate' => 'no_html',
            'default' => '',
        ),
        array(
            'id' => 'google-font-2',
            'type' => 'typography',
            'title' => esc_html__('Font 2', 'wp-maxclean'),
            'google' => true,
            'font-backup' => false,
            'font-style' => false,
            'color' => false,
            'text-align'=> false,
            'line-height'=>false,
            'font-size'=> false,
            'subsets'=> false,
        ),
        array(
            'id' => 'google-font-selector-2',
            'type' => 'textarea',
            'title' => esc_html__('Selector 2', 'wp-maxclean'),
            'subtitle' => esc_html__('add html tags ID or class (body,a,.class,#id)', 'wp-maxclean'),
            'validate' => 'no_html',
            'default' => '',
        ),
    )
));


/**
 * Typography
 * 
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Custom CSS', 'wp-maxclean'),
    'icon' => 'el-icon-bulb',
    'fields' => array(
        array(
            'id' => 'custom_css',
            'type' => 'ace_editor',
            'title' => esc_html__('CSS Code', 'wp-maxclean'),
            'subtitle' => esc_html__('create your css code here.', 'wp-maxclean'),
            'mode' => 'css',
            'theme' => 'monokai',
        )
    )
));

/**
 * Optimal Core
 * 
 * @author Fox
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Optimal Core', 'wp-maxclean'),
    'icon' => 'el-icon-idea',
    'fields' => array(
        array(
            'subtitle' => esc_html__('no minimize , generate css over time...', 'wp-maxclean'),
            'id' => 'dev_mode',
            'type' => 'switch',
            'title' => esc_html__('Dev Mode (not recommended)', 'wp-maxclean'),
            'default' => false
        )
    )
));
  