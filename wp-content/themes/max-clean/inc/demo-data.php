<?php
add_action('ef3-import-start', 'medix_move_trash');
function medix_move_trash(){
    wp_trash_post(1);
    wp_trash_post(2);
}
 
add_filter('ef3-theme-options-opt-name', 'wp_maxclean_set_demo_opt_name');

function wp_maxclean_set_demo_opt_name(){
    return 'smof_data';
}

add_filter('ef3-replace-content', 'wp_maxclean_replace_content', 10 , 2);

function wp_maxclean_replace_content($replaces, $attachment){
    return array(
        '/tax_query:/' => 'remove_query:',
        '/categories:/' => 'remove_query:',
    );
}

add_filter('ef3-replace-theme-options', 'wp_maxclean_replace_theme_options');

function wp_maxclean_replace_theme_options(){
    return array(
        'dev_mode' => 0,
    );
}
add_filter('ef3-enable-create-demo', 'wp_maxclean_enable_create_demo');

function wp_maxclean_enable_create_demo(){
    return false;
}
 
function wp_maxclean_set_menu_location(){
    
    $setting = array(
        'Footer menu' => 'second',
		'Main menu' => 'primary'
    );
    
    $navs = wp_get_nav_menus();
    
    $new_setting = array();
    
    foreach ($navs as $nav){
        
        if(!isset($setting[$nav->name]))
            continue;
        
        $id = $nav->term_id;
        $location = $setting[$nav->name];
        
        $new_setting[$location] = $id;
    }
    
    set_theme_mod('nav_menu_locations', $new_setting);
}

add_action('ef3-import-finish', 'wp_maxclean_set_menu_location');

function wp_maxclean_set_woo_page(){
    
    $woo_pages = array(
        'woocommerce_shop_page_id' => 'Shop',
        'woocommerce_cart_page_id' => 'Cart',
        'woocommerce_checkout_page_id' => 'Checkout'
    );
    
    foreach ($woo_pages as $key => $woo_page){
    
        $page = get_page_by_title($woo_page);
    
        if(!isset($page->ID))
            return ;
             
        update_option($key, $page->ID);
    
    }

}

add_action('ef3-import-finish', 'wp_maxclean_set_woo_page');

function wp_maxclean_crop_images() {
    /**
     * Crop image
     */
    $query = array(
        'post_type'      => 'attachment',
        'posts_per_page' => -1,
        'post_status'    => 'inherit',
    );
    $media = new WP_Query($query);
    if ($media->have_posts()) {
        foreach ($media->posts as $image) {
            if (strpos($image->post_mime_type, 'image/') !== false) {
                $image_path = get_attached_file($image->ID);
                $metadata = wp_generate_attachment_metadata($image->ID, $image_path);
                wp_update_attachment_metadata($image->ID, $metadata);
            }
        }
    }
}

add_action('ef3-import-finish', 'wp_maxclean_crop_images',99,2);



