<?php
/**
 * @name : Default
 * @package : CMSSuperHeroes
 * @author : Fox
 */
?>
<?php global $smof_data, $maxclean_meta; ?>
<?php if ( isset($smof_data['enable_header_top']) && $smof_data['enable_header_top'] =='1'): ?>
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">  
            <div class="slogan">
                <?php dynamic_sidebar('sidebar-2'); ?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-3 col-md-4 col-lg-4">
            <div class="header-social">
                <?php dynamic_sidebar('sidebar-top-center'); ?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
            <div class="header-contacts">
                <?php dynamic_sidebar('sidebar-3'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
        <!-- end header__top .-->
<?php endif;?>
<div id="cshero-header" class="cshero-main-header <?php if (!$smof_data['menu_sticky']) {echo 'no-sticky';} ?> <?php if ($smof_data['menu_sticky_tablets']) {echo 'sticky-tablets';} ?> <?php if ($smof_data['menu_sticky_mobile']) {echo 'sticky-mobile';} ?>">
    <div class="container">
        <div class="row">
            <div id="cshero-header-logo" class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo (isset($maxclean_meta->_cms_logo_select) && $maxclean_meta->_cms_logo_select=='1')? esc_url(get_template_directory_uri().'/inc/options/images/logo/logo.maximum.png'): esc_url($smof_data['main_logo']['url']); ?>" alt="Максимум"></a>
            </div>
            <div id="cshero-header-navigation" class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                <div class="cshero-header-cart-search hidden-xs hidden-sm">
                    <?php dynamic_sidebar('sidebar-search-cart'); ?>
                </div>
                <nav id="site-navigation" class="main-navigation">
                    <?php wp_maxclean_main_menu(); ?>
                </nav>
            </div>
            <div id="cshero-menu-mobile" class="collapse"><i class="pe-7s-menu"></i></div>
            <div class="cshero-header-cart-search hidden-md hidden-lg">
                <?php dynamic_sidebar('sidebar-search-cart'); ?>
            </div>
        </div>
        
    </div>
</div>
<!-- #site-navigation -->