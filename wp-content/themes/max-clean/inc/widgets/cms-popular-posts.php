<?php
/**
 * Class Wp_maxclean_PopularPosts
 */
class Wp_maxclean_PopularPosts extends WP_Widget {
    /**
     * Widget Setup
     */
    function __construct() {
        $widget_ops = array('classname' => 'cms-popular-posts', 'description' => esc_html__('A widget that displays popular posts.', 'wp-maxclean') );
        $control_ops = array('id_base' => 'cms_popular_posts');
        parent::__construct('cms_popular_posts', esc_html__('CMS popular Posts', 'wp-maxclean'), $widget_ops, $control_ops);
    }

    /**
     * Display Widget
     * @param array $args
     * @param array $instance
     */
    function widget($args, $instance) {
        extract($args);
        $title = $instance['title'];
        $posts = $instance['posts'];

        $args_query = array(
            'orderby' => 'comment_count',
            'order' => 'DESC',
            'posts_per_page' => $posts
        );
        $wp_maxclean_popularposts = new WP_Query($args_query);
        echo ''.$before_widget;
        if($title) {
            echo ''.$before_title.esc_attr($title).$after_title;
        }
        ?>
        <?php if ($wp_maxclean_popularposts->have_posts()) : ?>
            <?php while($wp_maxclean_popularposts->have_posts()): $wp_maxclean_popularposts->the_post(); global $post; ?>
                <section class="widget-post__item clearfix">
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="entry-thumbnail"> 
                        <a href="<?php the_permalink(); ?>" class="img"> 
                            <?php the_post_thumbnail('thumbnail'); ?>
                        </a> 
                    </div>
                    <?php endif; ?>
                    <div class="entry-main">
                      <h4 class="entry-header">
                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      </h4>
                      <ul class="entry-meta">
                        <li><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo esc_html__('By ','wp-maxclean');?><?php the_author(); ?></a> </li>
                        <li><i class="icon-commets fa fa-comments"></i> <?php comments_popup_link( esc_html__( ' 0', 'wp-maxclean' ), esc_html__( ' 1', 'wp-maxclean' ), esc_html__( ' %', 'wp-maxclean' ) ); ?>  </li>
                      </ul>
                      <div class="border-color"></div>
                    </div>
                </section>
                
            <?php endwhile; ?>
        <?php endif; ?>
        <!-- END WIDGET -->
        <?php
        echo ''.$after_widget;
        wp_reset_postdata();
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['posts'] = $new_instance['posts'];
        
        return $instance;
    }

    function form($instance) {


        $defaults = array('title' => 'Popular Posts', 'categories' => 'all', 'posts' => 5, 'show_featured' => true, 'show_author' => true, 'date_fomat' => '');
        $instance = wp_parse_args((array) $instance, $defaults); ?>

        <p>
            <label for="<?php echo ''.$this->get_field_id('title'); ?>"><?php esc_html_e('Title:', 'wp-maxclean') ?></label>
            <input type="text" class="widefat" id="<?php echo ''.$this->get_field_id('title'); ?>" name="<?php echo ''.$this->get_field_name('title'); ?>" value="<?php echo ''.$instance['title']; ?>" />
        </p>
        <p>
            <label for="<?php echo ''.$this->get_field_id('posts'); ?>"><?php esc_html_e('Number of posts:', 'wp-maxclean') ?></label>
            <input type="text" class="widefat" id="<?php echo ''.$this->get_field_id('posts'); ?>" name="<?php echo ''.$this->get_field_name('posts'); ?>" value="<?php echo ''.$instance['posts']; ?>" />
        </p>
    <?php
    }
}

register_widget('Wp_maxclean_PopularPosts');
?>