<?php
class Wp_maxclean_Allservices_Widget extends WP_Widget {
	function __construct() {
        parent::__construct(
            'wp_maxclean_allservices_widget', // Base ID
            esc_html__('CMS All services', 'wp-maxclean'), // Name
            array('description' => esc_html__('A widget that displays all portfolio servie', 'wp-maxclean')) // Args
        );
    }

    function widget($args, $instance) {
		extract($args, EXTR_SKIP);

        echo ''.$before_widget;
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
    
        if (!empty($title))
          echo ''.$before_title . $title . $after_title;;
    
        // WIDGET CODE GOES HERE
        ?>
          <ul class="list-sidebar">
          <?php // Create and run custom loop
            if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) ) {
    			$number = 8;
    		}
            $posttype = $instance['posttype'];
            $wp_maxclean_custom_posts = new WP_Query( array(
    				'post_type' => $posttype,
    				'posts_per_page' => $number,
    				'no_found_rows' => true,
    				'post_status' => 'publish',
    				'ignore_sticky_posts' => true,
    			) );
            while ($wp_maxclean_custom_posts->have_posts()) : $wp_maxclean_custom_posts->the_post();
          ?>
            <li class="list-sidebar-item"><a href="<?php the_permalink(); ?>" class="list-sidebar-link"><i class="icon"></i><?php the_title(); ?></a></li>
          <?php endwhile; ?>
           <?php wp_reset_postdata(); ?>
          </ul>
        <?php
    
        echo ''.$after_widget;
	}
 
	function update($new_instance, $old_instance) {
    	 $instance = $old_instance;
         $instance['title'] = $new_instance['title'];
         $instance['number'] = (int)$new_instance['number'];
         $instance['posttype'] = strip_tags( $new_instance['posttype'] );
        return $instance;
	}

	function form($instance) {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '','number' => 0 ) );
        $title = $instance['title'];
        $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 8;
        $posttype = isset( $instance['posttype'] ) ? $instance['posttype']: 'post';
    ?>
        <p><label for="<?php echo ''.$this->get_field_id('title'); ?>">
        Title: <input class="widefat" id="<?php echo ''.$this->get_field_id('title'); ?>" name="<?php echo ''.$this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p> 
        <p><label for="<?php echo ''.$this->get_field_id('number'); ?>">
        Number: <input class="widefat" id="<?php echo ''.$this->get_field_id('number'); ?>" name="<?php echo ''.$this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /></label></p>
        
        <p><label for="<?php echo ''.$this->get_field_id( 'posttype' ); ?>"><?php esc_html_e( 'Post Type:', 'wp-maxclean' ); ?></label>
		<select name="<?php echo ''.$this->get_field_name( 'posttype' ); ?>" id="<?php echo ''.$this->get_field_id( 'posttype' ); ?>">
		<?php
			$post_types = get_post_types( array( 'public' => true ), 'objects' );
			foreach ( $post_types as $post_type => $value ) {
				if ( 'attachment' == $post_type ) {
					continue;
				}
			?>
				<option value="<?php echo esc_attr( $post_type ); ?>"<?php selected( $post_type, $posttype ); ?>><?php esc_attr( $value->label); ?></option>
		<?php } ?>
		</select>
		</p>
	<?php
	}

} //End class

/**
* Class CS_Allservices_Widget
*/

function wp_maxclean_register_allservices_widget() {
    register_widget('Wp_maxclean_Allservices_Widget');
}

add_action('widgets_init', 'wp_maxclean_register_allservices_widget');