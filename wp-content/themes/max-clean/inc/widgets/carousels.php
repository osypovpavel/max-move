<?php
class Wp_maxclean_Carousel_Widget extends WP_Widget {
	function __construct() {
        parent::__construct(
            'wp_maxclean_carousel_widget', // Base ID
            esc_html__('Post type carousel', 'wp-maxclean'), // Name
            array('description' => esc_html__('A widget that displays post type carousel', 'wp-maxclean')) // Args
        );
    }

    function widget($args, $instance) {
		extract($args, EXTR_SKIP);
         /* Load prettyPhoto css*/
        if ( !wp_script_is('', 'wp-maxclean-owl-carousel')) {
            wp_enqueue_style('wp-maxclean-owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), '1.0.1');
            wp_enqueue_script('wp-maxclean-owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '1.3.2', true);
        }
        if ( !wp_script_is('', 'wp-maxclean-owl-carousel-cms')) {
            wp_enqueue_script('wp-maxclean-owl-carousel-cms', get_template_directory_uri() . '/assets/js/owl.carousel.cms.js', array( 'jquery' ), '1.3.2', true);
        }
        echo ''.$before_widget;
        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
        
        if (!empty($title))
          echo ''.$before_title . $title . $after_title;;
      
            if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) ) {
    			$number = 8;
    		}
            $posttype = $instance['posttype'];
            
            $wp_maxclean_custom_posts = new WP_Query( array(
    			'post_type' => $posttype,
    			'posts_per_page' => $number,
    			'no_found_rows' => true,
    			'post_status' => 'publish',
    			'ignore_sticky_posts' => true,
    		) );
           
          ?>
        <div class="cms-carousel-widget list-clients <?php //echo esc_attr($atts['template']);?> " > 
            <?php
           // $posts = $atts['posts'];
            while($wp_maxclean_custom_posts->have_posts()){
                $wp_maxclean_custom_posts->the_post();
                
                ?>
                <div class="cms-team-item list-clients-item">
                    <?php 
                        if(has_post_thumbnail() && !post_password_required() && !is_attachment() &&  wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false)):
                            $class = ' has-thumbnail';
                            $thumbnail = get_the_post_thumbnail(get_the_ID(),'wp_maxclean_medium-thumb');
                        else:
                            $class = ' no-image';
                            $thumbnail = '<img src="'.esc_url(get_template_directory_uri().'/assets/images/no-image.jpg').'" alt="'.get_the_title().'" />';
                        endif;
                        //echo '<div class="cms-grid-media '.esc_attr($class).'">'.$thumbnail.'</div>';
                        echo ''.$thumbnail;
                    ?>
                   
                </div>
                <?php
            }
            ?>
            <?php wp_reset_postdata(); ?>
        </div>
          
        <?php
    
        echo ''.$after_widget;
	}
 
	function update($new_instance, $old_instance) {
    	 $instance = $old_instance;
         $instance['title'] = $new_instance['title'];
         $instance['number'] = (int)$new_instance['number'];
         $instance['posttype'] = strip_tags( $new_instance['posttype'] );
        return $instance;
	}

	function form($instance) {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '','number' => 0 ) );
        $title = $instance['title'];
        $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 8;
        $posttype = isset( $instance['posttype'] ) ? $instance['posttype']: 'post';
    ?>
        <p><label for="<?php echo ''.$this->get_field_id('title'); ?>">
        Title: <input class="widefat" id="<?php echo ''.$this->get_field_id('title'); ?>" name="<?php echo ''.$this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p> 
        <p><label for="<?php echo ''.$this->get_field_id('number'); ?>">
        Number: <input class="widefat" id="<?php echo ''.$this->get_field_id('number'); ?>" name="<?php echo ''.$this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /></label></p>
        
        <p><label for="<?php echo ''.$this->get_field_id( 'posttype' ); ?>"><?php esc_html_e( 'Post Type:', 'wp-maxclean' ); ?></label>
		<select name="<?php echo ''.$this->get_field_name( 'posttype' ); ?>" id="<?php echo ''.$this->get_field_id( 'posttype' ); ?>">
		<?php
			$post_types = get_post_types( array( 'public' => true ), 'objects' );
             
			foreach ( $post_types as $post_type => $value ) {  
				if ( 'attachment' == $post_type ) {
					continue;
				}
			?>
				<option value="<?php echo esc_attr( $post_type ); ?>"<?php selected( $post_type, $posttype ); ?>><?php echo ''.$value->label ; ?></option>
		<?php } ?>
		</select>
		</p>
	<?php
	}

} //End class

/**
* Class CS_Carousel_Widget
*/

function wp_maxclean_register_carousel_widget() {
    register_widget('Wp_maxclean_Carousel_Widget');
}

add_action('widgets_init', 'wp_maxclean_register_carousel_widget');