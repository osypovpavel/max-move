<?php
/**
 * Twitter Feed Template
 * 
 * @package News Twitter
 * @author FOX
 * @version 1.0.0
 */


//$date_time = isset($atts['date_format'])? $atts['date_format'] : 'M d, Y';
//var_dump($date_time);
?>


<?php foreach ($twitter as $index => $item): ?>
	<?php
	/* open row. */
	if ($row_index == 0 ): ?><div class="news-twitter-item"><?php endif; ?>

	   <div class="news-tweet-content twitter-home-widget">
            <div class="round-figure round-figure_mod-b"><i class="icon fa fa-twitter"></i> </div>
        <?php
            $tweet_content = $item['text'];
            $tweet_content = preg_replace('/http:\/\/([a-z0-9_\.\-\+\&\!\#\~\/\,]+)/i', '<a class="author2" href="http://$1" target="_blank">http://$1</a>', $tweet_content);
            $tweet_content = preg_replace('/@([a-z0-9_]+)/i', '<a class="author" href="http://twitter.com/$1" target="_blank">@$1</a>', $tweet_content);
            
        	echo '<div class="tweet-text"><i class="fa fa-twitter"></i> '.wp_kses_post($tweet_content).'</div>';			
        	echo '<div class="tweet-time">'.wp_maxclean_relative_time($item['created_at']).'</div>';
?>
	   </div>

	<?php $row_index++; ?>

	<?php
	/* close row. */
	if ($row_index == $row || $items_count == $index ): $row_index = 0; ?></div><?php endif; ?>

<?php endforeach; ?>