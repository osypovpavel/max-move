<?php

/**
 * Auto create css from Meta Options.
 * 
 * @author Fox
 * @version 1.0.0
 */
class CMSSuperHeroes_DynamicCss
{

    function __construct()
    {
        add_action('wp_head', array($this, 'wp_maxclean_generate_css'));
    }

    /**
     * generate css inline.
     *
     * @since 1.0.0
     */
    public function wp_maxclean_generate_css()
    {
        global $smof_data, $maxclean_base;
        $css = $this->wp_maxclean_css_render();
        if (isset($smof_data['dev_mode']) && $smof_data['dev_mode']==false) {
            $css = $maxclean_base->wp_maxclean_compressCss($css);
        }
        echo '<style type="text/css" data-type="cms_shortcodes-custom-css">'.$css.'</style>';
    }

    /**
     * header css
     *
     * @since 1.0.0
     * @return string
     */
    public function wp_maxclean_css_render()
    {
        global $smof_data, $maxclean_meta;
        ob_start();

        /* custom css. */

        echo (isset($smof_data['custom_css']) && !empty($smof_data['custom_css']))?$smof_data['custom_css']:'';
        
        return ob_get_clean();
    }
}

new CMSSuperHeroes_DynamicCss();