jQuery(document).ready(function($){
	"use strict";
	var pageNum    = [];
 	var total      = [];
	var max        = [];
 	var perpage    = [];
	var nextLink   = [];
	var masonry    = [];
	var cposts     = [];
	var newItems   = [];
	$('.cms-grid-wraper').each(function(){
		var $this = $(this);
		var $masonry_container;
		var html_id = $this.attr('id');
        
        var data_btntext = $this.data('btntext');
		var cms_variable = window["cms_more_obj" + html_id.replace(new RegExp('-', 'g'),'_')];
		if(typeof cms_variable != 'undefined'){
			 pageNum[html_id]    = parseInt(cms_variable.startPage) + 1;
		 	 total[html_id]      = parseInt(cms_variable.total);
			 max[html_id]        = parseInt(cms_variable.maxPages);
		 	 perpage[html_id]    = parseInt(cms_variable.perpage);
			 nextLink[html_id]   = cms_variable.nextLink;
			 masonry[html_id]    = cms_variable.masonry;
			setInterval(function(){
				jQuery('#main').find('audio,video').mediaelementplayer();
			},3000);
			$.html_idPost = function(total,perpage,pageNum){
				cposts[html_id] = total-perpage*pageNum;
				return data_btntext;
			}
			$.loadData = function(html_id){
				"use strict";
				$.get(nextLink[html_id],function(data){
					// Update page number and nextLink.
					if(masonry[html_id] == 'masonry'){
						var items = $(data).find('#'+html_id + ' .cms-grid-masonry > .cms-grid-item');
						items = $(data).find('#'+html_id+ ' .cms-grid-masonry > .loadmore-needed');
						$('#'+html_id).children('.cms-grid-masonry').append(items);
						$(items).imagesLoaded(function(){
							//$('#'+html_id).children('.cms-grid-masonry').shuffle('appended',items);
						})
					}
					else{
						newItems[html_id] = $($(data).find('#'+html_id).children('.cms-grid').html());
						$('#'+html_id).children('.cms-grid').append(newItems[html_id]);
					}
					pageNum[html_id]++;
					if(nextLink[html_id].indexOf('/page/') > -1){
						nextLink[html_id] = nextLink[html_id].replace(/\/page\/[0-9]?/, '/page/'+ pageNum[html_id]);
					}
					else{
						nextLink[html_id] = nextLink[html_id].replace(/paged=[0-9]?/, 'paged='+ pageNum[html_id]);
					}
					// Add a new placeholder, for when user clicks again.
					$('#'+html_id +' .cms-load-posts')
						.before('<div class="cms-placeholder-'+ pageNum[html_id] +'"></div>')
			 		// Update the button message.
					if(pageNum[html_id] <= max[html_id]) {
						$('#'+html_id +' .cms-load-posts a').text($.html_idPost(total[html_id],perpage[html_id],pageNum[html_id]-1));
					} else {
						$('#'+html_id +' .cms-load-posts a').text('No more posts to load.');
					}
					$('#'+html_id +' .cms-load-posts').find('a').data('loading',0);

					/* Potfolio resize*/
					cms_same_height_potfolio();

					/* Popup image */
					//cms_popup_image();
				});
			}
            
			if(pageNum[html_id] <= max[html_id]) {
				var text = $.html_idPost(total[html_id],perpage[html_id],1);
				$('#'+html_id +' .cms_pagination').append('<div class="cms-placeholder-'+ pageNum[html_id] +'"></div><p class="cms-load-posts"><a data-loading="0" href="#" class="wpb_button_anone wpb_none btn btn-default cms-bounce-to-right"><span>'+text+'</span></a></p>');
			}
			$('#'+html_id +' .cms-load-posts a').on('click',function(){
				if(pageNum[html_id] <= max[html_id]){
					$(this).text('Loading posts...');
					$.loadData(html_id);
				}else {
					$('#'+html_id +' .cms-load-posts a').append('.');
				}
				return false;
			});
		}
	});

	/* Potfolio */
	function cms_same_height_potfolio(){
		var center_gird_item = $('.template-cms_grid--layout1 .center-gird .cms-grid-item');
		var outer_most = $('.template-cms_grid--layout1 .cms-grid .outer-most .cms-grid-item');
		var last_outer_most_img = $('.template-cms_grid--layout1 .cms-grid .outer-most.last-row .cms-grid-item img');
		var center_gird_height = $('.template-cms_grid--layout1 .center-gird').height()-5;
		var last_grid_height = $('.template-cms_grid--layout1 .center-gird .cms-grid-item:last-child img').height();
		center_gird_item.css({
			height: last_grid_height+'px',
		});
		outer_most.css({
			height: center_gird_height+'px',
		});
		last_outer_most_img.css({
			height: center_gird_height+'px',
		});
	}

	/* Popup image */
	function cms_popup_image(){
		$('.button-popup').magnificPopup({ 
		  	type: 'image',
			removalDelay: 500, 
		  	callbacks: {
			    beforeOpen: function() {
			       this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
			       this.st.mainClass = this.st.el.attr('data-effect');
			    }
		  	},
		  	closeOnContentClick: true,
		  	midClick: true 
		});
	}
});