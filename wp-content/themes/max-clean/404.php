<?php
/**
 * The template for displaying 404 pages (Not Found)
 * 
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main" class="container">
			<article id="post-0" class="post error404 no-results not-found">
				<section class="section-area section_404">
                    <div class="col-md-5 col-md-offset-1">
                      <h2 class="ui-title-inner"><?php esc_html_e( 'OOpS! PAGE Not fOUnd','wp-maxclean'); ?></h2>
                      <div class="border-color border-color_center"></div>
                      <img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/404.png'); ?>" alt="404" height="164" width="365" class="img-responsive"/>
                      <div class="info"><?php esc_html_e( 'The page you are looking for has not found.','wp-maxclean'); ?><br /> <?php esc_html_e( 'It might have been removed or moved to a new location.','wp-maxclean'); ?>  </div>
                    </div>
                </section>

			</article><!-- #post-0 -->

		</div><!-- #content -->
        <div class="search-404">
            <div class="container">
                <section class="section-area section_searce-page">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h2 class="ui-title-block"><?php esc_html_e( 'try searching a new page','wp-maxclean'); ?></h2>
                                <?php get_search_form(); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
	</div><!-- #primary -->

<?php get_footer(); ?>