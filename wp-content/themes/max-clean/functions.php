<?php  
/**
 * WP_Maxclean functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 */
 
/**
 * Add global values.
 */
global $smof_data, $maxclean_meta, $maxclean_base;
 
/* Dismiss vc update. */
if(function_exists('vc_set_as_theme')) vc_set_as_theme( true );

/* Add base functions */
require( get_template_directory() . '/inc/base.class.php' );

if(class_exists("Maxclean_Base")){
    $maxclean_base = new Maxclean_Base();
}
 
/* core functions. */
require_once( get_template_directory() . '/inc/functions.php' );

/**
 * Add new elements for VC
 * 
 * @author FOX
 */
add_action('vc_before_init', 'wp_maxclean_vc_before');

function wp_maxclean_vc_before(){
     
    if(!class_exists('CmsShortCode'))
        return ;
     
    require( get_template_directory() . '/inc/elements/cms_googlemap.php' ); 
     
}

add_action('vc_after_init', 'wp_maxclean_vc_params');
function wp_maxclean_vc_params() {
    require( get_template_directory() . '/vc_params/vc_rows.php' );
    require( get_template_directory() . '/vc_params/vc_column.php');
    require( get_template_directory() . '/vc_params/vc_custom_heading.php' );
    require( get_template_directory() . '/vc_params/vc_btn.php' );
    require( get_template_directory() . '/vc_params/vc_column_text.php' );
    vc_remove_element( "vc_button" );
    vc_remove_element( "vc_button2" );
}
 
  
/* Add SCSS */
if(!class_exists('scssc')){
    require( get_template_directory() . '/inc/libs/scss.inc.php' );
}

/* Add Meta Core Options */
if(is_admin()){
  
    if(!class_exists('CsCoreControl')){
        /* add mete core */
        require( get_template_directory() . '/inc/metacore/core.options.php' );
        /* add meta options */
        require( get_template_directory() . '/inc/options/meta.options.php' );
    }
     
}

 
/* Add widgets */
require( get_template_directory() . '/inc/widgets/cart_search.php' );
require( get_template_directory() . '/inc/widgets/news_tabs.php' );
require( get_template_directory() . '/inc/widgets/recent_post_v2.php' );
require( get_template_directory() . '/inc/widgets/social.php' );
require( get_template_directory() . '/inc/widgets/allservices.php' );
require( get_template_directory() . '/inc/widgets/carousels.php' );
require( get_template_directory() . '/inc/widgets/cms-popular-posts.php' );


/* Custom News Twitter template */
add_filter('znews_twitter/widget/template', 'wp_maxclean_news_twitter');
function wp_maxclean_news_twitter(){
	return get_template_directory() . '/inc/widgets/news-twitter/feed.php';
}

// Set up the content width value based on the theme's design and stylesheet.
if ( ! isset( $content_width ) )
	$content_width = 625;

  
/**
 * WP Maxclean setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 * WP Maxclean supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since 1.0.0
 */
function wp_maxclean_setup() {
	/*
	 * Makes Max Clean available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Twelve, use a find and replace
	 * to change 'twentytwelve' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wp-maxclean' , get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds title tag
	add_theme_support( "title-tag" );
	
	// Add woocommerce
	add_theme_support('woocommerce');
	
	// Adds custom header
	add_theme_support( 'custom-header' );
	
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme supports a variety of post formats.
	add_theme_support( 'post-formats', array( 'video', 'audio' , 'gallery', 'link', 'quote',) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', esc_html__( 'Primary Menu', 'wp-maxclean' ) );
    register_nav_menu( 'second', esc_html__( 'Second Menu', 'wp-maxclean' ) );

	/*
	 * This theme supports custom background color and image,
	 * and here we also set up the default background color.
	 */
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );

	//set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
    add_image_size( 'wp_maxclean_small_medium', 270, 220, true );
    add_image_size( 'wp_maxclean_medium-thumb', 400, 270, true );
    add_image_size( 'wp_maxclean_square-thumb', 500, 500, true );
    add_image_size( 'wp_maxclean_blog-large', 1140, 548, true );
}

add_action( 'after_setup_theme', 'wp_maxclean_setup' );

/**
 * Get meta data.
 * @author Fox
 * @return mixed|NULL
 */
function wp_maxclean_meta_data(){
    global $post, $maxclean_meta;
    
    if(!isset($post->ID)) return ;
    
    $maxclean_meta = json_decode(get_post_meta($post->ID, '_cms_meta_data', true));
    
    if(empty($maxclean_meta)) return ;
    
    foreach ($maxclean_meta as $key => $meta){
        $maxclean_meta->$key = rawurldecode($meta);
    }
}
add_action('wp', 'wp_maxclean_meta_data');

/**
 * Get post meta data.
 * @author Fox
 * @return mixed|NULL
 */
function wp_maxclean_post_meta_data(){
    global $post;
    
    if(!isset($post->ID)) return null;
    
    $post_meta = json_decode(get_post_meta($post->ID, '_cms_meta_data', true));
    
    if(empty($post_meta)) return null;
    
    foreach ($post_meta as $key => $meta){
        $post_meta->$key = rawurldecode($meta);
    }
    
    return $post_meta;
}

/**
 * add google font
 */
function wp_maxclean_roboto() {
    $fonts_url = '';
    $roboto = _x( 'on', 'Roboto font: on or off', 'wp-maxclean' );
    if ( 'off' !== $roboto ) {
        $query_args = array(
        'family' =>  'Roboto:300,400,700,900,400italic', 
        'subset' => urlencode( 'latin,latin-ext' )
        );
    }  
    $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    return esc_url_raw( $fonts_url );
}
function wp_maxclean_roboto_condensed() {
    $fonts_url = '';
    $merriweather = _x('on','Merriweather font: on or off','wp-maxclean');
     if ( 'off' !== $merriweather ) {
        $query_args = array(
        'family' =>  'Merriweather:700', 
        'subset' => urlencode( 'latin,latin-ext' )
        );
      }
    $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    return esc_url_raw( $fonts_url );
}
function wp_maxclean_merriweather() {
    $fonts_url = '';
    $roboto_condensed = _x( 'on', 'Roboto+Condensed font: on or off', 'wp-maxclean' );
     if ( 'off' !== $roboto_condensed ) {
        $query_args = array(
        'family' =>  'Roboto+Condensed:400,700', 
        'subset' => urlencode( 'latin,latin-ext' )
        );
      }
    $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    return esc_url_raw( $fonts_url );
}

/**
 * Enqueue scripts and styles for front-end.
 * @author Fox
 * @since CMS SuperHeroes 1.0
 */
function wp_maxclean_scripts_styles() {
    
	global $smof_data, $maxclean_meta, $wp_styles;
	
	/** theme options. */
	$script_options = array(
	    'menu_sticky'=> isset($smof_data['menu_sticky'])?$smof_data['menu_sticky']:false,
	    'menu_sticky_tablets'=> isset($smof_data['menu_sticky_tablets'])?$smof_data['menu_sticky_tablets']:false,
	    'menu_sticky_mobile'=> isset($smof_data['menu_sticky_mobile'])?$smof_data['menu_sticky_mobile']:false,
	    'paralax' => 1,
	    'back_to_top'=> isset($smof_data['footer_botton_back_to_top'])?$smof_data['footer_botton_back_to_top']:false
	);

	/*------------------------------------- JavaScript ---------------------------------------*/
	
	
	/** --------------------------libs--------------------------------- */
	
	wp_enqueue_script('wp_maxclean-modernizr-js', get_template_directory_uri() . '/assets/js/libs/modernizr.custom.js', array( 'jquery' ), '1.0.1', true);
	/* Adds JavaScript Bootstrap. */
	wp_enqueue_script('wp_maxclean-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '3.3.2', true);
	
	/* Add parallax plugin. */
	wp_enqueue_script('wp_maxclean-parallax', get_template_directory_uri() . '/assets/js/jquery.parallax-1.1.3.js', array( 'jquery' ), '1.1.3', true);
	
 
	/** --------------------------custom------------------------------- */
	 
    /* Add main.js */
    /* Add prettyPhoto js*/
    
    wp_enqueue_script('wp_maxclean-prettyphoto-js', get_template_directory_uri() . '/assets/js/jquery.prettyPhoto.js', array( 'jquery' ), '3.1.6', true);
    wp_enqueue_script('wp_maxclean-magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('wp_maxclean-libsClassie-js', get_template_directory_uri() . '/assets/js/libs/wow.min.js', array( 'jquery' ), '1.0.1', true);
	wp_register_script('wp_maxclean-main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '1.0.0', true);
	wp_localize_script('wp_maxclean-main', 'CMSOptions', $script_options);
	wp_enqueue_script('wp_maxclean-main');
	/* Add menu.js */
    wp_enqueue_script('wp_maxclean-menu', get_template_directory_uri() . '/assets/js/menu.js', array( 'jquery' ), '1.0.0', true);
 
   /* Loader */
    if((isset($smof_data['enable_page_loadding']) && $smof_data['enable_page_loadding']==true ) && (isset($maxclean_meta->_cms_enable_page_loading) && $maxclean_meta->_cms_enable_page_loading)){   
        wp_enqueue_script('wp_maxclean-classie-js', get_template_directory_uri() . '/assets/plugins/loader/js/classie.js', array( 'jquery' ), '1.0.1', true);
        wp_enqueue_script('wp_maxclean-pathLoader-js', get_template_directory_uri() . '/assets/plugins/loader/js/pathLoader.js', array( 'jquery' ), '1.0.1', true);
        wp_enqueue_script('wp_maxclean-mainLoader-js', get_template_directory_uri() . '/assets/plugins/loader/js/main.js', array( 'jquery' ), '1.0.1', true);
        
        /* Loader css*/
        wp_enqueue_style('wp-maxclean-loader', get_template_directory_uri() . '/assets/plugins/loader/css/loader.css', array(), '1.0.1');
	}
     
    /*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

    /*------------------------------------- Stylesheet ---------------------------------------*/
	
    // Add Google font
    wp_enqueue_style( 'wp-maxclean-roboto-font', wp_maxclean_roboto(), array(), null );
    wp_enqueue_style( 'wp-maxclean-roboto-condensed-font', wp_maxclean_roboto_condensed(), array(), null );
    wp_enqueue_style( 'wp-maxclean-merriweather-font', wp_maxclean_merriweather(), array(), null );

	/** --------------------------libs--------------------------------- */
	
	/* Loads Bootstrap stylesheet. */
	wp_enqueue_style('wp_maxclean-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '3.3.2');
	
	 
	/** --------------------------custom------------------------------- */
	
    
	/* Loads our main stylesheet. */
	wp_enqueue_style( 'wp_maxclean-style', get_stylesheet_uri(), array( 'wp_maxclean-bootstrap' ));

	/* Loads the Internet Explorer specific stylesheet. */
	wp_enqueue_style( 'wp_maxclean-ie', get_template_directory_uri() . '/assets/css/ie.css', array( 'wp_maxclean-style' ), '20121010' );
	$wp_styles->add_data( 'wp_maxclean-ie', 'conditional', 'lt IE 9' );
	
	/* WooCommerce */
	if(class_exists('WooCommerce')){
	    wp_enqueue_style( 'wp_maxclean-woocommerce', get_template_directory_uri() . "/assets/css/woocommerce.css", array(), '1.0.0');
	}
	/* Load prettyPhoto css*/
    wp_enqueue_style('wp_maxclean-prettyphoto-css', get_template_directory_uri() . '/assets/css/prettyPhoto.css', array(), '1.0.1');
 
    /* Load prettyPhoto css*/
    wp_enqueue_style('wp_maxclean-magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), '1.0.1');
    
    /* Loads Bootstrap stylesheet. */
	wp_enqueue_style('wp_maxclean-font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.3.0');

	/* Loads Font Ionicons. */
	wp_enqueue_style('wp_maxclean-font-ionicons', get_template_directory_uri() . '/assets/css/ionicons.min.css', array(), '2.0.1');
    
    /* Loads Normalize. */
    wp_enqueue_style('wp_maxclean-font-normalize', get_template_directory_uri() . '/assets/css/normalize.css', array(), '1.0.0');
    
    /* Loads Animate. */
    wp_enqueue_style('wp_maxclean-animate', get_template_directory_uri() . '/assets/css/animate.css', array(), '1.0.0');

	/* Loads Pe Icon. */
	wp_enqueue_style('wp_maxclean-pe-icon', get_template_directory_uri() . '/assets/css/pe-icon-7-stroke.css', array(), '1.0.1');
	
	/* Load static css*/
	wp_enqueue_style('wp_maxclean-static', get_template_directory_uri() . '/assets/css/static.css', array( 'wp_maxclean-style' ), '1.0.1');
}

add_action( 'wp_enqueue_scripts', 'wp_maxclean_scripts_styles' );

/**
 * Register sidebars.
 *
 * Registers our main widget area and the front page widget areas.
 *
 * @since Fox
 */
function wp_maxclean_widgets_init() {
	register_sidebar( array(
		'name' => esc_html__( 'Main Sidebar', 'wp-maxclean' ),
		'id' => 'sidebar-1',
		'description' => esc_html__( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'wp-maxclean' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
	) );
      
	register_sidebar( array(
		'name' => esc_html__( 'Header Top left', 'wp-maxclean' ),
		'id' => 'sidebar-2',
		'description' => esc_html__( 'Appears when using the optional Header with a page set as Header top left', 'wp-maxclean' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Header Top Center', 'wp-maxclean' ),
		'id' => 'sidebar-top-center',
		'description' => esc_html__( 'Appears when using the optional Header with a page set as Header top center', 'wp-maxclean' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
	) );
    register_sidebar( array(
		'name' => esc_html__( 'Header Top right', 'wp-maxclean' ),
		'id' => 'sidebar-3',
		'description' => esc_html__( 'Appears when using the optional Header with a page set as Header top right', 'wp-maxclean' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
    	'name' => esc_html__( 'Service left', 'wp-maxclean' ),
    	'id' => 'sidebar-4',
    	'description' => esc_html__( 'Appears on the left when view service detail page.', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
    
    register_sidebar( array(
    	'name' => esc_html__( 'Search & Cart', 'wp-maxclean' ),
    	'id' => 'sidebar-search-cart',
    	'description' => esc_html__( 'Appears on the right of top menu', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
    register_sidebar(array(
        'name' => esc_html__( 'Woocommerce Sidebar', 'wp-maxclean' ),
        'id' => 'woocommerce_sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="wg-title">',
		'after_title' => '</h3>',
    ));
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Top 1', 'wp-maxclean' ),
    	'id' => 'sidebar-5',
    	'description' => esc_html__( 'Appears when using the optional Footer with a page set as Footer Top 1', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Top 2', 'wp-maxclean' ),
    	'id' => 'sidebar-6',
    	'description' => esc_html__( 'Appears when using the optional Footer with a page set as Footer Top 2', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Top 3', 'wp-maxclean' ),
    	'id' => 'sidebar-7',
    	'description' => esc_html__( 'Appears when using the optional Footer with a page set as Footer Top 3', 'wp-maxclean' ),
    	'before_widget' => '<aside class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title"><span>',
    	'after_title' => '</span></h3>',
	) );
	
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Top 4', 'wp-maxclean' ),
    	'id' => 'sidebar-8',
    	'description' => esc_html__( 'Appears when using the optional Footer with a page set as Footer Top 4', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title"><span>',
    	'after_title' => '</span></h3>',
	) );
    
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Top 5', 'wp-maxclean' ),
    	'id' => 'sidebar-9',
    	'description' => esc_html__( 'Appears when using the optional Footer with a page set as Footer Top 5', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title"><span>',
    	'after_title' => '</span></h3>',
	) );
	
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Boton Left', 'wp-maxclean' ),
    	'id' => 'sidebar-10',
    	'description' => esc_html__( 'Appears when using the optional Footer Boton with a page set as Footer Boton left', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
    	'name' => esc_html__( 'Footer Boton Right', 'wp-maxclean' ),
    	'id' => 'sidebar-11',
    	'description' => esc_html__( 'Appears when using the optional Footer Boton with a page set as Footer Boton right', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
    
    register_sidebar( array(
    	'name' => esc_html__( 'Twitter', 'wp-maxclean' ),
    	'id' => 'sidebar-12',
    	'description' => esc_html__( 'Show widget twitter', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
    register_sidebar( array(
    	'name' => esc_html__( 'Contact buttom date', 'wp-maxclean' ),
    	'id' => 'sidebar-13',
    	'description' => esc_html__( 'Contact buttom date', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
    register_sidebar( array(
    	'name' => esc_html__( 'Contact bottom social', 'wp-maxclean' ),
    	'id' => 'sidebar-14',
    	'description' => esc_html__( 'Contact bottom social', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
    register_sidebar( array(
    	'name' => esc_html__( 'contact bottom phone', 'wp-maxclean' ),
    	'id' => 'sidebar-15',
    	'description' => esc_html__( 'Contact bottom phone', 'wp-maxclean' ),
    	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    	'after_widget' => '</aside>',
    	'before_title' => '<h3 class="wg-title">',
    	'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'wp_maxclean_widgets_init' );

/**
 * Filter the page menu arguments.
 *
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since 1.0.0
 */
function wp_maxclean_page_menu_args( $args ) {
    if ( ! isset( $args['show_home'] ) )
        $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'wp_maxclean_page_menu_args' );

 

/**
 * Save custom theme meta. 
 * 
 * @since 1.0.0
 */
function wp_maxclean_save_meta_boxes($post_id) {
    
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    /* update field subtitle */
    if(isset($_POST['post_subtitle'])){
        update_post_meta($post_id, 'post_subtitle', $_POST['post_subtitle']);
    }
}

add_action('save_post', 'wp_maxclean_save_meta_boxes');

/**
 * Display navigation to next/previous comments when applicable.
 *
 * @since 1.0.0
 */
function wp_maxclean_comment_nav() {
    // Are there comments to navigate through?
    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
    ?>
	<nav class="navigation comment-navigation">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'wp-maxclean' ); ?></h2>
		<div class="nav-links">
			<?php
				if ( $prev_link = get_previous_comments_link( esc_html__( 'Older Comments', 'wp-maxclean' ) ) ) :
					printf( '<div class="nav-previous">%s</div>', $prev_link );
				endif;

				if ( $next_link = get_next_comments_link( esc_html__( 'Newer Comments', 'wp-maxclean' ) ) ) :
					printf( '<div class="nav-next">%s</div>', $next_link );
				endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .comment-navigation -->
	<?php
	endif;
}

/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since 1.0.0
 */
function wp_maxclean_paging_nav() {
    // Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $GLOBALS['wp_query']->max_num_pages,
			'current'  => $paged,
			'mid_size' => 1,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => wp_kses( '<i class="fa fa-angle-left"></i>', true ),
			'next_text' => wp_kses( '<i class="fa fa-angle-right"></i>', true ),
	) );

	if ( $links ) :

	?>
	<nav class="navigation paging-navigation clearfix" style="margin-bottom: 30px">
            <span class="pagination-title">PAGES</span>
			<div class="pagination loop-pagination">
				<?php echo ''.$links; ?>
			</div><!-- .pagination -->
	</nav><!-- .navigation -->
	<?php
	endif;
}

/**
* Display navigation to next/previous post when applicable.
*
* @since 1.0.0
*/
function wp_maxclean_post_nav() {
    global $post;

    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );

    if ( ! $next && ! $previous )
        return;
    ?>
	<nav class="navigation post-navigation">
		<div class="nav-links clearfix">
			<?php
			$prev_post = get_previous_post();
			if (!empty( $prev_post )): ?>
			  <a class="btn btn-default post-prev left" href="<?php echo esc_url(get_permalink( $prev_post->ID )); ?>"><i class="fa fa-angle-left"></i><?php echo esc_attr($prev_post->post_title); ?></a>
			<?php endif; ?>
			<?php
			$next_post = get_next_post();
			if ( is_a( $next_post , 'WP_Post' ) ) { ?>
			  <a class="btn btn-default post-next right" href="<?php echo esc_url(get_permalink( $next_post->ID )); ?>"><?php echo get_the_title( $next_post->ID ); ?><i class="fa fa-angle-right"></i></a>
			<?php } ?>

			</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
function wp_maxclean_remove_comment_fields($fields) {
    unset($fields['author']);
    unset($fields['email']);
    unset($fields['url']);
    return $fields;
    }
add_filter('comment_form_default_fields','wp_maxclean_remove_comment_fields');
/* Add Custom Comment */
function wp_maxclean_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
    ?>
    <<?php echo esc_attr($tag) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body clearfix">
        <?php endif; ?>
       
        <div class="comment-author-image vcard comment">
            <div class="avatar-placeholder">
        	   <?php echo get_avatar( $comment, 87 ); ?>
            </div>
            <div class="comment-inner">
                <header class="comment-header"><cite class="comment-author"><?php echo get_comment_author()?></cite>
                  <span class="comment-datetime"><?php echo get_comment_date('M d, Y \a\t g:i A');?></span>
                </header>
                <div class="comment-content">
                  <?php comment_text(); ?>
                   <?php if ( $comment->comment_approved == '0' ) : ?>
                    	<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.' , 'wp-maxclean'); ?></em>
                    <?php endif; ?>
                  <div class="comment-reply"><?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?> </div>
                </div>
            </div>
         
        </div>
        
         
        <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
    <?php
}

add_filter('comment_form_default_fields','wp_maxclean_custom_fields');
function wp_maxclean_custom_fields($fields) {

        $commenter = wp_get_current_commenter();
        $req = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' );

        $fields[ 'author' ] = '<div class="input-group">'.
            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .'" tabindex="1" ' . $aria_req . ' placeholder="'.esc_html__( "NAME", "wp-maxclean" ).'" class="form-control" />
            </div>';
        
        $fields[ 'email' ] = '<div class="input-group">'.
            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" tabindex="2" ' . $aria_req . ' placeholder="EMAIL" class="form-control" />
            </div>';
        $fields[ 'url' ] = '<div class="input-group">
                        <input id="comment-website" type="text" name="url" value="' . esc_attr( $commenter['comment_author_url'] ) .'" tabindex="3" placeholder="'.esc_html__( "WEBSITE", "wp-maxclean" ).'" class="form-control">
                      </div>';

    return $fields;
}

//for the comment wrapping functions - ensures HTML does not break.
$comment_open_div = 0;

/**
 * Creates an opening div for a bootstrap row.
 * @global int $comment_open_div
 */
function wp_maxclean_lp_before_comment_fields(){
    global $comment_open_div;
    $comment_open_div = 1;
    echo '<div class="row"><div class="col-md-5">';
}
/**
 * Creates a closing div for a bootstrap row.
 * @global int $comment_open_div
 * @return type
 */
function wp_maxclean_lp_after_comment_fields(){
    global $comment_open_div;
    if($comment_open_div == 0)
        return;
    echo '</div>';
}

add_action('comment_form_before_fields', 'wp_maxclean_lp_before_comment_fields');
add_action('comment_form_after_fields', 'wp_maxclean_lp_after_comment_fields');

/**
 * limit words
 * 
 * @since 1.0.0
 */
if (!function_exists('wp_maxclean_limit_words')) {
    function wp_maxclean_limit_words($string, $word_limit) {
        $words = explode(' ', $string, ($word_limit + 1));
        if (count($words) > $word_limit) {
            array_pop($words);
        }
        return implode(' ', $words)."";
    }
}
add_filter('cms-shorcode-list', 'wp_maxclean_shortcode_list');
function wp_maxclean_shortcode_list(){
    return array(
	'cms_carousel',
	'cms_grid',
	'cms_fancybox_single',
    'cms_counter_single',
	);
}

add_filter('wp_list_categories', 'wp_maxclean_add_span_cat_count');
function wp_maxclean_add_span_cat_count($links) {
    $links = str_replace('</a>', '<i class="icon"></i></a>', $links);
     
    return $links;
}
add_filter('wp_list_pages', 'wp_maxclean_add_icon_to_link');
function wp_maxclean_add_icon_to_link($links) {
    $links = str_replace('</a>', '<i class="icon"></i></a>', $links);
     
    return $links;
}
add_filter('get_archives_link', 'wp_maxclean_add_icon_to_link_archive');
function wp_maxclean_add_icon_to_link_archive($links) {
    $links = str_replace('</a>', '<i class="icon"></i></a>', $links);
     
    return $links;
}
 
 
/**
 * replace rel on stylesheet (Fix validator link style tag attribute)
 */
function wp_maxclean_validate_stylesheet($src) { 
    if(strstr($src,'mediaelement-css')|| strstr($src,'wp-mediaelement-css') || strstr($src,'font-awesome-css') || strstr($src,'isotopes-css-css') || strstr($src,'wp_maxclean-prettyphoto-css') || strstr($src,'prettyphoto-css')){
        return str_replace('rel', 'property="stylesheet" rel', $src);
    }
    else{
        return $src;
    }
}
add_filter('style_loader_tag', 'wp_maxclean_validate_stylesheet');
 
 
 
 