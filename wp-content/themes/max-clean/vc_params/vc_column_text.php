<?php

    vc_add_param("vc_column_text", array(
        'type' => 'textfield',
        'heading' => esc_html__( 'Wrap Text Width', 'wp-maxclean' ),
        'param_name' => 'wrap_text_width',
        'value' => '',
        'description' => esc_html__( '400px', 'wp-maxclean' ),
    ));
    vc_add_param("vc_column_text", array(
        'type' => 'textfield',
        'heading' => esc_html__( 'Font size', 'wp-maxclean' ),
        'param_name' => 'fontsize',
        'value' => '',
        'description' => esc_html__( '18px', 'wp-maxclean' ),
    ));
     
    vc_add_param("vc_column_text", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Font weight", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "font_weight",
        "value" => array(
            "None"  => "",
            "300" => "300",
            "400" => "400",
            "500" => "500",
            "600" => "600",
            "700" => "700"
        ),
        "std" => ""
    ));
   
 