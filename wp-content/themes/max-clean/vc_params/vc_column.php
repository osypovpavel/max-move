<?php
/**
 * Add column params
 * 
 * @author Fox
 * @since 1.0.0
 */

    vc_add_param("vc_column", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Animation", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "col_animation",
        "value" =>  wp_maxclean_animate_lib()
    ));
    vc_add_param("vc_column", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Text-align", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "col_align",
        "value" =>  array(
            'Left'  => 'text-left',
            'Center'    => 'text-center',
            'Right'    => 'text-right',
        ), 
        "std" => ''
    ));
    vc_add_param("vc_column", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Data wow duration", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "col_data_wow_duration",
        "value" =>  array(
            'None'  => '',
            '1s'    => '1s',
            '2s'    => '2s',
            '3s'    => '3s',
            '4s'    => '4s',
            '5s'    => '5s',
        ), 
    ));
    vc_add_param("vc_column", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Data column delay", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "data_col_delay",
        "value" =>  array(
            'None'  => '',
            '0.5s'    => '0.5s',
            '1s'    => '1s',
            '1.5s'    => '1.5s',
            '2s'    => '2s',
            '2.5s'    => '2.5s',
        ), 
    ));
    
    
   
