<?php 
/* Loads Stroke flat icon Font. */
add_filter( 'vc_iconpicker-type-flaticon', 'vc_iconpicker_type_flaticon' );

/**
 * flat icons from  
 *
 * @param $icons - taken from filter - vc_map param field settings['source'] provided icons (default empty array).
 * If array categorized it will auto-enable category dropdown
 *
 * @since 4.4
 * @return array - of icons for iconpicker, can be categorized, or not.
 */
function vc_iconpicker_type_flaticon( $icons ) {
	$flaticon_icons = array(
		array( "flaticon-airplane105" => esc_html__( "airplane105", "wp-maxclean" ) ),
		array( "flaticon-building137" => esc_html__( "building137", "wp-maxclean" ) ),
		array( "flaticon-businessman253" => esc_html__( "businessman253", "wp-maxclean" ) ),
		array( "flaticon-cleaning11" => esc_html__( "cleaning11", "wp-maxclean" ) ),
		array( "flaticon-clipboard52" => esc_html__( "clipboard52", "wp-maxclean" ) ),
		array( "flaticon-construction16" => esc_html__( "construction16", "wp-maxclean" ) ),
		array( "flaticon-delivery30" => esc_html__( "delivery30", "wp-maxclean" ) ),
		array( "flaticon-logisticsdelivery" => esc_html__( "logisticsdelivery", "wp-maxclean" ) ),
		array( "flaticon-logistics3" => esc_html__( "logistics3", "wp-maxclean" ) ),
		array( "flaticon-package36" => esc_html__( "package36", "wp-maxclean" ) ),
		array( "flaticon-packages2" => esc_html__( "packages2", "wp-maxclean" ) ),
		array( "flaticon-restaurant36" => esc_html__( "restaurant36", "wp-maxclean" ) ),
	);

	return array_merge( $icons, $flaticon_icons );
}
 ?>