<?php
/**
 * New button implementation
 * array_merge is needed due to merging other shortcode data into params.
 * @since 4.5
 */
global $pixel_icons;
 
$params = array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'Text Button', 'wp-maxclean' ),
            'save_always' => true,
            'param_name' => 'title',
            // fully compatible to btn1 and btn2
            'value' => esc_html__( 'Text on the button', 'wp-maxclean' ),
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__( 'URL (Link)', 'wp-maxclean' ),
            'param_name' => 'link',
            'description' => esc_html__( 'Add link to button.', 'wp-maxclean' ),
            // compatible with btn2 and converted from href{btn1}
        ),
         array(
            'type' => 'dropdown',
            'heading' => esc_html__( 'Button space', 'wp-maxclean' ),
            'param_name' => 'button_space',
            "value" => array(
                'Button medium' => 'button-medium',
                'Button large' => 'button-large',
            ),
            "std" =>'',
            'description' => esc_html__( 'Size of button', 'wp-maxclean' ),
        ),
        array(
            "type" => "dropdown",
            "class" => "",
            "heading" => esc_html__("Button Type", 'wp-maxclean'),
            "param_name" => "button_type",
            "value" => array(
                'Button Default' => 'btn btn-default',
                'Button Default White' => 'btn btn-default btn-white',
                'Button Primary' => 'btn btn-primary',
                'Button Green' => 'btn btn-green',
                'Button Purple' => 'btn btn-purple',
                'Button Crimson' => 'btn btn-crimson',
                'Button second' => 'btn btn-second',
            ),
            "std" =>'Button Default',
        ),
         array(
            'type' => 'textfield',
            'heading' => esc_html__( 'Margin', 'wp-maxclean' ),
            'save_always' => true,
            'param_name' => 'margin',
            'value' => '',
            'description' => esc_html__( '0px 0px 0px 0px', 'wp-maxclean' ),
        ),
       
    );
     
 
/**
 * @class WPBakeryShortCode_VC_Btn
 */
vc_map( array(
    'name' => esc_html__( 'Button', 'wp-maxclean' ),
    'base' => 'vc_btn',
    'icon' => 'icon-wpb-ui-button',
    'category' => array(
        esc_html__( 'Content', 'wp-maxclean' ),
    ),
    'description' => esc_html__( 'Eye catching button', 'wp-maxclean' ),
    'params' => $params,
    'js_view' => 'VcButton3View',
    'custom_markup' => '{{title}}<div class="vc_btn3-container"><button class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-{{ params.shape }} vc_btn3-style-{{ params.style }} vc_btn3-color-{{ params.color }}">{{{ params.title }}}</button></div>',
) );
