<?php
/**
 * Add row params
 * 
 * @author Knight
 * @since 1.0.0
 */
add_action( 'init', 'wp_maxclean_fancybox_single_integrateWithVC' );
if(!function_exists('wp_maxclean_fancybox_single_integrateWithVC')){  
    include(get_template_directory() . '/vc_params/cms_fancybox_single/fontlibs/flaticon.php');
    wp_enqueue_style('wp-maxclean-font-flaticon', get_template_directory_uri() . '/assets/css/flaticon.css', array(), '1.0.0');
    function wp_maxclean_fancybox_single_integrateWithVC(){
         vc_map(
        	array(
        		"name" => esc_html__("CMS Single Fancy Box", 'wp-maxclean'),
        	    "base" => "cms_fancybox_single",
        	    "class" => "vc-cms-fancy-boxes",
        	    "category" => esc_html__("CmsSuperheroes Shortcodes", 'wp-maxclean'),
        	    "params" => array(
        	    	array(
        	            "type" => "textfield",
        	            "heading" => esc_html__("Title",'wp-maxclean'),
        	            "param_name" => "title",
        	            "value" => "",
        	            "description" => esc_html__("Title Of Fancy Icon Box",'wp-maxclean'),
        	            "group" => esc_html__("General Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "textarea",
        	            "heading" => esc_html__("Description",'wp-maxclean'),
        	            "param_name" => "description",
        	            "value" => "",
        	            "description" => esc_html__("Description Of Fancy Icon Box",'wp-maxclean'),
        	            "group" => esc_html__("General Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "dropdown",
        	            "heading" => esc_html__("Content Align",'wp-maxclean'),
        	            "param_name" => "content_align",
        	            "value" => array(
        	            	"Default" => "default",
        	            	"Left" => "left",
        	            	"Right" => "right",
        	            	"Center" => "center"
        	            	),
        	            "group" => esc_html__("General Settings", 'wp-maxclean')
        	        ),
        	        
        	        array(
        				'type' => 'dropdown',
        				'heading' => esc_html__( 'Icon library', 'wp-maxclean' ),
        				'value' => array(
        					esc_html__( 'Font Awesome', 'wp-maxclean' ) => 'fontawesome',
        					esc_html__( 'Open Iconic', 'wp-maxclean' ) => 'openiconic',
        					esc_html__( 'Typicons', 'wp-maxclean' ) => 'typicons',
        					esc_html__( 'Entypo', 'wp-maxclean' ) => 'entypo',
        					esc_html__( 'Linecons', 'wp-maxclean' ) => 'linecons',
        					esc_html__( 'Pixel', 'wp-maxclean' ) => 'pixelicons',
        					esc_html__( 'P7 Stroke', 'wp-maxclean' ) => 'pe7stroke',
        					esc_html__( 'RT Icon', 'wp-maxclean' ) => 'rticon',
                            esc_html__( 'Flat Icon', 'wp-maxclean' ) => 'flaticon',
        				),
        				'param_name' => 'icon_type',
        				'description' => esc_html__( 'Select icon library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_fontawesome',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'fontawesome',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'fontawesome',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        	        array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_openiconic',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'openiconic',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'openiconic',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_typicons',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'typicons',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'typicons',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_entypo',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'entypo',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'entypo',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_linecons',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'linecons',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'linecons',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_pixelicons',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'pixelicons',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'pixelicons',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_pe7stroke',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'pe7stroke',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'pe7stroke',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_rticon',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'rticon',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'rticon',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
        			array(
        				'type' => 'iconpicker',
        				'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
        				'param_name' => 'icon_glyphicons',
        	            'value' => '',
        				'settings' => array(
        					'emptyIcon' => true, // default true, display an "EMPTY" icon?
        					'type' => 'glyphicons',
        					'iconsPerPage' => 200, // default 100, how many icons per/page to display
        				),
        				'dependency' => array(
        					'element' => 'icon_type',
        					'value' => 'glyphicons',
        				),
        				'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
        				"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        			),
                    array(
                	    'type' => 'iconpicker',
                		'heading' => esc_html__( 'Icon Item', 'wp-maxclean' ),
                		'param_name' => 'icon_flaticon',
                        'value' => '',
                		'settings' => array(
                			'emptyIcon' => true, // default true, display an "EMPTY" icon?
                			'type' => 'flaticon',
                			'iconsPerPage' => 200, // default 100, how many icons per/page to display
                		),
                		'dependency' => array(
                			'element' => 'icon_type',
                			'value' => 'flaticon',
                		),
                		'description' => esc_html__( 'Select icon from library.', 'wp-maxclean' ),
                		"group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
                    ),
        		 
        			array(
        	            "type" => "attach_image",
        	            "heading" => esc_html__("Image Item",'wp-maxclean'),
        	            "param_name" => "image",
        	            "group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "textfield",
        	            "heading" => esc_html__("Title Item",'wp-maxclean'),
        	            "param_name" => "title_item",
        	            "value" => "",
        	            "description" => esc_html__("Title Of Item",'wp-maxclean'),
        	            "group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "textarea",
        	            "heading" => esc_html__("Content Item",'wp-maxclean'),
        	            "param_name" => "description_item",
        	            "group" => esc_html__("Fancy Icon Settings", 'wp-maxclean')
        	        ),
        	        
        	        array(
        	            "type" => "dropdown",
        	            "heading" => esc_html__("Button Type",'wp-maxclean'),
        	            "param_name" => "button_type",
        	            "value" => array(
        	            	"Button" => "button",
        	            	"Text" => "text"
        	            	),
        	            "group" => esc_html__("Buttons Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "textfield",
        	            "heading" => esc_html__("Link",'wp-maxclean'),
        	            "param_name" => "button_link",
        	            "value" => "#",
        	            "description" => "",
        	            "group" => esc_html__("Buttons Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "textfield",
        	            "heading" => esc_html__("Text",'wp-maxclean'),
        	            "param_name" => "button_text",
        	            "value" => "",
        	            "description" => "",
        	            "group" => esc_html__("Buttons Settings", 'wp-maxclean')
        	        ),
        	        array(
        	            "type" => "textfield",
        	            "heading" => esc_html__("Extra Class",'wp-maxclean'),
        	            "param_name" => "class",
        	            "value" => "",
        	            "description" => "",
        	            "group" => esc_html__("Template", 'wp-maxclean')
        	        ),
        	    	array(
        	            "type" => "cms_template",
        	            "param_name" => "cms_template",
        	            "admin_label" => true,
        	            "heading" => esc_html__("Shortcode Template",'wp-maxclean'),
        	            "shortcode" => "cms_fancybox_single",
        	            "group" => esc_html__("Template", 'wp-maxclean'),
        	        ),
                    array(
                        "type" => "colorpicker",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
                        "heading" => esc_html__("Background color", 'wp-maxclean'),
                        "param_name" => "background_color",
                        "value" => "",
                        "description" => esc_html__("Select background color for fox.", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--layout3.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout3.php')),
                        'group' => 'Template'
                    ),
                    array(
                        "type" => "colorpicker",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
                        "heading" => esc_html__("Font color", 'wp-maxclean'),
                        "param_name" => "font_color",
                        "value" => "",
                        "description" => esc_html__("Select color for text.", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--layout3.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout3.php')),
                        'group' => 'Template'
                    ),
                     array(
                        "type" => "colorpicker",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
                        "heading" => esc_html__("Border after color", 'wp-maxclean'),
                        "param_name" => "border_after_color",
                        "value" => "",
                        "description" => esc_html__("Select Border after color.", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--layout3.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout3.php')),
                        'group' => 'Template'
                    ),
                     array(
                        "type" => "textfield",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
                        "heading" => esc_html__("Scheme number", 'wp-maxclean'),
                        "param_name" => "scheme_number",
                        "value" => "",
                        "description" => esc_html__("Enter the number in the scheme.", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--layout4.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout4.php')),
                        'group' => 'Template'
                    ),
                    array(
                        'type' => 'checkbox',
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
                        'heading' => esc_html__("Show arrow", 'wp-maxclean'),
                        'param_name' => 'show_arrow',
                        'value' => array(
                            'Yes' => true
                        ),
                        'description' => esc_html__("Yes = Show Show arrow", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--layout4.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout4.php')),
                        'group' => 'Template'
                    ),
                    array(
        	            "type" => "dropdown",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
        	            "heading" => esc_html__("List mark type",'wp-maxclean'),
        	            "param_name" => "list_mark_type",
        	            "value" => array(
        	            	"Default" => "",
        	            	"Small" => "small"
        	            	),
                        'template' => array('cms_fancybox_single--layout1.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout1.php')),
        	            'group' => 'Template'
        	        ),
                    array(
        	            "type" => "dropdown",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
        	            "heading" => esc_html__("Align",'wp-maxclean'),
        	            "param_name" => "align",
        	            "value" => array(
        	            	"left" => "",
        	            	"right" => "right"
        	               ),
                        'template' => array('cms_fancybox_single--layout6.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--layout6.php')),
        	            'group' => 'Template'
        	        ),
                    array(
        	            "type" => "textarea_html",
                        "holder" => "div",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
        	            "heading" => esc_html__("Contact information",'wp-maxclean'),
        	            "param_name" => "content",
        	            "value" =>  esc_html__("Enter the contact information", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--contact-info.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--contact-info.php')),
        	            'group' => 'Template'
        	        ),
                    array(
                        "type" => "colorpicker",
                        "class" => "",
                        "edit_field_class" => "cms_custom_param vc_col-sm-12 vc_column",
                        "heading" => esc_html__("Icon color", 'wp-maxclean'),
                        "param_name" => "icon_color",
                        "value" => "",
                        "description" => esc_html__("Select color for icon.", 'wp-maxclean'),
                        'template' => array('cms_fancybox_single--contact-info.php'),
                        "dependency" => array("element"=>"cms_template", "value" => array('cms_fancybox_single--contact-info.php')),
                        'group' => 'Template'
                    ),
        		)
        	)
        );
    }
}
 
?>