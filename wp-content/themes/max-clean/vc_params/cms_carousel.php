<?php
	$params = array(
		array(
			"type" => "checkbox",
			"heading" => esc_html__("Border top",'wp-maxclean'),
			"param_name" => "border_top",
			"value" => array(
                'Yes' => true
            ),
			"template" => array("cms_carousel--clients.php")
		)
	);
 
?>