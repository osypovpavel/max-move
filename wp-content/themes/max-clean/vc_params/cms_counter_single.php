<?php
 
    vc_add_param('cms_counter_single', array(
	    "type" => "colorpicker",
        "heading" => esc_html__("Font color",'wp-maxclean'),
        "param_name" => "font_color",
        "value" => '',
        "description" => esc_html__("Select color for text.", 'wp-maxclean'),
        "std" => '#fff',
        "group" => esc_html__("General Settings", 'wp-maxclean')
    )); 
?>