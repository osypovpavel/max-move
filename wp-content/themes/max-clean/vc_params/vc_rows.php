<?php
/**
 * Add row params
 * 
 * @author Fox
 * @since 1.0.0
 */

    vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Content Full Width", 'wp-maxclean'),
        'param_name' => 'full_width',
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = full width, default content boxed.", 'wp-maxclean')
    ));
    vc_remove_param("vc_row", "el_id");
    vc_remove_param("vc_row", "parallax");
    vc_remove_param("vc_row", "parallax_image");
    vc_remove_param("vc_row", "video_bg");
    vc_remove_param("vc_row", "video_bg_url");
    vc_remove_param("vc_row", "video_bg_parallax");
    vc_remove_param("vc_row", "css_animation");
    
    
    vc_add_param("vc_row", array(
        "type" => "textfield",
        "heading" => esc_html__("Extra id name", 'wp-maxclean'),
        "param_name" => "row_id"
    ));
    vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Hide backgroud in mobile", 'wp-maxclean'),
        'param_name' => 'hide_mobile_bg',
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = Hide, default Show.", 'wp-maxclean')
    ));
    vc_add_param("vc_row", array(
        "type" => "colorpicker",
        "class" => "",
        "heading" => esc_html__("Border top color", 'wp-maxclean'),
        "param_name" => "border_top_color",
        "value" => "",
        "description" => esc_html__("Select color for border top.", 'wp-maxclean')
    ));
    
    vc_add_param('vc_row', array(
        'type' => 'textfield',
        'heading' => esc_html__("Z-index", 'wp-maxclean'),
        'param_name' => 'rowz_index',
        'value' => '', 
        'description' => esc_html__("Z-index for row", 'wp-maxclean')
    ));
    vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Overflow hidden", 'wp-maxclean'),
        'param_name' => 'overflow_hidden',
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = Hide.", 'wp-maxclean')
    ));
    vc_add_param("vc_row", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Animation", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "row_animation",
        "value" =>  wp_maxclean_animate_lib()
    ));
    vc_add_param("vc_row", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Data wow duration", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "row_data_wow_duration",
        "value" =>  array(
            'None'  => '',
            '1s'    => '1s',
            '2s'    => '2s',
            '3s'    => '3s',
            '4s'    => '4s',
            '5s'    => '5s',
        ), 
    ));
    vc_add_param("vc_row", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Data row delay", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "data_row_delay",
        "value" =>  array(
            'None'  => '',
            '0.5s'    => '0.5s',
            '1s'    => '1s',
            '1.5s'    => '1.5s',
            '2s'    => '2s',
            '2.5s'    => '2.5s',
        ),
    ));
    vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Is call to action?", 'wp-maxclean'),
        'param_name' => 'is_call_to_action',
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = padding(50px 56px) for call to action row.", 'wp-maxclean')
    ));
    vc_add_param('vc_row', array(
        'type' => 'dropdown',
        'heading' => esc_html__("Background Position", 'wp-maxclean'),
        'param_name' => 'bg_position',
        'value' => array(
            'center' => 'center',
            'left' => 'left',
            'right' => 'right',
        ),
        'std' => 'centre',
        'group' => 'Design Options',
        'description' => esc_html__("Select the kind of background would you like to set for this row.", 'wp-maxclean')
    ));
    
    vc_add_param('vc_row', array(
        'type' => 'dropdown',
        'heading' => esc_html__("Overlay Color", 'wp-maxclean'),
        'param_name' => 'overlay_row',
        'value' => array(
            'No' => '',
            'Yes' => 'yes'
        ),
        'group' => 'Design Options'
    ));
    vc_add_param("vc_row", array(
        "type" => "colorpicker",
        "class" => "",
        "heading" => esc_html__('Color', 'wp-maxclean'),
        "param_name" => "overlay_color",
        'group' => 'Design Options',
        "dependency" => array(
            "element" => "overlay_row",
            "value" => array(
                "yes"
            )
        ),
        "description" => ''
    ));
    vc_add_param('vc_row', array(
        'type' => 'textfield',
        'heading' => esc_html__("Opacity", 'wp-maxclean'),
        'param_name' => 'overlay_opacity',
        'group' => 'Design Options',
        "dependency" => array(
            "element" => "overlay_row",
            "value" => array(
                "yes"
            )
        ),
        'description' => esc_html__("Set opacity overlay color - ex: 0.6 .", 'wp-maxclean')
    ));
   
    vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Enable Waves Top", 'wp-maxclean'),
        'param_name' => 'enable_waves_top',
        "edit_field_class" => "vc_col-sm-6 vc_column",
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = Enable Waves Top, default Disable.", 'wp-maxclean'),
        "group" => esc_html__("Waves Settings", 'wp-maxclean')
    ));
     vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Enable Waves Bottom", 'wp-maxclean'),
        'param_name' => 'enable_waves_bottom',
        "edit_field_class" => "vc_col-sm-6 vc_column",
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = Enable Waves Bottom, default Disable.", 'wp-maxclean'),
        "group" => esc_html__("Waves Settings", 'wp-maxclean')
    ));
    vc_add_param('vc_row', array(
	    "type" => "dropdown",
        "heading" => esc_html__("Wave top color type",'wp-maxclean'),
        "param_name" => "wave_top_color_type",
        "edit_field_class" => "vc_col-sm-6 vc_column",
        "value" => array(
        	"grey" => "grey",
        	"primary" => "primary",
            "white" => "white",
        	"white2" => "white2",
        	),
        "std" => "white",
        "group" => esc_html__("Waves Settings", 'wp-maxclean')
    ));  
    vc_add_param('vc_row', array(
	    "type" => "dropdown",
        "heading" => esc_html__("Wave bottom color type",'wp-maxclean'),
        "param_name" => "wave_bottom_color_type",
        "edit_field_class" => "vc_col-sm-6 vc_column",
        "value" => array(
        	"grey" => "grey",
        	"primary" => "primary",
            "white" => "white",
        	"white2" => "white2",
        	),
        'std' => "white",
        "group" => esc_html__("Waves Settings", 'wp-maxclean')
    ));  
    vc_add_param('vc_row', array(
	    "type" => "textfield",
        "heading" => esc_html__("Margin bottom of wave top",'wp-maxclean'),
        "param_name" => "wave_margin_bottom_of_wtop",
        "edit_field_class" => "vc_col-sm-6 vc_column",
        "value" => '',
        "std" => '',
        "group" => esc_html__("Waves Settings", 'wp-maxclean') 
    )); 
    vc_add_param('vc_row', array(
	    "type" => "textfield",
        "heading" => esc_html__("Margin top of wave bottom",'wp-maxclean'),
        "param_name" => "wave_margin_top_of_wbottom",
        "edit_field_class" => "vc_col-sm-6 vc_column",
        "value" => '',
        "std" => '',
        "group" => esc_html__("Waves Settings", 'wp-maxclean') 
    )); 
    
    vc_add_param('vc_row', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Enable triagl style", 'wp-maxclean'),
        'param_name' => 'enable_triagl_style',
        'value' => array(
            'Yes' => true
        ),
        'description' => esc_html__("Yes = Enable, default disable.", 'wp-maxclean'),
        "group" => esc_html__("Triagl Settings", 'wp-maxclean')
    ));
    
    vc_add_param('vc_row', array(
	    "type" => "dropdown",
        "heading" => esc_html__("Triagl style",'wp-maxclean'),
        "param_name" => "triagl_style",
        "value" => array(
        	"top-grey" => "topgray",
        	"top-primary" => "topprimary",
            "top-secondary" => "topsecondary",
            "top-white" => "topwhite",
            "btm-transparent" => "btmtransparent",
        	"btm-primary" => "btmprimary",
            "btm-secondary" => "btmsecondary",
            "btm-white" => "btmwhite",
        	),
        'std' => "top-grey",
        "group" => esc_html__("Triagl Settings", 'wp-maxclean')
    ));  
     
