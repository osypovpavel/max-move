<?php
/**
 * Add row params
 * 
 * @author Knight
 * @since 1.0.0
 */
  
 
    vc_add_param('vc_custom_heading', array(
	    "type" => "textfield",
        "heading" => esc_html__("Title",'wp-maxclean'),
        "param_name" => "text",
        "value" => '',
        "std" => '',
        'weight' => 10,
    )); 
    vc_add_param('vc_custom_heading', array(
	    "type" => "textfield",
        "heading" => esc_html__("Sub title",'wp-maxclean'),
        "param_name" => "subtitle",
        "value" => '',
        'weight' => 9,
    )); 
    vc_add_param('vc_custom_heading', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Subtitle on top", 'wp-maxclean'),
        'param_name' => 'subtitle_on_top',
        'value' => array(
            'Yes' => true
        ),
        'weight' => 8,
        'description' => esc_html__("Yes = Show subtitle first", 'wp-maxclean')
    ));
    vc_add_param('vc_custom_heading', array(
        'type' => 'checkbox',
        'heading' => esc_html__("Show border color", 'wp-maxclean'),
        'param_name' => 'show_border_color',
        'value' => array(
            'Yes' => true
        ),
        'weight' => 7,
        'description' => esc_html__("Yes = Show Border color", 'wp-maxclean')
    ));
    vc_add_param('vc_custom_heading', array(
        'type' => "textfield",
        'heading' => esc_html__("Border color width", 'wp-maxclean'),
        'param_name' => 'border_color_width',
        'value' => '',
        'std' => '',
        'description' => esc_html__("ex: 370px", 'wp-maxclean'),
        'weight' => 6
    ));
    vc_add_param('vc_custom_heading', array(
        'type' => "textfield",
        'heading' => esc_html__("Border color thick", 'wp-maxclean'),
        'param_name' => 'border_color_thick',
        'value' => '',
        'std' => '',
        'description' => esc_html__("ex: 3px", 'wp-maxclean'),
        'weight' => 5
    ));
    vc_add_param('vc_custom_heading', array(
        'type' => "textfield",
        'heading' => esc_html__("Border color margin top", 'wp-maxclean'),
        'param_name' => 'border_color_margin_top',
        'value' => '',
        'std' => '',
        'description' => esc_html__("ex: 25px", 'wp-maxclean'),
        'weight' => 4
    ));
    vc_add_param('vc_custom_heading', array(
        'type' => "textfield",
        'heading' => esc_html__("Border color margin bottom", 'wp-maxclean'),
        'param_name' => 'border_color_margin_bottom',
        'value' => '',
        'std' => '',
        'description' => esc_html__("ex: 55px", 'wp-maxclean'),
        'weight' => 3
    ));
     vc_add_param('vc_custom_heading', array(
        'type' => "textfield",
        'heading' => esc_html__("Letter spacing title", 'wp-maxclean'),
        'param_name' => 'letter_spacing_title',
        'value' => '',
        'std' => '',
        'description' => esc_html__("ex: .07em or 2px", 'wp-maxclean'),
        'weight' => 2
    ));
    vc_add_param("vc_custom_heading", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Border Style", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "heading_style",
        "value" => array(
            "border left - left" => "heading-left-border-left",
            "border center - left" => "heading-center-border-left",
            "border center - center" => "heading-center-border-center"
        ),
        "std" => "border left - left",
        'weight' => 1
    ));
    vc_add_param('vc_custom_heading', array(
        "type" => "cms_template",
        "param_name" => "cms_template",
        "admin_label" => true,
        "heading" => esc_html__("Shortcode Template",'wp-maxclean'),
        "shortcode" => "vc_custom_heading",
        "group" => esc_html__("Template", 'wp-maxclean'),
    ));
 