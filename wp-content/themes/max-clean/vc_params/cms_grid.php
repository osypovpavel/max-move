<?php
 
 vc_add_param("cms_grid", array(
        'type' => 'dropdown',
        'heading' => esc_html__( 'Animation', 'wp-maxclean' ),
        'param_name' => 'grid_home_animation',
        'std' => '',
        'description' => esc_html__( 'Animations  for grid', 'wp-maxclean' ),
        'value' =>  wp_maxclean_animate_lib(),
        'dependency' => array(
            'element' => 'cms_template',
            'value' => array(
                'cms_grid--home_posts.php',
                'cms_grid--moving-home.php',
            ),
        ),
        "group" => esc_html__("Template", 'wp-maxclean')
    ));
    vc_add_param("cms_grid", array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_html__("Data wow duration", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "grid_data_wow_duration",
        "value" =>  array(
            'None'  => '',
            '1s'    => '1s',
            '2s'    => '2s',
            '3s'    => '3s',
            '4s'    => '4s',
            '5s'    => '5s',
        ), 
        'dependency' => array(
            'element' => 'cms_template',
            'value' => array(
                'cms_grid--home_posts.php',
                'cms_grid--moving-home.php',
            ),
        ),
        "group" => esc_html__("Template", 'wp-maxclean')
    ));
    vc_add_param("cms_grid", array(
        "type" => "checkbox",
        "class" => "",
        "heading" => esc_html__("Show readmore", 'wp-maxclean'),
        "admin_label" => true,
        "param_name" => "show_readmore",
        'value' => array(
                'Yes' => true
            ),
        'dependency' => array(
            'element' => 'cms_template',
            'value' => array(
                'cms_grid--home_posts.php',
                'cms_grid--moving-home.php',
                'cms_grid--cleaning-more.php',
                'cms_grid--moving-services.php'
            ),
        ),
        "group" => esc_html__("Template", 'wp-maxclean')
    ));
 