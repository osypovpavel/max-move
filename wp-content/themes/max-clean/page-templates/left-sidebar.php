<?php
/**
 * Template Name: Left Sidebar
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 * @author Fox
 */

get_header(); ?>
<div id="page-left-sidebar">
        <div class="container">
            <div class="row"> 
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <?php get_sidebar(); ?>
                </div>
                <div id="primary" class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div id="content" role="main">
    
                       <?php
                            // Start the loop.
                            while ( have_posts() ) : the_post();
    
                                // Include the page content template.
                                get_template_part( 'single-templates/content', 'page' );
    
                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;
    
                            // End the loop.
                            endwhile;
                            ?>
    
                    </div><!-- #content -->
                </div><!-- #primary -->
            </div> <!-- #row -->
        </div> <!-- #container -->

        <?php wp_maxclean_page_info_foot();?>
	 
</div>
<?php get_footer(); ?>