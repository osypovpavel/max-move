<?php
/**
 * Template Name: Right Sidebar
 *
 * @package CMSSuperHeroes
 * @subpackage WP Maxclean
 * @since 1.0.0
 * @author Fox
 */

get_header(); ?>
<div id="page-right-sidebar">
    <div class="container">
        <div class="row">
            <div id="primary" class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div id="content" role="main">

                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'single-templates/content', 'page' ); ?>
                        <?php comments_template( '', true ); ?>
                    <?php endwhile; // end of the loop. ?>

                </div><!-- #content -->
            </div><!-- #primary -->
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                 <?php
                    if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
                    <div id="secondary" class="widget-area" role="complementary">
                    <?php 
                        dynamic_sidebar( 'sidebar-1' );
                        ?>
                        </div>
                        <?php
                    }
                    else { ?>
                    <div id="secondary" class="widget-area" role="complementary">
                        <?php 
                        get_sidebar();
                        ?> 
                        </div>
                    <?php }
                ?>
            </div>
        </div>
    </div>
    <?php wp_maxclean_page_info_foot();?>
</div>
<?php get_footer(); ?>