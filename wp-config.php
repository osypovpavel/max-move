<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'maxclean_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T<MOB2Q9!uD%^?}(8Y;P=v]QEdR,H|KcOKgFx%^5MSUZb>S>DX95jhfYQ~w7*S~-');
define('SECURE_AUTH_KEY',  '&d6|c95{TogS.s[=Ss ;QjoaQ`fQ_#450v:=#-o:s}RfE)d iR(tD~=hZ`VX])jW');
define('LOGGED_IN_KEY',    'M_$CM}RxBS}5)rNJS9[g`!R}6v9)~hv$#><*!t7(MUNX,T*{5TAPBcAEMKHxz~`}');
define('NONCE_KEY',        'DdbFA?QEM02L]}QHVX~N%OdpX@H0nz?_ffdRAJ+f?&uUL+x;0/m1/NruUm#;&O;t');
define('AUTH_SALT',        '!J<;v ;uz,Z^nLB3j0aH;%P3T4*1vXw(1[<3ee__}ue<w}h+@LtD _#h}Z.LR+Kt');
define('SECURE_AUTH_SALT', '?#11KZ>c%_)|q7jY%n(*7l}:f@c3BGumd2X=*26Kh$w:g&efvsQdlM@voxR4R6sV');
define('LOGGED_IN_SALT',   'kH{TOB{%XtQ~M$frNuw>eV!J.O+^+I+o/S{BpC527rr2q>5 9hk+qC6B.l6n3 As');
define('NONCE_SALT',       'JJ{U&FzxPws(&4#{=@&#!nc6!Q.P}@&=iNk&CS^et>MtVYMtm<  XS(3U-KoG&JE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
